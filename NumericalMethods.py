"""
Custom numerical methods and algorithms used in this project
"""
from math import factorial

import numpy as np

from config import logger


# numpy error handling
np.seterr(over='raise')


def binomial_coefficient(n, k):
    return factorial(n) / (factorial(k) * factorial(n-k))


def adjust_vector_shape(x_vec):
    """
    Adds 3rd unit row to a vector in preparation for usage in transformations
    """
    assert x_vec.shape[1] in [1, 2]
    
    # single column vector
    if x_vec.shape[1] == 1:
        x_len = x_vec.shape[0]/2
        x_new = np.ones((3, x_len), dtype=np.float)
        x_new[0, :] = x_vec[0:x_len]    # x
        x_new[1, :] = x_vec[x_len:]     # y

    # double column vector
    else:
        x_len = x_vec.shape[0]
        x_new = np.ones((3, x_len), dtype=np.float)
        x_new[0, :] = x_vec[:, 0]     # x
        x_new[1, :] = x_vec[:, 1]     # y

    return x_new


def apply_rowcol_dist(x_vec, col_dist, row_dist):
    """
    col_dist - distance on x axis between two consecutive nodes  
    row_dist - distance on y axis between two consecutive rows 
    """
    assert type(x_vec) == np.ndarray
    assert x_vec.shape[0] in [2, 3]

    x_vec[0, : ] *= col_dist    # bx
    x_vec[1, : ] *= row_dist    # by

    return x_vec


def apply_transformations(x_vec, t_vec, angle_rot, angle_shear):
    assert type(x_vec) == np.ndarray

    # determine vector shape
    if x_vec.shape[0] == 2:
        raise    
    if x_vec.shape[0] == 3:
        # shear
        t_shear = shear_matrix(angle_shear)
        t_rot = rotation_matrix(angle_rot)
        t_tran = translation_matrix(np.array(t_vec))

        return np.dot(t_tran, np.dot(t_rot, np.dot(t_shear, x_vec)))[:2, :]
    else:
        # y vector stored under x
        raise


def get_lattice_mapping(n_elements, max_columns):
    """
    Returns a matrix of [[i, j]] mappings given a number of elements
    and maximum number of elements per column
    """
    mapping = np.zeros((n_elements, 2), dtype=np.int)
    i, j = 0, 0
    for i_element in xrange(n_elements):
        #mapping[i_element, :] = list(divmod(i_element, max_columns))
        j, i = divmod(i_element, max_columns)
        #i, j = divmod(i_element, max_columns)
        mapping[i_element, :] = [i, j]

    return mapping


def get_lattice_point_coords(n_elements, max_columns, row_dist, col_dist, return_shape=2):
    """
    Returns a list of x,y points given:
    n_elements - number of lattice elements
    max_columns - maximum number of elements per row
    row_dist - distance between consecutive rows
    col_dist - distance between consecutive elements in a row
    return_shape - number of columns in the returned matrix
            if its set to 1 the y vector will be set below x vector
    """
    assert return_shape == 2

    element_map = get_lattice_mapping(n_elements, max_columns)
    element_map = adjust_vector_shape(element_map)

    element_map = apply_rowcol_dist(element_map, row_dist, col_dist)
    #element_map[0, : ] *= col_dist
    #element_map[1, : ] *= row_dist

    return element_map


def get_wt_coordinates(angle_rot, angle_shear, col_dist, row_dist, d_x, d_y,
                       x_vec=None, n_elements=None, max_col=None,
                       vector_format='optimizer'):
    """
    Function used to get the WT coordinates having the variables from WFLOP#1
    
    Input:
    vector_col_format - specifies the format of the output coordinate vector
            'optimizer' - 1 column vector, y vector is stored below x vector
            anythine else - outputs a row vector, each column is a set of [x,y]
    """
    logger.debug('get_wt_coordinates: alpha={}, beta={}, bx={}, by={}, dx={}, dy={}, x_vec={}'
                 .format(angle_rot, angle_shear, col_dist, row_dist, d_x, d_y, x_vec))
    # variable order
    # row_dist, col_dist, t_vec, angle_rot, angle_shear,
    # alpha  - rotation angle of the WF
    # beta   - shear angle fo the WF
    # b_x    - distance between WT in a row
    # b_y    - distance between rows
    # d_x    - horizontal translation of the WF
    # d_y    - vertical translation of the WF

    # create a new vector if n_elements and max_col are specified
    # and x_vec is not
    if x_vec is None and n_elements is not None and max_col is not None:
        x_vec = adjust_vector_shape(get_lattice_mapping(n_elements, max_col))
    elif x_vec is None and n_elements is None and max_col is None:
        logger.error('x_vec or n_elements and max_col need to be specified')
        raise TypeError()

    assert type(x_vec) == np.ndarray
    # assumes we have an initial wt coordinates from mapping
    #x_vec = apply_transformations(
    #        apply_rowcol_dist(x_vec, row_dist, col_dist), 
    #        [[d_x], [d_y]], angle_rot, angle_shear)
    x_vec = apply_transformations(
            apply_rowcol_dist(x_vec, col_dist, row_dist), 
            [[d_x], [d_y]], angle_rot, angle_shear)

    if vector_format == 'optimizer':
        return np.hstack((x_vec[0,:], x_vec[1,:]))
    else:
        return x_vec[:2, :]


#def rotate(vec, angle):
#    assert vec.shape[1] == 1
#    assert 0. <= angle <= 2.*np.pi
#
#    angle = angle * np.pi / 180.
#    T_rot = np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]], dtype=np.float)
#    return np.dot(T_rot, vec.T).T


#def shear(vec, angle):
#    assert vec.shape[1] == 2
#    assert 0. <= angle <= np.pi / 2.
#
#    angle = angle * np.pi / 180.
#    T_shear= np.array([[-np.cos(angle)/np.sin(angle), -np.sin(angle)], 
#                       [0,                             np.cos(angle)]], 
#              dtype=np.float)
#    return np.dot(T_shear, vec.T).T


#def translate(vec, t):
#    assert vec.shape[1] == 2
#    assert t.shape == (2, 1)
#    
#    vec_new = np.ones((vec.shape[0], 3))
#    vec_new[:, 0:2] = vec
#    
#    M_trans = np.array([[1, 0, t[0]], [0, 1, t[1]], [0, 0, 1]], dtype=np.float)
#    return np.dot(M_trans, vec_new.T)[0:2,:].T


def rotation_matrix(angle):
    #assert 0. <= angle <= 2. * np.pi
    return np.array([[np.cos(angle), -np.sin(angle), 0], 
                     [np.sin(angle),  np.cos(angle), 0],
                     [0,              0,             1]], dtype=np.float)


def translation_matrix(trans_vec):
    assert trans_vec.shape[0] == 2

    return np.array([[1.,   0.,   trans_vec[0]],
                     [0.,   1.,   trans_vec[1]],
                     [0.,   0.,   1.]], dtype=np.float)


def shear_matrix(angle):
    """
    Returns a horizontal shear mapping matrix given an angle in radians.
    Transformation matrix preserves the distances, not areas.
    """
    # TODO: ipopt seems to ignore this
    #assert 0. <= angle <= np.pi * 2.

    return np.array([[1,    np.sin(np.pi/2 - angle), 0.], 
                     [0.,   np.cos(np.pi/2 - angle), 0.],
                     [0.,   0.,            1.]], 
                  dtype=np.float)


def __example():
    import matplotlib.pyplot as plt

    number_elements = 20
    elements_per_row = 4

    row_dist = 2
    col_dist = 3
    t_vec = np.array([[15], [16]])
    alpha = 45. *np.pi/180
    beta = 60. *np.pi/180

    element_mapping = get_lattice_mapping(number_elements, elements_per_row)
    element_coords = get_lattice_point_coords(number_elements, elements_per_row,
                                              row_dist, col_dist)
    print element_mapping
    print element_coords

    #t_rot = rotation_matrix(alpha)
    t_shear = shear_matrix(beta)
    #t_trans = translation_matrix(t_vec)

    # apply transformations
    #l_rot = np.dot(t_rot, element_coords)
    l_shear = np.dot(t_shear, element_coords)
    #l_trans = np.dot(t_trans, element_coords)
    #l_all = np.dot(t_trans, np.dot(t_rot, np.dot(t_shear, element_coords)))
    l_all = apply_transformations(element_coords, t_vec, alpha, beta)
    print l_all


    plt.scatter(element_coords[0,:], element_coords[1,:], c='r', label='initial')
    #plt.scatter(l_rot[0,:], l_rot[1,:], c='g', marker='x', label='rotation')
    plt.scatter(l_shear[0,:], l_shear[1,:], c='b', marker='v', label='shear')
    #plt.scatter(l_trans[0,:], l_trans[1,:], c='y', marker='<', label='translation')

    plt.scatter(l_all[0,:], l_all[1,:], c='g', marker='x', label='all')


    plt.legend()
    plt.axes().set_aspect('equal', 'datalim')
    plt.show()

def run1():
    import matplotlib.pyplot as plt
    x0 = [0., 0., 50., 50., 0., 0.]

    x_new = get_wt_coordinates(*x0, n_elements=8, max_col=4)

    print x_new

    print x_new.shape
    print x_new[9:]

    plt.scatter(x_new[:8], x_new[8:])
    plt.axes().set_aspect('equal', 'datalim')
    plt.show()

if __name__ == '__main__':
    __example()
    #run1()

"""
Module containing functions and classes used in Colonel <-> Python interface
"""
import os
import stat     # file permissions
import sys
import shutil
from numpy import float64

from config import logger
from config import WORKSPACE_PATH
#from config import COLONEL_INSTANCE_IDENTIFIER 
from config import COLONEL_INPUT_FILE
from config import COLONEL_OUTPUT_FILE
from config import COLONEL_INPUT_DIR_PREFIX
from config import COLONEL_OUTPUT_DIR_PREFIX
#from config import COLONEL_INSTANCE_IDENTIFIER
#from config import COLONEL_EXECUTABLE_PATH
from config import DEFAULT_INIT_FILENAME
from config import ARCHIVE_STATUS, ARCHIVE_FILE_PATH
from ColonelCommandsData import colonelScriptCommands as CCD


class ColonelInit(object):

    InitFilename = None
    InitContents = None  # string with init file contents
    
    def __init__(self, filename=DEFAULT_INIT_FILENAME):
        if filename is not None:
            if os.path.exists(filename):
                logger.info('Setting Init file.')
                self.InitFilename = filename
            else:
                logger.error('Init file {} does not exist'.format(filename))

    def read(self):
        if self.InitFilename is not None:
            with open(self.InitFilename, 'r') as f:
                self.InitContents = f.read()
        else:
            logger.error('Invalid Init file')

    def write(self, location=WORKSPACE_PATH):
        if self.InitContents is not None:
            logger.info('Writing Init file contents to {}'
                        .format(os.path.join(location, DEFAULT_INIT_FILENAME)))
            with open(os.path.join(location, DEFAULT_INIT_FILENAME), 'w') as f:
                f.write(self.InitContents)
        else:
            logger.error('Init file was not read.')


class ColonelInput(object):
    """Class representing Colonel input file """

    def __init__(self, colonelInstanceString=None, filepath=None):

        self.InputFilePath = None
        
        # number of input files processed
        self.number_inputs = 0
        
        # CommandList
        # { 'CommandGroupName': CommandGroupName,
        # 'Data':
        # [{'CommandName': command, 
        #   'CommandGroupName': str,
        #   'ArgumentList': [Arguments], 
        #   'OutputLines': [LineMin, LineMax]}, 
        # ...
        # ]
        # if 'Output' key has an empty list, that means no output is expected from this command
        self.CommandList = {}

        # stores the value of the last expected line number in the output file
        self.lastOutputLine = -1

        if filepath is not None:
            if os.access(filepath, os.W_OK):
                logger.info('ColonelInput: Setting input file path to {}'
                            .format(filepath))
                self.InputFilePath = filepath
        else:
            if colonelInstanceString is not None:
                self.InputFilePath = os.path.join(WORKSPACE_PATH,
                        colonelInstanceString, COLONEL_INPUT_DIR_PREFIX,
                        COLONEL_INPUT_FILE)

                logger.info('ColonelInput: Using default input file path {}.'
                            .format(self.InputFilePath))
            else:
                logger.error('ColonelInput: no instance string '
                             'or input file path specified.')
                sys.exit()

    def addCommand(self, commandName=None, commandArguments=None, group=None):

        status = self.__checkCommandArguments(commandName, commandArguments)

        if status == 0:
            # set the command group
            if group is not None:
                CommandGroupName = group
            else:
                logger.debug('No command group specified. Using default one.')
                CommandGroupName = 'Initial'

            # determine output line numbers
            if CCD[commandName]['ProducesOutput'] == True:
                LineMin = self.lastOutputLine + 1
                if isinstance(CCD[commandName]['NumberOfOutputRows'], str):
                    # variable number of output rows
                    # TODO: change this
                    #logger.debug('{} has variable number of output rows'.format(commandName))
                    LineMax = LineMin + 1
                else:
                    LineMax = LineMin + CCD[commandName]['NumberOfOutputRows']
                LineList = [LineMin, LineMax]

                # update the last output line
                self.lastOutputLine = LineMax
            else:
                LineList = []

            # check if there is already a command list group
            # if not create one
            if len(self.CommandList) == 0:   # empty dictionary
                self.CommandList['CommandGroupName'] = CommandGroupName
                self.CommandList['Data'] = []
                logger.debug('Creating new command group')

            if self.CommandList['CommandGroupName'] == CommandGroupName:
                self.CommandList['Data'].append({
                    'CommandName': commandName,
                    'Arguments': commandArguments,
                    'OutputLines': LineList})
                #logger.debug('Added command: {}'.format(self.CommandList['Data'][-1]))
        else:
            logger.error('Wrong arguments ({}) provided for command {}'
                         .format(commandArguments, commandName))

        #logger.debug('addCommand: CommandList = {}'.format(self.CommandList))
        #logger.debug('Processed command: {}'.format(commandName))

    def getCommandList(self):
        return self.CommandList

    def getCommandListString(self):
        CommandString = ''
        if len(self.CommandList) > 0:
            for iCommand in self.CommandList['Data']:
                #logger.debug('Current iCommand: {}'.format(iCommand))

                # add newline if its not the beginning of the string
                if CommandString != '':
                    CommandString += '\n'
                
                # add command name
                CommandString += iCommand['CommandName']

                # add arguments if necessary
                if iCommand['Arguments'] is not None:
                    #CommandString += ' '.join(_ for _ in iCommand['Arguments'])
                    for iArg in iCommand['Arguments']:
                        # check if argument is a string
                        if isinstance(iArg, str):
                            if iArg in ['on', 'off']:
                                CommandString += ' {}'.format(iArg)
                            else:
                                # add quotes if an argument is a string
                                CommandString += ' \"{}\"'.format(iArg)
                        # correctly format the floating point numbers
                        elif isinstance(iArg, float) or isinstance(iArg, float64):
                            CommandString += ' {:.15f}'.format(iArg)
                        else:
                            CommandString += ' {}'.format(iArg)

        return CommandString

    def insertCommand(self, commandIndex, commandName, commandArguments=None):
        pass

    def moveTurbines(self, turbineList=None, X=None):
        pass

    def removeCommand(self, commandIndex=None):
        pass

    def resetCommandList(self):
        # reset the command list
        logger.debug('Resetting the command list')
        self.CommandList = {}

        # output lines needs to be reset as well
        self.lastOutputLine = -1

    def save(self):
        logger.debug('Saving input file {}'.format(self.InputFilePath))

        CommandString = self.getCommandListString()

        #print '\n\n{}\n\n'.format(CommandString)

        # TODO: this causes Colonel to read input file incorrectly
        #       produces <Unrecognized command>
        #with open(self.InputFilePath, 'w') as fd:
        #    fd.writelines(CommandString)
        
        # write the new input file only if the old one was deleted by Colobel
        while True:
            if not os.path.exists(self.InputFilePath):
                # write only permissions
                with os.fdopen(
                        os.open(self.InputFilePath, os.O_WRONLY | os.O_CREAT, 0220), 'w') as fd:
                    fd.writelines(CommandString)

                # change permissions adding read access for ug
                os.chmod(self.InputFilePath, stat.S_IRUSR | stat.S_IWUSR 
                        | stat.S_IRGRP | stat.S_IWGRP)

                # archive the input file
                if ARCHIVE_STATUS:
                    archiveDict = {'Name': 'Input',
                                'Filename': self.InputFilePath,
                                'Data': CommandString}
                    with open(ARCHIVE_FILE_PATH, 'a') as fa:
                        fa.write('{}'.format(archiveDict))
                        fa.write('\n')

                    # copy the input file
                    try:
                        fn, ext = os.path.splitext(self.InputFilePath)
                        shutil.copy(self.InputFilePath, 
                                    os.path.join(fn + str(self.number_inputs) + ext))
                        self.number_inputs += 1
                    except OSError as e:
                        logger.error('OSError: <{}>'.format(e.message))
                        pass
                break
            #else:
            #    logger.error('Input file <{}> still exists!'.format(self.InputFilePath))


    def setInputFile(self, location):
        logger.info('Setting input file location to {}'.format(location))
        self.InputFilePath = location

    def __checkCommandArguments(self, command, argumentList):

        # TODO:
        # add a check to verify when an empty list is provided when None is expected !!!!!!

        #logger.debug('Checking command arguments for {},[{}]'.format(command, argumentList))
        status = self.__checkCommandInDict(command)

        # check if number of arguments is correct
        if status == 0:
            if isinstance(argumentList, list):
                if len(argumentList) == CCD[command]['NumberOfArguments']:
                    # check if the arguments are of the right type
                    if 'Types' in CCD[command]['ArgumentTypes']:
                        for iArg in argumentList:
                            if type(iArg) not in CCD[command]['ArgumentTypes']['Types']:
                                logger.error('Wrong argument type. Got {}, expected {}'
                                            .format(type(iArg), CCD[command]['ArgumentTypes']['Types']))
                                return 1
                            else:
                                status = 0
                    elif 'Values' in CCD[command]['ArgumentTypes']:
                        for iArg in argumentList:
                            if iArg not in CCD[command]['ArgumentTypes']['Values']:
                                status = 1
                                logger.error('Argument [{}] not allowed for {}'.format(iArg, command))
                    else:
                        status = 1
                        logger.error('Invalid number of arguments or invalid types.')
                        return status
                else:
                    status = 1
                    logger.error('Wrong number of command arguments ({}) for {}'
                                .format(len(argumentList), command))


            elif argumentList is None and CCD[command]['NumberOfArguments'] == 0:
                # this particular command does not accept arguments
                status = 0
                logger.debug('Valid command \"{}\" and arguments ({})'.format(command, argumentList))

            else:
                status = 1
                logger.error('Command arguments have to be provided as a list.')

        return status

    def __checkCommandInDict(self, command):
        """Checks if the specified 'command' is in the allowed commands list"""
        if isinstance(command, str):
            if command in CCD:
                #logger.debug('Command \'{}\' is valid'.format(command))
                return 0
            else:
                logger.error('Invalid command \'{}\''.format(command))
                return 1  # command not in dict
        else:
            logger.error('Command has to be a string, got {}'
                         .format(type(command)))
            return 1


class ColonelOutput(object):

    def __init__(self, instanceIdentifier):
        self.OutputFilePath = None
        self.OutputContents = None

        # number of processed output files
        self.number_outputs = 0

        if instanceIdentifier is not None:
            p = os.path.join(WORKSPACE_PATH, instanceIdentifier, 
                    COLONEL_OUTPUT_DIR_PREFIX, COLONEL_OUTPUT_FILE)
            self.OutputFilePath = p
        else:
            logger.error('ColonelOutput: Received wrong instance identifier '
                         '<{}>'.format(instanceIdentifier))
            # using default output filename
            #self.OutputFilePath = os.path.join(COLONEL_OUTPUT_PATH, DEFAULT_OUTPUT_FILENAME)

    def delete_outputfile(self):
        if self.OutputContents is not None and self.OutputFilePath is not None:
            logger.debug('Removing output file <{}>'.format(self.OutputFilePath))
            try:
                os.remove(self.OutputFilePath)
            except OSError:
                logger.error('Error deleting file <{}>'.format(self.OutputFilePath))

    def getAEP(self):
        AEPDict = None
        if self.OutputContents is not None:
            # TODO: get the line numbers that correspond to AEP

            # convert strings to float
            # assumes there is only 1 line in the file contents
            try:
                AEP = [float(_) for _ in self.OutputContents.split()]
            except ValueError as e:
                logger.error('Path = <{}> output contents: <{}>'
                             .format(self.OutputFilePath, self.OutputContents))
                raise e

            AEPDict = {'Name': 'AEP', 'Data': AEP}
            
            # delete the old data
            self.OutputContents = None
        else:
            logger.error('Read the contents of the output file first')
        return AEPDict

    def getAEP_turbine(self):
        AEPDict = None
        if self.OutputContents is not None:
            # TODO: get the line numbers that correspond to AEP

            # convert strings to float
            # assumes there is only 1 line in the file contents
            try:
                AEP = [float(_) for _ in self.OutputContents.split()]
            except ValueError as e:
                logger.error('Path = <{}> output contents: <{}>'
                             .format(self.OutputFilePath, self.OutputContents))
                raise e

            AEPDict = {'Name': 'AEP turbine', 'Data': AEP}
            
            # delete the old data
            self.OutputContents = None
        else:
            logger.error('Read the contents of the output file first')
        return AEPDict

    def get_gradient_turbine(self):
        AEPDict = None
        if self.OutputContents is not None:
            # TODO: get the line numbers that correspond to AEP

            # convert strings to float
            # assumes there is only 1 line in the file contents
            try:
                AEP = [float(_) for _ in self.OutputContents.split()]
            except ValueError as e:
                logger.error('Path = <{}> output contents: <{}>'
                             .format(self.OutputFilePath, self.OutputContents))
                raise e

            AEPDict = {'Name': 'Gradient turbine', 'Data': AEP}
            
            # delete the old data
            self.OutputContents = None
        else:
            logger.error('Read the contents of the output file first')
        return AEPDict

    def getGradient(self):
        d_gradient = None
        if self.OutputContents is not None:
            d_gradient = []
            for line in self.OutputContents.splitlines():
                line_lst = line.split()
                d_gradient.append([float(line_lst[0]), float(line_lst[1])])

            # delete the old data
            self.OutputContents = None
        else:
            logger.error('Read the contents of the output file first')
        return d_gradient


    def getTurbineList(self):
        WTList = []
        if self.OutputContents is not None:
            logger.debug('<{}>'.format(self.OutputContents))
            tmpList = []
            # process the data
            for idx, line in enumerate(self.OutputContents.splitlines()):
                # disregard the header line
                if idx != 0:
                    # get the data from columns
                    tmpList = line.split()
                    WTList.append({'Number': tmpList[0],
                                   'Id': tmpList[1],
                                   'Model': tmpList[2],
                                   'x': tmpList[3],
                                   'y': tmpList[4],
                                   'Status': tmpList[5]})
                    #logger.debug('{}'.format(tmpList))
        else:
            logger.error('Read the contents of the output file first')

        return WTList

    def getOutputFilePath(self):
        return self.OutputFilePath

    def getOutputString(self):
        return self.OutputContents

    def read(self):
        if self.OutputFilePath is not None:
            # Check file access rights
            while True:
                #if os.path.exists(self.OutputFilePath):
                if os.access(self.OutputFilePath, os.F_OK | os.R_OK):
                    logger.debug('Reading output file {}'.format(self.OutputFilePath))

                    try:
                        with open(self.OutputFilePath, 'r') as f:
                            self.OutputContents = f.read()

                        # archive the output file
                        if ARCHIVE_STATUS:
                            archiveDict = {'Name': 'Output',
                                           'Filename': self.OutputFilePath,
                                           'Data': self.OutputContents}
                            with open(ARCHIVE_FILE_PATH, 'a') as fa:
                                fa.write('{}'.format(archiveDict))
                                fa.write('\n')
                            
                            # copy the output file
                            try:
                                fn, ext = os.path.splitext(self.OutputFilePath)
                                shutil.copy(self.OutputFilePath,
                                            os.path.join(fn + str(self.number_outputs) + ext))
                                self.number_outputs += 1
                            except OSError as e:
                                logger.error('OSError <{}>'.format(e.message))

                        break
                    except IOError:
                        logger.error('IOError for <{}>'.format(self.OutputFilePath))
        else:
            logger.error('Output file is not set.')
        
    def parse(self):
        pass

    def setCommandList(self, commandList):
        """ Used to determine the origin commands from the output file """
        pass

    def setOutputContents(self, ContentsString):
        self.OutputContents = ContentsString
        logger.debug('Manually setting OutputContents')


def DistributeInputSignal(signal):
    """Function responsible for distribution  """
    pass


=======
SIGNALS
=======

This document contains documentation describing the use of signals and their format in communication between processes.

General signal template:

.. code-block:: python

    {
    'SignalName': str,
    'SignalData': [data],
    'SignalOrigin': str,
    'SignalTarget': str,    # Optional
    'SignalReturn': str     # Optional
    }


InputProcessor
==============

Incomming signals
-----------------
addCommand
save
resetCommandList
getCommandList
getCommandListString

.. code-block:: python

    {
    'SignalName': 'Gradient',
    'SignalData': ['column vector with perturbed coordinates'],
    'SignalOrigin': 'Distributor'
    'SignalTarget': 'Instance Identifier'
    }


Outgoing signals
----------------

OutputProcessor
===============

Incomming signals
-----------------

Outgoing signals
----------------


Optimizer
=========

Incomming signals
-----------------

Outgoing signals
----------------


Distributor
===========

Incomming signals
-----------------

'SignalName'
    - addCommand
    - getTurbineList
    - gradient
    - save

.. code-block:: python

    {
    'SignalName': 'Finished gradient',
    'SignalData': [aep],
    'SignalOrigin': 'OutputProcessor'
    'SignalTarget': 'Instance Identifier'
    }

.. code-block:: python

    {
    'SignalName': 'ChangeTarget',
    'SignalData': None,
    'SignalOrigin': str,
    'SignalReturn': 'New Worker Id'
    }


Outgoing signals
----------------
.. code-block:: python

    {
    'SignalName': 'Gradient',
    'SignalData': ['column vector with perturbed coordinates'],
    'SignalOrigin': 'Distributor'
    'SignalTarget': 'Instance Identifier'
    }


Watchdog
========

Incomming signals
-----------------
None explicitely

Outgoing signals
----------------

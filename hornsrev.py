import numpy as np

from examples import WFLOP_1, WFLOP_2
#import  NumericalMethods as nm
import Symbolic as sb
import NumericalMethods as nm
#from WFLOP import BaseWFLOP
from WFLOP import WindFarm
#from workers import checkSignal
from config import (logger,
                    WFLOP1_MAX_ELEMENTS_PER_ROW,
                    WFLOP1_NUMBER_ELEMENTS,
                    WFLOP1_CONS_BOUNDARY,
                    WFLOP1_CONS_AREA,
                    WFLOP1_CONS_PROXIMITY,
                    WFLOP2_SCALING)


class hornsrev(WFLOP_1):
    """
    This is the base WFLOP class.
    All problems should create a child class and inherit methods from it.
    
    Methods:
    -------
    get_initial_setup - returns the variales needed for particular optimizer
    objfun          - objective function
    confun          - constraint function
    grad_objfun     - gradient of the objective function
    jac_confun      - jacobian of the constraint function
    hess_objfun     - hessian of the objective function
    hess_confun     - hessian of the constraint function
    """
    
    id = 'optimizer'

    def __init__(self, queue_list):
        logger.info('Initiating WFLOP#1')
        self.q_container = queue_list

        self.wf = WindFarm(self.q_container, NumberWT=WFLOP1_NUMBER_ELEMENTS)

        # wind farm boundary point coordinates
        hrxmin = 423974.
        hrxmax = 429431.
        hrymin = 6147543.
        hrymax = 6151447.
        hr_offset = 1000.
        
        self.boundary_poly = np.array([[hrxmin-hr_offset, hrxmax+hr_offset, hrxmax+hr_offset, hrxmin-hr_offset],
                                       [hrymin-hr_offset, hrymin-hr_offset, hrymax+hr_offset, hrymax+hr_offset]])

        # initial shape of the lattice
        #x_lattice = nm.adjust_vector_shape(nm.get_lattice_mapping(
        #    WFLOP1_NUMBER_ELEMENTS, WFLOP1_MAX_ELEMENTS_PER_ROW))
        self.n_elements = WFLOP1_NUMBER_ELEMENTS
        self.max_col = WFLOP1_MAX_ELEMENTS_PER_ROW

        # symbolic stuff
        self.vars = sb.WFLOP1_vars()
        logger.info('Deriving constraints for WFLOP1')
        self.con_expr = sb.WFLOP1_constraints(self.vars, self.boundary_poly)
        # function calculating constraint
        self.con_fun = sb.get_confun(self.vars, self.con_expr)
        # function calculating the jacobian of constraints
        logger.info('Deriving jacobian of constraints for WFLOP1')
        jaccon_expr = sb.get_jaccon_expr(self.vars, self.con_expr, simplify_expr=False)

        self.jaccon_fun = sb.get_jaccon_fun(self.vars, jac_constraints=jaccon_expr)

        # list of non zero rows and cols of jacobian
        self.jaccon_nz = sb.get_jac_nz_pos(jaccon_expr)

        # function returning dx/dvars used in gradient of the obj function
        dxdvars_expr = sb.WFLOP1_dxdvars(self.vars, self.n_elements, self.max_col)
        self.dxdvars_fun = sb.get_dxdvars_fun(self.vars, dxdvars_expr=dxdvars_expr)

        #logger.info(dxdvars_expr)
        #logger.info(self.con_expr)

    def initial_setup(self):
        logger.info('Setting up WFLOP#1')

        # Variable order:
        # ==============
        # x[0] - alpha  - rotation angle of the WF
        # x[1] - beta   - shear angle fo the WF
        # x[2] - b_x    - distance between WT in a row
        # x[3] - b_y    - distance between rows
        # x[4] - d_x    - horizontal translation of the WF
        # x[5] - d_y    - vertical translation of the WF

        # constraint bounds
        # -----------------
        b_u, b_l, area_l, area_u, prox_l, prox_u = [], [], [], [], [], [] 
        if WFLOP1_CONS_BOUNDARY:
            #b_l, b_u = np.array(sb.get_boundary_bounds(self.boundary_poly), dtype=np.float)
            # linalg
            b_u = np.zeros((4*WFLOP1_NUMBER_ELEMENTS,), dtype=np.float)
            #b_u = np.zeros((4*self.n_elements,), dtype=np.float)
            b_l = np.ones(b_u.shape)*-1e19
            # winding number
            #b_l = np.ones((self.n_elements,), dtype=np.float)*np.pi
            #b_u = np.ones((self.n_elements,), dtype=np.float)*1e20
        
        if WFLOP1_CONS_AREA:
            area_u = np.array([21000000], dtype=np.float)
            area_l = -area_u

        if WFLOP1_CONS_PROXIMITY:
            # number of proximity constraints
            n_prox_cons = nm.binomial_coefficient(self.n_elements, 2)

            prox_l = np.array([560.]*n_prox_cons, dtype=np.float)
            prox_u = np.array([1e19]*n_prox_cons, dtype=np.float)
        
        x_min = min(self.boundary_poly[0])
        x_max = max(self.boundary_poly[0])
        y_min = min(self.boundary_poly[1])
        y_max = max(self.boundary_poly[1])
        # number of variables
        n = len(self.vars)

        # xl is the lower bound of x as bounded constraints 
        xl = np.array([-1e19, 0., 200., 200., x_min, y_min], dtype=np.float)
        #xl = np.array([-1e19, 0.76518292326788695, 560., 560., x_min, y_min], dtype=np.float)
        xl = np.array([xl[i]/self.scale[i+1] for i in range(len(xl))])

        # xu is the upper bound of x as bounded constraints 
        xu = np.array([1e19, np.pi, 10000., 10000., x_max, y_max], dtype=np.float)
        #xu = np.array([1e19, 2.3764097303219063, 1e19, 1e19, x_max, y_max], dtype=np.float)
        xu = np.array([xu[i]/self.scale[i+1] for i in range(len(xu))])

        # m is the number of constraints
        m = len(self.con_expr)

        # gl is the lower bound of constraints 
        gl = np.concatenate((b_l, area_l, prox_l))

        # gu is the upper bound of constraints both gl,
        # gu should be one dimension arrays with length m 
        gu = np.concatenate((b_u, area_u, prox_u))
        # nnzj is the number of nonzeros in Jacobi matrix 

        nnzj = len(self.jaccon_nz[0])
        # nnzh is the number of non-zeros in Hessian matrix, you can set it to 0 
        nnzh = 0
        # initial coordinates
        x0 = np.array([0., np.pi/2, 400., 400., x_min, y_min], dtype=np.float)
        #x0 = (xl+xl)/2.
        x0 = np.array([x0[ii]/self.scale[ii+1] for ii in range(len(x0))])

        logger.info('Number of variables = {}'.format(n))
        logger.info('Number of constraints = {}'.format(m))
        logger.info('Number of nonzero elements in jaccon = {}'.format(nnzj))
        logger.info('Variable Constraints:')
        logger.info('Lower: <{}>'.format(xl))
        logger.info('Upper: <{}>'.format(xu))
        logger.info('Constraints bounds:')
        logger.info('Lower: <{}>'.format(gl))
        logger.info('Upper: <{}>'.format(gu))
        logger.info('Initial position'.format())
        logger.info('alpha = {}'.format(x0[0]))
        logger.info('beta = {}'.format(x0[1]))
        logger.info('b_x = {}'.format(x0[2]))
        logger.info('b_y = {}'.format(x0[3]))
        logger.info('d_x = {}'.format(x0[4]))
        logger.info('d_y = {}'.format(x0[5]))

        return n, xl, xu, m, gl, gu, nnzj, nnzh, x0


    #def objfun(self, *args, **kwargs):
    #    raise NotImplementedError

    #def confun(self, *args, **kwargs):
    #    raise NotImplementedError

    #def grad_objfun(self, *args, **kwargs):
    #    raise NotImplementedError

    #def jac_confun(self, *args, **kwargs):
    #    """
    #    This function expects a flat np.ndarray as an output
    #    in case flag == False
    #    """
    #    raise NotImplementedError

    #def hess_objfun(self, *args, **kwargs):
    #    raise NotImplementedError

    #def hess_confun(self, *args, **kwargs):
    #    raise NotImplementedError
    

class hornsrev_cartesian(WFLOP_2):

    scale = WFLOP2_SCALING
    id = 'optimizer'
    
    def __init__(self, queue_list):
        logger.info('Initiating WFLOP#2')
        self.q_container = queue_list

        self.wf = WindFarm(self.q_container, NumberWT=WFLOP1_NUMBER_ELEMENTS)

        # wind farm boundary point coordinates
        hrxmin = 423974.
        hrxmax = 429431.
        hrymin = 6147543.
        hrymax = 6151447.
        hr_offset = 5.
        
        self.boundary_poly = np.array([[hrxmin-hr_offset, hrxmax+hr_offset, hrxmax+hr_offset, hrxmin-hr_offset],
                                       [hrymin-hr_offset, hrymin-hr_offset, hrymax+hr_offset, hrymax+hr_offset]])
        
        # actual horns rev boundary
        p1 = [423974.0,6151447.0]
        p2 = [429431.0,6147543.0]
        p3 = [424386.0,6147543.0]
        p4 = [429014.0,6151447.0]
        self.boundary_poly = np.array([[p1[0], p2[0], p3[0], p4[0]],
                                       [p1[1], p2[1], p3[1], p4[1]]], dtype=np.float)

        # initial shape of the lattice
        #x_lattice = nm.adjust_vector_shape(nm.get_lattice_mapping(
        #    WFLOP1_NUMBER_ELEMENTS, WFLOP1_MAX_ELEMENTS_PER_ROW))
        self.n_elements = WFLOP1_NUMBER_ELEMENTS
        self.max_col = WFLOP1_MAX_ELEMENTS_PER_ROW

        # symbolic stuff
        self.vars = sb.WFLOP2_vars(self.n_elements)
        self.con_expr = sb.WFLOP2_constraints(self.vars, self.boundary_poly,
                                              scaling_factor=self.scale)
        #logger.info('Constraints:')
        #logger.info(self.con_expr)

        # function calculating constraint
        self.con_fun = sb.get_confun(self.vars, self.con_expr)
        # function calculating the jacobian of constraints
        jaccon_expr = sb.get_jaccon_expr(self.vars, self.con_expr)

        #logger.info('Jacobian of the constraints')
        #logger.info(jaccon_expr)

        self.jaccon_fun = sb.get_jaccon_fun(self.vars, jac_constraints=jaccon_expr)

        # list of non zero rows and cols of jacobian
        self.jaccon_nz = sb.get_jac_nz_pos(jaccon_expr)

    def initial_setup(self):
        logger.info('Setting up hornsrev layout and WFLOP#2')

        b_u, b_l, prox_l, prox_u = [], [], [], [] 

        if WFLOP1_CONS_BOUNDARY:
            b_u = np.zeros((self.boundary_poly.shape[1]*self.n_elements,), dtype=np.float)
            b_l = np.ones(b_u.shape)*-1e19
        if WFLOP1_CONS_PROXIMITY:
            n_prox_cons = nm.binomial_coefficient(self.n_elements, 2)
            prox_l = np.array([200.]*n_prox_cons, dtype=np.float)
            prox_u = np.array([1e19]*n_prox_cons, dtype=np.float)

        # number of variables
        n = self.n_elements*2

        # xl is the lower bound of x as bounded constraints
        x_min = [np.min(self.boundary_poly[0,:])]*self.n_elements
        x_max = [np.max(self.boundary_poly[0,:])]*self.n_elements
        y_min = [np.min(self.boundary_poly[1,:])]*self.n_elements
        y_max = [np.max(self.boundary_poly[1,:])]*self.n_elements

        xl = np.array(x_min + y_min, dtype=np.float)
        xl /= self.scale.x

        # xu is the upper bound of x as bounded constraints 
        xu = np.array(x_max + y_max, dtype=np.float)
        xu /= self.scale.x

        # m is the number of constraints, 
        m = len(self.con_expr)
        # gl is the lower bound of constraints 
        gl = np.concatenate((b_l, prox_l))
        # gu is the upper bound of constraints 
        #         both gl, gu should be one dimension arrays with length m 
        gu = np.concatenate((b_u, prox_u))
        # nnzj is the number of nonzeros in Jacobi matrix 
        nnzj = len(self.jaccon_nz[0])
        # nnzh is the number of non-zeros in Hessian matrix, you can set it to 0 
        nnzh = 0

        # initial coordinates
        x0 = np.array([10.*ii for ii in range(n)] + [200.*ii for ii in range(n)])

        x0 = np.array([100., 1900., 1800., 200., 100.,  200., 1800., 1700.], dtype=np.float)

        hr_x = np.array([
                423974.00, 424033.00, 424092.00, 424151.00, 424210.00, 424268.00, 424327.00,
                424386.00, 424534.00, 424593.00, 424652.00, 424711.00, 424770.00, 424829.00,
                424888.00, 424947.00, 425094.00, 425153.00, 425212.00, 425271.00, 425330.00,
                425389.00, 425448.00, 425507.00, 425654.00, 425713.00, 425772.00, 425831.00,
                425890.00, 425950.00, 426009.00, 426068.00, 426214.00, 426273.00, 426332.00,
                426392.00, 426451.00, 426510.00, 426569.00, 426628.00, 426774.00, 426833.00,
                426892.00, 426952.00, 427011.00, 427070.00, 427129.00, 427189.00, 427334.00,
                427393.00, 427453.00, 427512.00, 427571.00, 427631.00, 427690.00, 427749.00,
                427894.00, 427953.00, 428013.00, 428072.00, 428132.00, 428191.00, 428250.00,
                428310.00, 428454.00, 428513.00, 428573.00, 428632.00, 428692.00, 428751.00,
                428811.00, 428870.00, 429014.00, 429074.00, 429133.00, 429193.00, 429252.00,
                429312.00, 429371.00, 429431.00], dtype=np.float)
        hr_y = np.array([
            6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148658.00, 6148101.00, 6147543.00,
            6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148658.00, 6148101.00, 6147543.00,
            6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148658.00, 6148101.00, 6147543.00,
            6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00,
            6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00,
            6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00,
            6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00,
            6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00,
            6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00,
            6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00],
            dtype=np.float)

        x0 = np.concatenate((hr_x[:self.n_elements], hr_y[:self.n_elements]))
        print x0

        # variable scaling
        x0[:self.n_elements] /= self.scale.x
        x0[self.n_elements:] /= self.scale.y
        # this results in the same position for all of the wind turbines
        #x0 = (xu + xl)/2.

        logger.info('Number of variables = {}'.format(n))
        logger.info('Number of constraints = {}'.format(m))
        logger.info('Number of nonzero elements in jaccon = {}'.format(nnzj))
        logger.info('Variable Constraints:')
        logger.info('Lower: <{}>'.format(xl))
        logger.info('Upper: <{}>'.format(xu))
        logger.info('Constraints bounds:')
        logger.info('Lower: <{}>'.format(gl))
        logger.info('Upper: <{}>'.format(gu))
        logger.info('Initial position'.format())
        logger.info('x0 = {}'.format(x0))

        return n, xl, xu, m, gl, gu, nnzj, nnzh, x0

import time
import multiprocessing
import traceback
import sys
from collections import namedtuple
import os
#import csv

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore

from NumericalMethods import binomial_coefficient
from config import (logger,
                    PLOTTING_ENABLED,
                    WFLOP1_NUMBER_ELEMENTS,
                    WFLOP1_MAX_ELEMENTS_PER_ROW,
                    ARCHIVE_STATUS,
                    ARCHIVE_FOLDER_PATH,
                    WFLOP1_CONS_BOUNDARY,
                    WFLOP1_CONS_AREA,
                    WFLOP1_CONS_PROXIMITY,
                    WFLOP_REGION)

DataContainer = namedtuple('DataContainer', ['aep', 'alpha', 'beta', 'bx', 'by', 'dx', 'dy'])


class PlotterMplt(multiprocessing.Process):
    id = 'plotter'

    def __init__(self, queue_list, n_elements, max_col):
        logger.info('Initiating')
        multiprocessing.Process.__init__(self)

        self.q_container = queue_list
        self.n_elements = n_elements
        self.max_col = max_col

    def run(self):
        if PLOTTING_ENABLED:
            logger.info('Starting')
            try:
                fig = plt.figure()
                ax_wf = fig.add_subplot(111, aspect='equal',
                                    xlim=(-500, 2500), ylim=(-500, 2500))

                self.particles, = ax_wf.plot([], [], 'ro')

                anim = animation.FuncAnimation(fig, self.update_plot, interval=10)

                print anim
                plt.show()

            except KeyboardInterrupt:
                logger.warning('Received KeyboardInterrupt')

            except Exception as e:
                error_str = traceback.format_exc()
                logger.error(error_str)
                self.q_container['main'].put([e, error_str])

    def initi_plot(self):
        pass

    def update_plot(self, i):
        signal = self.q_container[self.id].get()
        x = signal[0][:self.n_elements]
        y = signal[0][self.n_elements:]
        self.particles.set_data(x, y)

        return self.particles


class PlotterPyQt(multiprocessing.Process):
    id = 'plotter'

    def __init__(self, q_container, n_elements=WFLOP1_NUMBER_ELEMENTS,
                 max_col=WFLOP1_MAX_ELEMENTS_PER_ROW, event=None, *args, **kwargs):
        logger.info('Initializing')
        multiprocessing.Process.__init__(self)
        self.q_container = q_container
        self.n_elements = n_elements
        self.max_col = max_col

        self.layout_data = np.array([], dtype=np.float)

        # TODO try to update this automatically
        # Number of constraints
        self.num_boundcon = 0
        if WFLOP1_CONS_BOUNDARY:
            # assume the boundary arae is a 4 sided polygon
            self.num_boundcon += WFLOP1_NUMBER_ELEMENTS*4 + WFLOP1_NUMBER_ELEMENTS*4
        if WFLOP1_CONS_AREA:
            self.num_boundcon += 1
        if WFLOP1_CONS_PROXIMITY:
            self.num_boundcon += binomial_coefficient(WFLOP1_NUMBER_ELEMENTS, 2)

        self.e_terminate = event

    def run(self):

        # preallocate variables
        self.data = DataContainer(np.array([]), np.array([]), np.array([]), np.array([]),
                                  np.array([]), np.array([]), np.array([]))
        self.data_cons = [np.array([]) for _ in range(self.num_boundcon+3)]

        # iter    objective    inf_pr   inf_du lg(mu)  ||d||  lg(rg) alpha_du alpha_pr  ls
        self.data_iter = [np.array([]) for _ in range(10)]

        while not self.e_terminate.is_set():
            try:
                self.create_plots()
            except KeyboardInterrupt:
                logger.warning('Received KeyboardInterrupt')

                break
            except Exception as e:
                error_str = traceback.format_exc()
                logger.error(error_str)
                self.q_container['main'].put([e, error_str])
    
    def create_plots(self):
        logger.info('Creating plots')

        # global options
        # --------------
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')
        pg.setConfigOptions(antialias=True)

        # setup the window
        # ----------------
        # Always start by initializing Qt (only once per application)
        self.app = QtGui.QApplication([])
        self.window = pg.GraphicsWindow(title='Wind farm layout')
        self.window1 = pg.GraphicsWindow(title='Objective function')
        self.window2 = pg.GraphicsWindow(title='Constraints')
        self.win_ipopt = pg.GraphicsWindow(title='IPOPT iteration data')

        # setup the plots
        # ---------------

        # hornsrev
        if WFLOP_REGION in ['hornsrev', 'bathymetry'] :
            hr_offset = 2000.
            xmin = 423974. - hr_offset
            xmax = 429431. + hr_offset
            ymin = 6147543. - hr_offset
            ymax = 6151447. + hr_offset

            boundary_region_x = [xmin, xmax, xmax, xmin, xmin]
            boundary_region_y = [ymin, ymin, ymax, ymax, ymin]

        # sample wind farm
        elif WFLOP_REGION == 'simple':
            xmin = 0.
            xmax = 2025.
            ymin = -25.
            ymax = 2000.

            boundary_region_x = [xmin, xmax, xmax, xmin, xmin]
            boundary_region_y = [ymin, ymin, ymax, ymax, ymin]


        elif WFLOP_REGION == 'vesterhavsyd':

            boundary_region_x = []
            boundary_region_y = []
        else:
            logger.error('WFLOP_REGION = {} is unimplemented'.format(WFLOP_REGION))


        # wind farm layout
        self.layout_plot = self.window.addPlot(title='Wind farm layout')
        self.curve_layout = self.layout_plot.plot(pen=None, symbol='o')
        self.layout_plot.setXRange(xmin, xmax, padding=0.3)
        self.layout_plot.setYRange(ymin, ymax, padding=0.3)
        self.layout_boundary = self.layout_plot.plot(boundary_region_x,
                                                     boundary_region_y,
                                                     pen=pg.mkPen('k', width=4))
        if WFLOP_REGION == 'hornsrev':
            hr_x = np.array([423974.00, 424033.00, 424092.00, 424151.00, 424210.00, 424268.00, 424327.00,
                             424386.00, 424534.00, 424593.00, 424652.00, 424711.00, 424770.00, 424829.00,
                             424888.00, 424947.00, 425094.00, 425153.00, 425212.00, 425271.00, 425330.00,
                             425389.00, 425448.00, 425507.00, 425654.00, 425713.00, 425772.00, 425831.00,
                             425890.00, 425950.00, 426009.00, 426068.00, 426214.00, 426273.00, 426332.00,
                             426392.00, 426451.00, 426510.00, 426569.00, 426628.00, 426774.00, 426833.00,
                             426892.00, 426952.00, 427011.00, 427070.00, 427129.00, 427189.00, 427334.00,
                             427393.00, 427453.00, 427512.00, 427571.00, 427631.00, 427690.00, 427749.00,
                             427894.00, 427953.00, 428013.00, 428072.00, 428132.00, 428191.00, 428250.00,
                             428310.00, 428454.00, 428513.00, 428573.00, 428632.00, 428692.00, 428751.00,
                             428811.00, 428870.00, 429014.00, 429074.00, 429133.00, 429193.00, 429252.00,
                             429312.00, 429371.00, 429431.00], dtype=np.float)
            hr_y = np.array([6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148658.00, 6148101.00, 6147543.00,
                             6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148658.00, 6148101.00, 6147543.00,
                             6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148658.00, 6148101.00, 6147543.00,
                             6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00,
                             6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00,
                             6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00,
                             6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00,
                             6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00,
                             6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00,
                             6151447.00, 6150889.00, 6150332.00, 6149774.00, 6149216.00, 6148659.00, 6148101.00, 6147543.00],
                             dtype=np.float)
            self.layout_hr = self.layout_plot.plot(hr_x, hr_y, pen=None, symbol='x',
                    symbolBrush=pg.mkBrush(color='r'), symbolPen=pg.mkPen(None))


        # AEP
        self.plot_aep = self.window1.addPlot(title='AEP', colspan=3)
        self.curve_aep = self.plot_aep.plot(pen='k')
        self.window1.nextRow()

        # alpha
        self.plot_alpha = self.window1.addPlot(title='alpha')
        self.curve_alpha = self.plot_alpha.plot(pen='k')
        # beta
        self.plot_beta = self.window1.addPlot(title='beta')
        self.curve_beta = self.plot_beta.plot(pen='k')
        # bx
        self.plot_bx = self.window1.addPlot(title='bx')
        self.curve_bx = self.plot_bx.plot(pen='k')
        self.window1.nextRow()
        # by
        self.plot_by = self.window1.addPlot(title='by')
        self.curve_by = self.plot_by.plot(pen='k')
        # dx
        self.plot_dx = self.window1.addPlot(title='dx')
        self.curve_dx = self.plot_dx.plot(pen='k')
        # dy
        self.plot_dy = self.window1.addPlot(title='dy')
        self.curve_dy = self.plot_dy.plot(pen='k')

        # boundary constraints
        self.plot_boundary = self.window2.addPlot(title='Boundary constraints',
                                                  colspan=2)
        # TODO: figure out the number of boundary constraints here!!
        self.curve_bound = []
        for ic in range(self.num_boundcon):
            c = self.plot_boundary.plot()
            self.curve_bound.append(c)
        self.window2.nextRow()
        # area constraints
        self.plot_area = self.window2.addPlot(title='Area constraints')
        self.curve_area = self.plot_area.plot(pen='b')
        # proximity constraints
        self.plot_prox = self.window2.addPlot(title='Proximity constraints')
        self.curve_prox = self.plot_prox.plot(pen='r')

        # iteration data
        # ==============
        self.plot_iter1 = self.win_ipopt.addPlot(title='obj')
        self.curve_iter1 = self.plot_iter1.plot(pen='k') 

        self.plot_iter2 = self.win_ipopt.addPlot(title='inf_pr')
        #self.plot_iter2.setLogMode(y=True)
        self.curve_iter2 = self.plot_iter2.plot(pen='k') 

        self.plot_iter3 = self.win_ipopt.addPlot(title='inf_du')
        #self.plot_iter3.setLogMode(y=True)
        self.curve_iter3 = self.plot_iter3.plot(pen='k') 

        self.plot_iter4 = self.win_ipopt.addPlot(title='log(mu)')
        #self.plot_iter4.setLogMode(y=True)
        self.curve_iter4 = self.plot_iter4.plot(pen='k') 

        self.win_ipopt.nextRow()

        self.plot_iter5 = self.win_ipopt.addPlot(title='||d||')
        #self.plot_iter5.setLogMode(y=True)
        self.curve_iter5 = self.plot_iter5.plot(pen='k') 

        #self.plot_iter6 = self.win_ipopt.addPlot(title='log(rg)')
        #self.curve_iter6 = self.plot_iter6.plot(pen='k') 

        self.plot_iter7 = self.win_ipopt.addPlot(title='alpha_pr')
        #self.plot_iter7.setLogMode(y=True)
        self.plot_iter7.setYRange(0, 1, padding=0.1)
        self.curve_iter7 = self.plot_iter7.plot(pen='k') 

        self.plot_iter8 = self.win_ipopt.addPlot(title='alpha_du')
        #self.plot_iter8.setLogMode(y=True)
        self.plot_iter8.setYRange(0, 1, padding=0.1)
        self.curve_iter8 = self.plot_iter8.plot(pen='k') 

        self.plot_iter9 = self.win_ipopt.addPlot(title='ls')
        self.curve_iter9 = self.plot_iter9.plot(pen='k') 

        # plot updates
        self.timer_plot = QtCore.QTimer()
        self.timer_plot.timeout.connect(self.update_plots)
        self.timer_plot.start(20)

        # data updates
        self.timer_data = QtCore.QTimer()
        self.timer_data.timeout.connect(self.update_data)
        self.timer_data.start(10)

        # run event loop
        # this is blocking!!
        if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
            QtGui.QApplication.instance().exec_()

        logger.info('Stopping application')

    def update_data(self):

        if not self.e_terminate.is_set():
            if not self.q_container[self.id].empty():
                # get the data from queue
                signal = self.q_container[self.id].get()

                if 'objective' in signal:
                    data = signal['objective']
                    #self.data.layout.resize(data[0].shape, refcheck=False)
                    #self.data.layout[:] = - data[0]
                    self.layout_data = data[0]

                    self.data.aep.resize(len(self.data.aep) + 1)
                    self.data.aep[-1:] = - data[2]

                    self.data.alpha.resize(len(self.data.alpha) + 1)
                    self.data.alpha[-1:] = data[1][0]

                    self.data.beta.resize(len(self.data.beta) + 1)
                    self.data.beta[-1:] = data[1][1]

                    self.data.bx.resize(len(self.data.bx) + 1)
                    self.data.bx[-1:] = data[1][2]

                    self.data.by.resize(len(self.data.by) + 1)
                    self.data.by[-1:] = data[1][3]

                    self.data.dx.resize(len(self.data.dx) + 1)
                    self.data.dx[-1:] = data[1][4]

                    self.data.dy.resize(len(self.data.dy) + 1)
                    self.data.dy[-1:] = data[1][5]

                    if ARCHIVE_STATUS:
                        with open(os.path.join(ARCHIVE_FOLDER_PATH, 'progression.csv'), 'a') as f:
                            np.savetxt(f, np.transpose(
                                    [self.data.aep[-1:], self.data.alpha[-1:],
                                    self.data.beta[-1:], self.data.bx[-1:], self.data.by[-1:],
                                    self.data.dx[-1:], self.data.dy[-1:]]))
                        with open(os.path.join(ARCHIVE_FOLDER_PATH, 'layout.csv'), 'a') as f:
                            np.savetxt(f, self.layout_data.reshape((1,-1)))

                elif 'constraints' in signal:
                    data = signal['constraints']
                    #self.data_cons = np.concatenate((self.data_cons, data.transpose()))
                    for ic in range(len(data)):
                        self.data_cons[ic].resize(len(self.data_cons[ic])+1)
                        self.data_cons[ic][-1] = data[ic]

                    if ARCHIVE_STATUS:
                        tmp_cons_list = np.array([con[-1] for con in self.data_cons if len(con) > 0])
                        with open(os.path.join(ARCHIVE_FOLDER_PATH, 'constraints.csv'), 'a') as f:
                            np.savetxt(f, tmp_cons_list.reshape((1,-1)))

                # Ipopt iteration data
                elif 'callback' in signal:
                    data = signal['callback']
                    for ii in range(len(data)-1):
                        self.data_iter[ii].resize(len(self.data_iter[ii])+1)
                        self.data_iter[ii][-1] = data[ii+1]
                    if ARCHIVE_STATUS:
                        tmp_iter_list = np.array([iter[-1] for iter in self.data_iter if len(iter) > 0])
                        with open(os.path.join(ARCHIVE_FOLDER_PATH, 'iteration_data.csv'), 'a') as f:
                            np.savetxt(f, tmp_iter_list.reshape((1, -1)))

                else:
                    logger.error('Unknown key <{}> in data signal'.format(data.keys))
        else:
            logger.info('Stopping data timer')
            self.timer_data.stop()

            #if ARCHIVE_STATUS:
                # TODO finish this
            #   logger.info('Saving data from this run')
            #    
            #    #with open(os.path.join(ARCHIVE_FILE_PATH + '.csv')) as f:
            #    #    writer = csv.writer(f, delimiter=' ')
             
            #    np.savez(os.path.join(ARCHIVE_FILE_PATH + '.npz'),
            #                self.data.aep, self.data.alpha,
            #                self.data.beta, self.data.bx, self.data.by,
            #                self.data.dx, self.data.dy)
             
            #    np.savetxt(os.path.join(ARCHIVE_FILE_PATH + '.csv'),
            #                (self.data.aep, self.data.alpha,
            #                 self.data.beta, self.data.bx, self.data.by,
            #                 self.data.dx, self.data.dy))

    def update_plots(self):
        if not self.e_terminate.is_set():

            self.curve_layout.setData(self.layout_data[:self.n_elements],
                                    self.layout_data[self.n_elements:])

            self.curve_aep.setData(self.data.aep)
            self.curve_alpha.setData(self.data.alpha)
            self.curve_beta.setData(self.data.beta)
            self.curve_bx.setData(self.data.bx)
            self.curve_by.setData(self.data.by)
            self.curve_dx.setData(self.data.dx)
            self.curve_dy.setData(self.data.dy)

            # constraints window
            if WFLOP1_CONS_AREA:
                self.curve_area.setData(self.data_cons[-3])
            if WFLOP1_CONS_PROXIMITY:
                self.curve_prox.setData(self.data_cons[-2])
            if WFLOP1_CONS_BOUNDARY:
                for ic in range(self.num_boundcon):
                    self.curve_bound[ic].setData(self.data_cons[ic])

            # iteration window
            #logger.info(self.data_cons)
            #logger.info(self.data_iter)
            self.curve_iter1.setData(self.data_iter[1])
            self.curve_iter2.setData(self.data_iter[2])
            self.curve_iter3.setData(self.data_iter[3])
            self.curve_iter4.setData(self.data_iter[4])
            self.curve_iter5.setData(self.data_iter[5])
            #self.curve_iter6.setData(self.data_iter[6])
            self.curve_iter7.setData(self.data_iter[7])
            self.curve_iter8.setData(self.data_iter[8])
            self.curve_iter9.setData(self.data_iter[9])
        else:
            logger.info('Stopping plot timer')
            self.timer_plot.stop()


def run1():
    plt.axis([0, 1000, 0, 1])
    plt.ion()
    plt.show()

    for i in range(1000):
        y = np.random.random()
        plt.scatter(i, y)
        plt.draw()
        time.sleep(0.05)


def run_scatter():
    fig = plt.figure()
    ax = fig.add_subplot(111)

    # some X and Y data
    x = np.arange(10000)
    y = np.random.randn(10000)

    li, = ax.plot(x, y)

    # draw and show it
    fig.canvas.draw()
    #plt.ion()
    #plt.show()
    plt.show(block=False)

    # loop to update the data
    while True:
        try:
            y[:-10] = y[10:]
            y[-10:] = np.random.randn(10)

            # set the new data
            li.set_ydata(y)

            fig.canvas.draw()

            time.sleep(0.01)
        except KeyboardInterrupt:
            break


def run_pyqt():
    plotter = PlotterPyQt('non')
    plotter.start()

if __name__ == "__main__":
    #run_scatter()
    run_pyqt()



"""
Classes and functions responsible for running the optimization algorithms
"""
import multiprocessing
import time
import numpy as np
#import sys
import os
import traceback

from config import logger
from config import OPT_BACKEND
from config import PLOTTING_ENABLED
from config import IPOPT_PERTURBATION
from config import IPOPT_TOLERANCE
from workers import checkSignal
from WFLOP import WindFarm
from WFLOP import BaseWFLOP

if OPT_BACKEND.lower() == 'pyopt':
    import pyOpt
elif OPT_BACKEND.lower() == 'ipopt':
    import pyipopt


#def callback_fun(*args, **kwargs):
#    for arg in args:
#        logger.info(arg)
#    for k, v in kwargs:
#        logger.info(k, v)

class OptimizationWorker(multiprocessing.Process):

    Q = None  # container with all the worker input queues
    id = 'optimizer'  # id used to represent this class in queue container
    wf_layout = None

    def __init__(self, queue_container, wflop_class, terminate_event=None, *args, **kwargs):
        multiprocessing.Process.__init__(self)
        self.Q = queue_container

        assert issubclass(wflop_class, BaseWFLOP)

        # TODO I will probably have to change this
        #      to a list and dict
        self.wflop_class = wflop_class

        self.wflop_args = args
        self.wflop_kwargs = kwargs

        self.e_terminate = terminate_event


        logger.debug('Optimizer initiated')

    def run(self):
        logger.info('Starting')
        logger.info('Process PID = {}'.format(os.getpid()))

        self.c_problem = self.wflop_class(self.Q, *self.wflop_args, **self.wflop_kwargs)

        try:
            if OPT_BACKEND.lower() == 'pyopt':
                logger.info('Using pyOpt optimizer')
                self.pyopt_run_optimization()
            elif OPT_BACKEND.lower() == 'ipopt':
                logger.info('Using IPOPT optimizer')
                self.ipopt_run_optimization()
            else:
                logger.error('{} optimizer not implemented'.format(OPT_BACKEND))

        except KeyboardInterrupt:
            logger.warning('Received KeyboardInterrupt')
        except Exception as e:
            error_str = traceback.format_exc()
            logger.error(error_str)
            self.Q['main'].put([e, error_str])

    def ipopt_run_optimization(self):
        """
        Runs IPOPT optimization
        Needs variables: (n, xl, xu, m, gl, gu, nnzj, nnzh, x0) from get_initial_setup
        and functions: confun, jac_confun, objfun, grad_objfun 
        """
        n, xl, xu, m, gl, gu, nnzj, nnzh, x0 = self.c_problem.initial_setup()

        logger.info('Starting IPOPT optimization')

        nlp = pyipopt.create(n, xl, xu, m, gl, gu, nnzj, nnzh,
                self.c_problem.objfun, self.c_problem.grad_objfun,
                self.c_problem.confun, self.c_problem.jac_confun)
        pyipopt.set_loglevel(1)

        # IPOPT OPTIONS
        # -------------
        #nlp.str_option('derivative_test', 'first-order')
        #nlp.num_option('derivative_test_perturbation', IPOPT_PERTURBATION)
        #nlp.str_option('derivative_test_print_all', 'yes')

        #nlp.str_option('print_options_documentation', 'yes')

        # equality constraints
        #nlp.str_option('fixed_variable_treatment', 'make_constraint')
        #nlp.str_option('fixed_variable_treatment', 'relax_bounds')

        #nlp.int_option('max_iter', 60)
        nlp.num_option('tol', IPOPT_TOLERANCE)
        #nlp.str_option('linear_solver', 'mumps')

        cf = self.ipopt_get_callback_fun()
        nlp.set_intermediate_callback(cf)

        #time.sleep(99999999)

        logger.info('Starting the solver')
        x, zl, zu, constraint_multipliers, obj, status = nlp.solve(x0)
        logger.info('Stopping the solver')
        nlp.close()

        logger.info('Solved IPOPT problem')
        logger.info('x={}, zl={}, zu={}, constraint_multipliers={}, obj={}, status={}'
                    .format(x, zl, zu, constraint_multipliers, obj, status))

        # emit a terminate signal?
        try:
            logger.info('Setting terminate event')
            self.e_terminate.set()
        except AttributeError:
            # self.e_terminate = None
            logger.debug('Terminate event is not set')

    def ipopt_get_callback_fun(self):
        """
        Returns a callback function for pyipopt
        """
        if PLOTTING_ENABLED:
            def callback_fun(*args):
                s = {'callback': args}
                self.Q['plotter'].put(s)
        else:
            def callback_fun(*args):
                pass
        return callback_fun

    def pyopt_run_optimization(self, *args, **kwargs):
        """Function running the main optimization algorithm"""

        # TODO get this from Colonel using 'get turbines list'
        NumberWT = 8    # number of WT

        WFLayout = None

        # send request for turbine list
        rDict = {'SignalName': 'getTurbineList',
                'SignalData': None,
                'SignalOrigin': 'Optimizer',
                'SignalTarget': '0'}
        self.Q['distributor'].put(rDict)
        logger.debug('Sent turbine list request')

        # TODO: use Q.get() here to get the initial WF state
        initialSignal = self.Q[self.id].get()
        if checkSignal(initialSignal) != 0:
            logger.error('Received wrong initial wind farm layout')
        
        InitialWFlayout = initialSignal['SignalData']

        if InitialWFlayout['WFName'] in ['Initial', 'Turbine List']:
            logger.debug('Received initial WF layout')
            WFLayout = WindFarm(self.Q,
                                initialLayout=InitialWFlayout,
                                NumberWT=NumberWT)
        else:
            logger.error('Non-initial layout in queue')

        opt_prob = pyOpt.Optimization('Basic WF optimization', self.pyopt_optimizationfunction)

        # add variables
        opt_prob.addVarGroup('x', NumberWT)
        opt_prob.addVarGroup('y', NumberWT)

        # add objective function
        opt_prob.addObj('AEP')

        # add constraints
        opt_prob.addConGroup('Geom. boundaries', 1)

        # print problem setup
        logger.info(opt_prob)

        logger.info('Starting optimization')

        # run the optimizer
        # initiate the solver
        optimizer = pyOpt.SLSQP()

        # setup
        #optimizer(opt_prob, sens_type='FD',
        #        queue_in = Q_in,
        #        WFLayout=WFLayout)
        x_tmp = np.array([ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0., 0.,  0.,  0.])
        
        logger.debug('Manually calculating gradient')

        gf, gc, status = self.pyopt_gradfun(x_tmp, 0, 0, self.Q)

        logger.debug('Gradient calculated as: gf={}, gc={}'.format(gf, gc))

        #sys.exit()
        
        optimizer(opt_prob, sens_type=self.pyopt_gradfun,
                queue_container=self.Q,
                WFLayout=WFLayout)

        logger.info('{}'.format(opt_prob.solution[0]))
        # TODO add optimizer error catching

    def pyopt_optimizationfunction(self, X, queue_container, WFLayout, *args, **kwargs):
        """ pyOpt wrapper around objective and constraints functions """
        Q = queue_container
        status = [None, None]

        tic = time.time()

        f, status[0] = self.pyopt_objfun(X, Q, WFLayout, *args, **kwargs)
        g, status[1] = self.pyopt_confun(X, *args, **kwargs)

        # check if the objective function and constraints were properly calculated
        if not all(status):
            status = 0
        else:
            status = 1

        logger.debug('Optimization took {}s'.format(time.time()-tic))

        logger.info('OptimizationFunction: Finished.')
        logger.debug('f=<{}>'.format(f))
        logger.debug('g=<{}>'.format(g))
        logger.debug('status=<{}>'.format(status))

        return f, g, status

    def pyopt_confun(self, X, *args, **kwargs):
        # TODO: implement constraints
        g = [0]*1
        g[0] = X[0] - X[1] - 20
        status = 0

        return g, status

    def pyopt_gradfun(self, x, f, c, queue_container, *args, **kwargs):
        """Custom function for calculating gradients needed by pyOpt"""
        Q = queue_container
        
        #for i, con in enumerate(args):
        #    logger.debug('{}:{}'.format(i, con))

        #for i, con in kwargs.items():
        #    logger.debug('{}:{}'.format(i, con))

        grad_f, _ = self.pyopt_gradobj(x, queue_container=Q)
        grad_c, _ = self.pyopt_gradcon(x)
        status = 0

        return grad_f, grad_c, status

    def pyopt_gradcon(self, x, *args, **kwargs):
        grad_c = [None] * len(x)
        for idx, ix in enumerate(x):
            grad_c[idx] = 1
        
        status = 0

        return grad_c, status

    def pyopt_gradobj(self, x, queue_container, *args, **kwargs):
        """Calculates the gradient of objective function"""
        Q = queue_container

        d = {'SignalName': 'Gradient',
            'SignalData': x,
            'SignalOrigin': 'Optimizer'}

        # send a request to calculate the gradient at x
        Q['distributor'].put(d)

        # wait for the gradient to be calculated
        logger.debug('Waiting for gradient')

        grad_signal = Q[self.id].get()

        logger.debug('Gradient received')
        
        grad_fun = grad_signal['SignalData']
        status = 0

        return grad_fun, status

    def pyopt_objfun(self, X, queue_container, WFLayout, *args, **kwargs):
        Q = queue_container
        status = 1

        # process the X vector into colonel input file
        logger.debug('Process the X vector into colonel input file')
        logger.debug('{}'.format(X))
        
        WFLayout.setCoordinates(X)
        WFLayout.requestAEP()

        # get the AEP
        logger.debug('Waiting for AEP data from queue')
        queueData = Q[self.id].get()
        
        #logger.debug('Objective function: received <{}>'
        #             .format(queueData))
        AEP = None
        if checkSignal(queueData) == 0:
            if queueData['SignalData']['Name'] == 'AEP':
                # <net AEP (GWh)> < gross AEP (GWh)> <capacity factor> 
                AEP = queueData['SignalData']['Data'][0]
                status = 0
            else:
                logger.error('ObjectiveFunction received wrong data <{}>'
                            .format(queueData))
        f = AEP

        logger.debug('Objective function: received <{}>, f={}, status={}'
                    .format(queueData, f, status))

        return f, status

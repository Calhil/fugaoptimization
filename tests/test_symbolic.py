import sympy as sp
import numpy as np
import matplotlib.pyplot as plt

#import numpy as np
import Symbolic as sb
from config import logger
from config import WFLOP1_NUMBER_ELEMENTS
from config import WFLOP1_MAX_ELEMENTS_PER_ROW
from Symbolic import Point


def run_wflop1_test():
    import timeit
    sp.pprint_use_unicode()
    logger.info('Testing WFLOP#1')
    
    #p_poly = sp.Matrix([[0, 10,  0, -2.5, -5],
    #                    [0, 10, 15, 25,  5]])
    p_poly = sp.Matrix([[0, 2000, 2000,    0],
                        [0,    0, 2000, 2000]])

    vars = sb.WFLOP1_vars()
    cons = sb.WFLOP1_constraints(vars, boundary_vertices=p_poly)
    jac_con = sb.get_jaccon_expr(vars, cons)

    logger.info('Constraints:')
    sp.pprint(cons)
    logger.info('Jacobian of the constraints')
    sp.pprint(jac_con)

    jaccon_expr = sb.get_jaccon_expr(vars, cons)

    #jacfun_numpy = jaccon_gen_numpy(vars, cons)
    jacfun_numpy = sb.get_jaccon_fun(vars, jac_constraints=jaccon_expr)
    #jacfun_sympy = jaccon_gen_sympy(vars, cons)
    #jacfun_fortran = jaccon_gen_fortran(vars, cons)

    logger.info('Positions of the nonzero values in the jacobian matrix')
    sp.pprint(sb.get_jac_nz_pos(jaccon_expr))

    vars_0 = [1., 1., 1., 1., 1., 1.]

    #logger.info(jacfun_numpy(*vars_0))
    sp.pprint(jacfun_numpy(*vars_0))
    #logger.info(jacfun_sympy(vars_0))
    #sp.pprint(jacfun_sympy(vars_0))
    #logger.info(jacfun_fortran(*vars_0))

    t_numpy = timeit.Timer(lambda: jacfun_numpy(*vars_0))
    #t_sympy = timeit.Timer(lambda: jacfun_sympy(vars_0))
    #t_fortran= timeit.Timer(lambda: jacfun_fortran(vars))
    
    n_exec = 1000
    logger.info('jaccon_numpy: {}s'.format(t_numpy.timeit(number=n_exec)/n_exec))
    #logger.info('jaccon_sympy: {}s'.format(t_sympy.timeit(number=n_exec)/n_exec))
    #logger.info('jaccon_fortran: {}s'.format(t_fortran.timeit(number=n_exec)/n_exec))


def run1():
    import timeit
    sp.pprint_use_unicode()

    vars = sb.WFLOP1_vars()
    f_dxdvars = sb.get_dxdvars_fun(vars, 20, 4)
    print f_dxdvars

    vars_0 = [1., 1., 1., 1., 1., 1.]
    sp.pprint(f_dxdvars(*vars_0))

    t_numpy = timeit.Timer(lambda: f_dxdvars(*vars_0))

    n_exec = 10000
    logger.info('f_dxdvars (numpy): {}s'.format(t_numpy.timeit(number=n_exec)/n_exec))


def run2():
    from math import floor
    from collections import namedtuple
    import matplotlib.pyplot as plt

    sp.pprint_use_unicode()

    Point = namedtuple('Point', ['x', 'y'])

    # arguments
    n_elements = WFLOP1_NUMBER_ELEMENTS
    max_col = WFLOP1_MAX_ELEMENTS_PER_ROW

    alpha, beta, bx, by, dx, dy = sp.symbols('alpha, beta b_x, b_y, d_x, d_y')
    vars = [alpha, beta, bx, by, dx, dy]
    #vars_0 = [75*(sp.pi/180), sp.pi/3, 3, 3, 0, 1]
    #vars_0 = [2.00000000e+00, -3.14159265e+00, 5.04337980e+02, 7.50000000e+02, 6.32162213e+02, 8.05073986e-09]
    #vars_0 = [2.90165037e-01, -3.14159192e+00, 3.38546369e+02, 1.39940342e+02, 5.23405162e+02, 5.36380586e+02]
    vars_0 = [5.58049473e-01, -3.14159268e+00,  1.31498352e+02, 2.60871415e+02, 8.63105613e+02, 1.33846418e+03]

    sub_d = dict(zip(vars, vars_0))
    # 1) Get the WF points and boundary vertices
    # boundary points
    #a = Point(0, 0)
    #b = Point(10, 10)

    #p_poly = sp.Matrix([[0, 10, 10, 0, 0],
    #                    [0,  0, 10, 10, 0]])
    #p_poly = sp.Matrix([[0, 10,  0, -2.5, -5, 0],
    #                    [0, 10, 15, 25,  5, 0]])
    p_poly = sp.Matrix([[0, 2000, 2000,    0, 0],
                        [0,    0, 2000, 2000, 0]])

    # get point coordinates
    #p_matrix = [[0, 0],
    #            [0, max_col-1],
    #            [int(floor(n_elements/max_col)), 0],
    #            [int(floor(n_elements/max_col)), max_col-1]]

    p_matrix = sp.Matrix([[0, 0, int(floor(n_elements/max_col))-1, int(floor(n_elements/max_col))-1],
                          [0, max_col-1, 0, max_col-1],
                          [1, 1, 1, 1]])
    #p_matrix = sp.Matrix([[0, 0, int(floor(n_elements/max_col)), int(floor(n_elements/max_col))],
    #                      [0, max_col-1, 0, max_col-1],
    #                      [1, 1, 1, 1]])

    sp.pprint(p_matrix)

    p_matrix[0,:] *= bx
    p_matrix[1,:] *= by
    sp.pprint(p_matrix)

    T_rot = sb.rotation_matrix(alpha)
    T_shear = sb.shear_matrix(beta)
    T_trans = sb.translation_matrix(dx, dy)

    p_trans = T_trans*T_rot*T_shear*p_matrix
    p_trans.row_del(2)
    sp.pprint(p_trans.evalf(subs=sub_d))


    # determine signs for a point inside the polygon
    p_center = Point(sum(p_poly[0,:])/p_poly.cols, sum(p_poly[1,:])/p_poly.cols)
    #print p_center

    signs = []
    c_l = []    # lower constraint bounds
    c_u = []    # upper constraint bounds
    for lcol in range(p_poly.cols-1):
        a = Point(p_poly[0, lcol], p_poly[1, lcol])
        # next point
        b = Point(p_poly[0, lcol+1], p_poly[1, lcol+1])

        sign = sb.dist_point_line(p_center, a, b)
        signs.append(sign)

        # determine the constraint bounds
        if sign < 0:
            c_l.append('-inf')
            c_u.append(0)
        else:
            c_l.append(0)
            c_u.append('inf')

    signs =  np.sign(signs)
    #print signs

    #print 'Constraint bounds'
    #print c_l
    #print c_u

    # determine distance
    d_expr = sp.zeros(p_trans.cols*(p_poly.cols-1), 1)

    for p_col in range(p_trans.cols):
        p = Point(p_trans[0, p_col], p_trans[1, p_col])

        print 'Point p{}({}, {})'.format(p_col, p.x.evalf(subs=dict(zip(vars, vars_0))), p.y.evalf(subs=dict(zip(vars, vars_0))))
        pinpoly = []
        for l_col in range(p_poly.cols-1):
            print 'Distance of point p{} to line l{}: d ='.format(p_col, l_col),
            # this point
            a = Point(p_poly[0, l_col], p_poly[1, l_col])
            # next point
            b = Point(p_poly[0, l_col+1], p_poly[1, l_col+1])

            #d = ((b.y - a.y)*p.x - (b.x - a.x)*p.y + b.x*a.y - b.y*a.x)# / sp.sqrt((b.y - a.y)**2 + (b.x - a.x)**2)
            d = sb.dist_point_line(p, a, b)
            print d.evalf(subs=dict(zip(vars, vars_0)))
            
            p_sign = np.sign(d.evalf(subs=dict(zip(vars, vars_0))))
            if p_sign in [0, signs[l_col]]:
                pinpoly.append(True)
            else:
                pinpoly.append(False)

            # get distance equations
            d_expr[p_col*(p_poly.cols-1) + l_col] = d

        if all(pinpoly):
            print 'Point p{} is inside polygon'.format(p_col)
        else:
            print 'Point p{} is outside polygon'.format(p_col)

        print
    
    #d_simp = sp.simplify(d_expr)
    #sp.pprint(d_simp)

    #d_jac = sp.simplify(d_simp.jacobian(vars))
    #sp.pprint(d_jac)

    #sp.pprint(d_simp.evalf(subs=dict(zip(vars, vars_0))))
    #print get_jac_nz_pos(d_jac)

    # plotting
    x = np.array(p_trans[0, :].evalf(subs=dict(zip(vars, vars_0))).tolist())
    y = np.array(p_trans[1, :].evalf(subs=dict(zip(vars, vars_0))).tolist())

    polyx = np.array(np.array(p_poly[0,:].evalf().tolist())[0], dtype=np.float)
    polyy = np.array(np.array(p_poly[1,:].evalf().tolist())[0], dtype=np.float)

    print 'Points coordinates'
    sp.pprint(x)
    sp.pprint(y)

    plt.figure()
    plt.scatter(x, y)
    plt.scatter(p_center.x, p_center.y, c='r')
    plt.plot(polyx, polyy)
    plt.show()


def run3():
    sp.pprint_use_unicode()

    alpha, beta, bx, by, dx, dy = sp.symbols('alpha, beta b_x, b_y, d_x, d_y')
    vars = [alpha, beta, bx, by, dx, dy]
    p_poly = sp.Matrix([[0, 10,  0, -2.5, -5],
                        [0, 10, 15, 25,  5]])

    b_expr = sb.get_boundary_constraints_expr(vars, p_poly)
    b_l, b_u  = sb.get_boundary_bounds(p_poly)

    sp.pprint(b_expr)
    print
    print b_l
    print b_u


def run_area():
    sp.pprint_use_unicode()

    # define points
    vars = sb.WFLOP1_vars()
    vertices = sb.get_wf_vertices(vars)

    sp.pprint(sp.simplify(vertices))

    vars0 = [0, 0, 10, 10, 0, 0]
    sub_dict = dict(zip(vars, vars0))

    vert_num = vertices.evalf(subs=sub_dict)
    sp.pprint(vert_num)

    v1 = sb.get_vector(vertices[:,0], vertices[:,1])
    v2 = sb.get_vector(vertices[:,0], vertices[:,2])

    sp.pprint(v1)
    sp.pprint(v2)

    area = sb.get_area_from_vectors(v1, v2)

    sp.pprint(area)
    sp.pprint(area.evalf(subs=sub_dict))
    
    jac_area = sp.Matrix([area]).jacobian(vars)
    sp.pprint(jac_area)
    sp.pprint(sp.simplify(jac_area))
    sp.pprint(jac_area.evalf(subs=sub_dict))

    # proximity expr
    logger.info('Proximity constraints')
    prox = sb.get_proximity_expr_wflop1(vars)
    sp.pprint(prox)
    logger.info('Jacobian of the proximity constraints')
    jacprox = sb.get_jac_proximity(vars, prox)
    sp.pprint(jacprox)


def __test_jaccon():
    #Point = namedtuple('Point', ['x', 'y'])

    # arguments
    n_elements = 12
    max_col = 4

    alpha, beta, bx, by, dx, dy = sp.symbols('alpha, beta b_x, b_y, d_x, d_y')
    vars = [alpha, beta, bx, by, dx, dy]
    #vars_0 = [75*(sp.pi/180), sp.pi/3, 3, 3, 0, 1]
    #vars_0 = [sp.pi, 0., 400, 400, 1000., 1000.]
    vars_0 = [5.27905512e+00, -6.63611642e-01,
              4.05661984e+02, 4.05968801e+02,
              1.00823295e+03, 9.93951027e+02]
    #boundary points
    #p_poly = sp.Matrix([[0, 10,  0, -2.5, -5, 0],
    #                    [0, 10, 15, 25,  5, 0]])

    p_poly = [[0, 2000, 2000,    0],
              [0,    0, 2000, 2000]]
    #wf_vert = get_wf_vertices(vars, n_elements=n_elements, max_col=max_col)

    cons = sb.WFLOP1_constraints(vars, p_poly, n_elements=n_elements, max_col=max_col)

    jac_con = sb.get_jaccon_expr(vars, cons)

    nz_pos = sb.get_jac_nz_pos(jac_con)

    sp.pprint(jac_con)
    sp.pprint(nz_pos)
    print nz_pos
    print jac_con.evalf(subs=dict(zip(vars, vars_0)))

    sp.pprint(jac_con.evalf(subs=dict(zip(vars, vars_0))))

    # print nonzero structure
    jac_con.print_nonzero()


def __test_scaling_cons():
    logger.info('Testing scaling')
    sp.pprint_use_unicode()
    from config import WFLOP1_SCALING as scaling 

    bdypts = [[0, 2000, 2000,    0],
              [0,    0, 2000, 2000]]
    
    vars = sb.WFLOP1_vars()
    cons = sb.WFLOP1_constraints(vars, boundary_vertices=bdypts, scaling_factor=scaling)

    sp.pprint(cons)


def __test_cons_wflop1():
    sp.pprint_use_unicode()
    import NumericalMethods as nm
    import numpy as np
    import matplotlib.pyplot as plt

    # arguments
    n_elements = WFLOP1_NUMBER_ELEMENTS
    max_col = WFLOP1_MAX_ELEMENTS_PER_ROW

    alpha, beta, bx, by, dx, dy = sp.symbols('alpha, beta b_x, b_y, d_x, d_y')
    bdypts = sp.Matrix([[0, 2000, 2000,    0],
                        [0,    0, 2000, 2000]])
    #bdypts = np.array([[0, 2000, 2000,    0],
    #                   [0,    0, 2000, 2000]])
    #vars = [alpha, beta, bx, by, dx, dy]
    #vars0 = [2.00000000e+00, -3.14159265e+00, 5.04337980e+02, 7.50000000e+02, 6.32162213e+02, 8.05073986e-09]
    #vars0 = [2.90165037e-01, -3.14159192e+00, 3.38546369e+02, 1.39940342e+02, 5.23405162e+02, 5.36380586e+02]
    vars0 = [5.58049473e-01, -3.14159268e+00,  1.31498352e+02, 2.60871415e+02, 8.63105613e+02, 1.33846418e+03]
    # 5.57871933e-01  -3.14159268e+00   1.31517402e+02   2.60900269e+02 8.63110708e+02   1.33846421e+03

    #cons = WFLOP1_constraints(vars, bdypts, n_elements=n_elements, max_col=max_col)
    #confun = get_confun(vars, cons)
    #cons0 = confun(*vars0)

    #sp.pprint(cons0)
    #print len(cons0)

    x = nm.get_wt_coordinates(*vars0, n_elements=n_elements, max_col=max_col)

    print 'Points coordinates'
    tmp = np.vstack([x[:n_elements], x[n_elements:]]).T
    print tmp[0, :]
    print tmp[max_col,:]
    print tmp[-max_col,:]
    print tmp[-1, :] 

    # plotting
    plt.figure()
    plt.plot(bdypts[0,:].tolist()[0], bdypts[1,:].tolist()[0])
    plt.scatter(x[:n_elements], x[n_elements:])

    plt.xlim(-500, 3000)
    plt.ylim(-500, 3000)
    plt.show()


def __test_cons_wflop2():
    sp.pprint_use_unicode()

    n_elements = 1
    vars = sb.WFLOP2_vars(n_elements)
    vars0 = [100*ii for ii in range(n_elements)] + [100*ii for ii in range(n_elements)]
    
    cons = sb.WFLOP2_constraints(vars)
    jaccons = sb.get_jaccon_expr(vars, cons)
    confun = sb.get_confun(vars, cons)
    jacfun = sb.get_jaccon_fun(vars, jac_constraints=jaccons)


    sp.pprint(vars)
    sp.pprint(vars0)
    sp.pprint(cons)
    sp.pprint(confun(*vars0))
    print len(cons)
    sp.pprint(jaccons)
    sp.pprint(jacfun(*vars0))


def __test_winding_number():
    logger.info('Testing winding number\n')
    
    # simple, convex polygon
    bdypts = sp.Matrix([[0, 2000, 2000,    0],
                        [0,    0, 2000, 2000]])
    # nonconvex polygon
    bdypts = sp.Matrix([[0, 2000, 1000, 2000,    0],
                        [0,    0,  500, 2000, 2000]])

    vertices = sp.Matrix([[500, 1750, 1500, 500,  2500, 2500, 2001],
                          [0,    200, 1250, 1000,    0, 2500, 1000]])

    for i_vert in range(vertices.cols):
        p1 = Point(vertices[0, i_vert], vertices[1, i_vert])

        alpha = 0.
        for i_line in range(bdypts.cols):
            logger.info('i_line = {}'.format(i_line))
            p2 = Point(bdypts[0, i_line], bdypts[1, i_line])
            if i_line < bdypts.cols-1:
                p3 = Point(bdypts[0, i_line+1], bdypts[1, i_line+1])
            else:
                p3 = Point(bdypts[0, 0], bdypts[1, 0])

            logger.info('p1 = {}'.format(p1))
            logger.info('p2 = {}'.format(p2))
            logger.info('p3 = {}'.format(p3))

            v1 = sb.get_vector(p1, p2)
            v2 = sb.get_vector(p1, p3)

            angle = sb.angle_between_vectors(v1, v2)

            logger.info('angle(v1, v2) = {} ({}deg)'.format(angle.evalf(), angle.evalf()*180/np.pi))
            alpha += angle

        logger.info('alpha = {} == 2pi? {}'.format(alpha.evalf(), alpha.evalf() == 2*np.pi))
        print

    # symbolic exprs
    # --------------
    vars = sb.WFLOP1_vars()
    bcons = sb.get_boundary_constraints_windnum_expr(vars, bdypts, simplify_expr=False)
    #sp.pprint(bcons)
    jaccons = sb.get_jaccon_expr(vars, bcons, simplify_expr=False)
    #sp.pprint(jaccons)
    logger.info('Structure of the jacobian')
    sp.pprint(jaccons.print_nonzero())


    # plotting
    # --------
    plt.plot(bdypts[0,:].tolist()[0] + [0], bdypts[1,:].tolist()[0]+[0])
    plt.plot(vertices[0,:].tolist()[0], vertices[1,:].tolist()[0], 'ro')
    plt.show()




if __name__ == '__main__':

    run2()
    #__test_winding_number()
    __test_scaling_cons()


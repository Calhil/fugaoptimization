import numpy as np
import sympy as sp

from config import (WFLOP1_NUMBER_ELEMENTS,
                    WFLOP1_MAX_ELEMENTS_PER_ROW,
                    CD_COEFF)
from tests.test_gradients import (fd_central,
                                  dummy_windfarm)
from Symbolic import (WFLOP1_vars,
                      WFLOP1_dxdvars,
                      #wf_coordinates,
                      expr_gen_numpy)
from NumericalMethods import (get_wt_coordinates)

sp.pprint_use_unicode()
np.set_printoptions(linewidth=150, precision=4)

# TODO:
# implement normal wind farm next
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1





def fd_central_vars(x, wf, fd_order=0,
                    x_pert=[0.000001, 0.000001, 0.00001, 0.0001, 0.000001, 0.000001]):
    """ Calculate AEP gradient using finite difference """
    assert 0 <= fd_order <= len(CD_COEFF) - 1
    
    grad_fd = np.zeros_like(x)
    # perturbation matrix, assumes x_pert=y_pert
    h = range(-len(CD_COEFF[fd_order])/2, len(CD_COEFF[fd_order])/2 + 1)
    h.remove(0)
    #h = np.array(h)*x_pert

    pert = [np.array(h)*x_pert[i] for i in range(len(x_pert))]

    for ix in range(len(x)):
        grad_tmp = np.zeros_like(CD_COEFF[fd_order])
        for ic in range(len(CD_COEFF[fd_order])):
            #logger.info('ix = {}, ic = {}'.format(ix, ic))

            x_tmp = np.array(x)
            x_tmp[ix] += pert[ix][ic]
            #print x_tmp

            x_from_vars = get_wt_coordinates(*x_tmp,
                                             n_elements=WFLOP1_NUMBER_ELEMENTS,
                                             max_col=WFLOP1_MAX_ELEMENTS_PER_ROW)

            AEP = wf.get_AEP(x_from_vars)
            #logger.info('AEP = <{}> at x = <{}>'.format(AEP, x_tmp))
            grad_tmp[ic] = AEP * CD_COEFF[fd_order][ic]
        #print grad_tmp
        grad_fd[ix] = np.sum(grad_tmp) / (1*x_pert[ix])

    return grad_fd


if __name__ == "__main__":

    windfarm = dummy_windfarm(WFLOP1_NUMBER_ELEMENTS, poly_order=5)

    #vars0 = [0., 0., 50., 50., 200., 200.]
    vars0 = [10.*np.pi/180, 80.*np.pi/180, 500., 500., 20., 20.]
    #x0 = wf_coordinates(vars0)
    x0 = get_wt_coordinates(*vars0, n_elements=WFLOP1_NUMBER_ELEMENTS,
            max_col=WFLOP1_MAX_ELEMENTS_PER_ROW)

    print 'vars0:'
    print vars0
    print 'vars0 -> x0:'
    print x0

    #x0 = [100*i for i in range(WFLOP1_NUMBER_ELEMENTS*2)]
    #x0 = np.array(x0, dtype=np.float)

    grad_cd = fd_central(x0, windfarm, fd_order=1)
    grad_an = windfarm.get_gradAEP(x0)

    print '\nGradient analytical:'
    print grad_an
    print 'Gradient numerical:'
    print grad_cd

    # gradient (analytical) in the vars coordinate system
    vars = WFLOP1_vars()
    dxdvars_expr = WFLOP1_dxdvars(vars, WFLOP1_NUMBER_ELEMENTS, WFLOP1_MAX_ELEMENTS_PER_ROW,
                                  simplify_expr=False)
    dxdvars_fun = expr_gen_numpy(vars, dxdvars_expr)
    dxdvars_eval = dxdvars_fun(*vars0)
    print '\ndx/dvars(vars0):'
    sp.pprint(dxdvars_eval)
    grad_vars_cd = grad_cd*dxdvars_eval
    grad_vars_an = grad_an*dxdvars_eval

    # gradient (numerical) in the vars coordinate system obtained using FD
    grad_fd_vars = fd_central_vars(vars0, windfarm, fd_order=3)

    # print gradients
    print '\nGradients wrt vars:\n---------'

    print '\nGradient analytical, chain rule:'
    sp.pprint(grad_vars_an)

    print '\nGradient numerical, chain rule:'
    sp.pprint(grad_vars_cd)

    print '\nGradient fd, no chain rule:'
    sp.pprint(grad_fd_vars)

    sp.pprint(grad_vars_cd - grad_vars_an)

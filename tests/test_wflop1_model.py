import pyqtgraph as pg
from pyqtgraph import QtCore, QtGui
import numpy as np
import latex
import sympy as sp

import NumericalMethods as nm
import Symbolic as sb

from config import WFLOP1_NUMBER_ELEMENTS, WFLOP1_MAX_ELEMENTS_PER_ROW

sp.pprint_use_unicode()
np.set_printoptions(linewidth=150, precision=1)


alpha, beta, bx, by, dx, dy = 0., 0., 0., 0., 0., 0.
method = 'numerical'
method = 'symbolic'

# constraints
wflop1_vars = sb.WFLOP1_vars()
boundary_poly = np.array([[0., 2000., 2000.,    0.],
                          [0.,    0., 2000., 2000.]], dtype=np.float)

excluded_poly = np.array([[0., 2000., 2000.,    0.],
                          [0.,    0., 1000., 1000.]], dtype=np.float)


cons_expr = sb.WFLOP1_constraints(wflop1_vars, boundary_poly,
                                n_elements=WFLOP1_NUMBER_ELEMENTS,
                                max_col=WFLOP1_MAX_ELEMENTS_PER_ROW)
#excl_con_expr = sb.get_boundary_constraints_expr(wflop1_vars, excluded_poly)


con_fun = sb.get_confun(wflop1_vars, cons_expr)
#con_fun = sb.get_confun(wflop1_vars, excl_con_expr)

con_data = [np.array([]) for _ in range(WFLOP1_NUMBER_ELEMENTS*4)]

wt_coordinates_expr = sb.wf_coordinates(wflop1_vars)

eval_coords = sb.expr_gen_numpy(wflop1_vars, wt_coordinates_expr)


def save_latex_to_pdf(expr, filename):
    output_expr = r"""\documentclass[12pt]{article} 

    \usepackage{amsmath}
    \usepackage{amsfonts}
    \usepackage{euler}

    \begin{document}

    """ + sp.latex(expr, mode='inline') + \
    r"""
    \end{document}
    """

    pdf = latex.build_pdf(output_expr)
    pdf.save_to(filename)


def valueChanged():
    vars = [None]*8
    for i, spin in enumerate(spins):
        #print i, spin[1].value()
        vars[i] = spin[1].value()

    if method == 'numerical':
        x = nm.get_wt_coordinates(vars[0]*np.pi/180., vars[1]*np.pi/180., 
                                  vars[2], vars[3], vars[4], vars[5],
                                  n_elements=vars[6],
                                  max_col=vars[7])
    elif method == 'symbolic':
        x = sb.wf_coordinates([vars[0]*np.pi/180., vars[1]*np.pi/180., 
                               vars[2], vars[3], vars[4], vars[5]],
                               n_elements=vars[6],
                               max_col=vars[7])

    n_elements = vars[6]
    curve_layout.setData(x[:n_elements], x[n_elements:])

    #constraints
    con_eval = con_fun(vars[0]*np.pi/180., vars[1]*np.pi/180., 
                       vars[2], vars[3], vars[4], vars[5])

    sp.pprint(np.hstack((con_eval, con_eval > 0.) ))
    print vars
    print eval_coords(vars[0]*np.pi/180., vars[1]*np.pi/180., 
                      vars[2], vars[3], vars[4], vars[5])
    print x
    print '==============================================='
    print

    for ic in range(WFLOP1_NUMBER_ELEMENTS*4):
        # append data
        con_data[ic].resize( len(con_data[ic]) + 1)
        con_data[ic][-1] = con_eval[ic]
        # plot the data
        curve_con[ic].setData(con_data[ic])


if __name__ == "__main__":

    save_latex_to_pdf(cons_expr, '/dev/shm/constraints.pdf')
    save_latex_to_pdf(wt_coordinates_expr.T, '/dev/shm/wfcoordinates.pdf')

    app = QtGui.QApplication([])

    pg.setConfigOption('background', 'w')
    pg.setConfigOption('foreground', 'k')
    pg.setConfigOptions(antialias=True)

    spins = [
        ("alpha", pg.SpinBox(value=0.0, bounds=[-360, 360], minStep=1, step=10)),
        ("beta", pg.SpinBox(value=10., bounds=[-360, 360], minStep=1, step=10)),
        ("bx", pg.SpinBox(value=0., suffix='m', bounds=[50, 2000], siPrefix=True, step=10, minStep=1)),
        ("by", pg.SpinBox(value=0.0, suffix='m', bounds=[50, 2000], siPrefix=True, step=10, minStep=1)),
        ("dx", pg.SpinBox(value=0.0, suffix='m', bounds=[-500, 2500], siPrefix=True, step=10, minStep=1)),
        ("dy", pg.SpinBox(value=0.0, suffix='m', bounds=[-500, 2500], siPrefix=True, step=10, minStep=1)),
        ("n_elements", pg.SpinBox(value=16, int=True, bounds=[0, 50], siPrefix=True, step=1, minStep=1)),
        ("max_col", pg.SpinBox(value=4, int=True, bounds=[0, 20], siPrefix=True, step=1, minStep=1)),
    ]


    win = QtGui.QMainWindow()
    win.setWindowTitle('pyqtgraph example: SpinBox')
    cw = QtGui.QWidget()
    layout = QtGui.QGridLayout()
    cw.setLayout(layout)
    win.setCentralWidget(cw)
    win.show()

    labels = []

    # sample wind farm
    xmin = 0.
    xmax = 2000.
    ymin = 0.
    ymax = 2000.

    #wf_layout = pg.PlotItem()
    wf_layout = pg.PlotWidget()
    curve_layout = wf_layout.plot(pen=None, symbol='o')
    wf_layout.setXRange(xmin, xmax, padding=0.3)
    wf_layout.setYRange(ymin, ymax, padding=0.3)
    layout_boundary = wf_layout.plot([xmin, xmax, xmax, xmin, xmin],
                                     [ymin, ymin, ymax, ymax, ymin],
                                     pen=pg.mkPen('k', width=4))
    con_plot = pg.PlotWidget()
    curve_con = []
    for ic in range(WFLOP1_NUMBER_ELEMENTS*4):
        c = con_plot.plot()
        curve_con.append(c)

    for idx, (text, spin) in enumerate(spins):
        label = QtGui.QLabel(text)
        labels.append(label)
        layout.addWidget(label, 2*idx,  0, 1, 2)
        layout.addWidget(spin, 2*idx+1, 0, 1, 2)
        # MOD THIS
        spin.sigValueChanged.connect(valueChanged)
        #spin.sigValueChanging.connect(valueChanging)

    # plot
    #layout.addWidget(changingLabel, 0, 1)
    #layout.addWidget(changedLabel, 2, 1)
    layout.addWidget(wf_layout, 0, 2, 16, 12)
    layout.addWidget(con_plot, 0, 14, 16, 12)


    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

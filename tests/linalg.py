import sympy as sp
#import numpy as np
import Symbolic as sb


def line_coeffs_from_pts(p1, p2):
    assert len(p1) == 2
    assert len(p2) == 2
    
    A = p2[1] - p1[1]
    B = p1[0] - p2[0]
    C = p1[0]*p2[1] - p2[0]*p1[1]
    
    return A, B, C


def boundary_cons_test():
    polypts = sp.Matrix([[0, 2000, 2000,    0],
                         [0,    0, 2000, 2000]])

    vars = sb.WFLOP1_vars()

    print vars
    bcon = sb.get_boundary_constraints_linalg_expr(vars, polypts)
    print type(bcon)
    print len(bcon)
    sp.pprint(bcon)
    sp.pprint(len(bcon))

    print '=================='
    sp.pprint(sb.wf_coordinates(vars))


def polygon_test():

    #polypts = sp.Matrix([[0, 2000, 2000,    0],
    #                     [0,    0, 2000, 2000]])
    polypts = sp.Matrix([[0, 2000, 1500, 2000,    0],
                         [0,    0, 500, 2000, 2000]])

    A = sp.zeros(polypts.cols, 2)
    C = sp.zeros(polypts.cols,1)

    for i_line in range(polypts.cols):
        p1 = sb.Point(polypts[0, i_line], polypts[1, i_line])
        if i_line != polypts.cols-1:
            p2 = sb.Point(polypts[0, i_line+1], polypts[1, i_line+1])
        else:
            p2 = sb.Point(polypts[0, 0], polypts[1, 0])

        A_line, B_line, C_line = line_coeffs_from_pts(p1, p2)

        A[i_line, 0] = A_line
        A[i_line, 1] = B_line
        C[i_line] = C_line


        print 'Line passing through {}, {}'.format(p1, p2)
        print A_line, B_line, C_line

    sp.pprint(A)
    sp.pprint(C)
    
    # wf vertex points
    #vertices = sp.Matrix([[200, 400, 400, 200],
    #                      [200, 200, 400, 400]])
    vertices = sp.Matrix([[500, 1750, 1500, 500],
                          [0,    200, 1250, 1000]])

    for i_point in range(vertices.cols):
        print 
        print 'Point', i_point+1
        #sp.pprint(A*vertices[:, i_point])
        sp.pprint(A*vertices[:, i_point] - C)
        sp.pprint((A*vertices[:, i_point], C))

    import matplotlib.pyplot as plt
    plt.figure()

    # wf boundary
    plt.plot(polypts[0,:].tolist()[0] + [0], polypts[1,:].tolist()[0]+[0])
    plt.plot(vertices[0,:].tolist()[0], vertices[1,:].tolist()[0], 'ro')
    plt.show()


if __name__ == '__main__':
    print 'main'

    #polygon_test()
    boundary_cons_test()

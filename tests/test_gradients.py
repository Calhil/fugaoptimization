from multiprocessing.queues import SimpleQueue
from time import sleep
import numpy as np
import os

from config import OPT_GRADIENT
from config import FD_ORDER
from config import logger
from config import COLONEL_INSTANCE_IDENTIFIER
from config import RunInitialConfig
from workers import InputProcessor, SpawnObservers, OutputProcessor
from workers import ColonelSubprocess
from workers import Distributor
#from WFLOP import WindFarm

from config import FD_COEFFICIENTS as FD_COEFF
from config import CD_COEFF
from config import FD_PERTURBATION

# constants
number_turbines = 2


#CD_COEFF = [[-1/2., 1/2.],
#            [1./12, -2./3, 2./3, -1./12],
#            [-1./60, 3./20, -3./4, 3./4, -3./20, 1./60],
#            [1./280, -4./105, 1./5, -4./5, 4./5, -1./5, 4./105, -1./280]]
#
#FD_COEFF= [[-1., 1.],                                           # 1
#           [-3./2, 2., -1./2],                                  # 2
#           [-11./6, 3., -3./2, 1./3],                           # 3
#           [-25./12, 4., -3., 4./3, -1./4],                     # 4
#           [-137/.60, 5., -5., 10./3, -5./4, 1./5],             # 5
#           [-49./20, 6., -15./2, 20./3, -15./4, 6./5, -1./6],   # 6
#           [-363./140, 7., -21./2, 35./3, -35./4, 21./5, -7./6, 1./7],
#           [-761./280, 8., -14., 56./3, -35./2, 56./5, -14./3, 8./7, -1./8]]


OUTPUT_FILENAME = 'gradients.dat'


def fd_central(x, wf, fd_order=0, x_pert=20):
    """ Calculate AEP gradient using finite difference """
    assert 0 <= fd_order <= len(CD_COEFF) - 1
    
    grad_fd = np.zeros_like(x)
    # perturbation matrix, assumes x_pert=y_pert
    h = range(-len(CD_COEFF[fd_order])/2, len(CD_COEFF[fd_order])/2 + 1)
    h.remove(0)
    h = np.array(h)*x_pert

    logger.info('h = {}'.format(h))

    for ix in range(len(x)):
        grad_tmp = np.zeros_like(CD_COEFF[fd_order])
        for ic in range(len(CD_COEFF[fd_order])):
            #logger.info('ix = {}, ic = {}'.format(ix, ic))

            x_tmp = np.array(x)
            x_tmp[ix] += h[ic]

            AEP = wf.get_AEP(x_tmp)
            #print AEP
            #logger.info('AEP = <{}> at x = <{}>'.format(AEP, x_tmp))
            grad_tmp[ic] = AEP * CD_COEFF[fd_order][ic]
        #print grad_tmp
        grad_fd[ix] = np.sum(grad_tmp) / (1*x_pert)

    return grad_fd


def fd_forward(x, wf, fd_order=0, x_pert=20):
    """ Forward difference """
    assert 0 <= fd_order <= len(FD_COEFF) - 1

    grad_fd = np.zeros_like(x)
    h = range(len(FD_COEFF[fd_order]))
    h = np.array(h)*x_pert

    logger.info('h = {}'.format(h))

    for ix in range(len(x)):
        grad_tmp = np.zeros_like(FD_COEFF[fd_order])
        for ic in range(len(FD_COEFF[fd_order])):
            #logger.info('ix = {}, ic = {}'.format(ix, ic))

            x_tmp = np.array(x)
            x_tmp[ix] += h[ic]


            AEP = wf.get_AEP(x_tmp)
            logger.info('AEP = <{}>'.format(AEP))
            grad_tmp[ic] = AEP * FD_COEFF[fd_order][ic]

        #print grad_tmp
        grad_fd[ix] = np.sum(grad_tmp) / (1*x_pert)

    return grad_fd


class dummy_windfarm():
    """
    Dummy wind farm class
    Used to verify central difference
    AEP_i(x_i, y_i) = x_i**n + y_i**n
    AEP = sum(AEP_i)
    """
    def __init__(self, number_turbines, poly_order=2):
        self.nturb = number_turbines
        self.polyord = poly_order
    
    def get_AEP(self, x):
        aep = 0.
        for ii in range(self.nturb):
            aep += x[ii]**self.polyord \
                   + x[ii + self.nturb]**self.polyord
        return aep

    def get_gradAEP(self, x):
        grad = np.zeros_like(x)

        for ii in range(self.nturb):
            grad[ii] = self.polyord * x[ii] ** (self.polyord - 1)
            grad[ii+self.nturb] = self.polyord * x[ii + self.nturb] ** (self.polyord - 1)
        
        return grad


if __name__ == '__main__':
    logger.info('Testing gradients')

    RunInitialConfig()

    worker_ids = ['distributor', 'optimizer', 'input', 'output', 'main', 'plotter']
    q_container = {id: SimpleQueue() for id in worker_ids}

    logger.info('Initializing workers')
    inputWorker = InputProcessor(q_container)

    # start watching for changes in output file
    obs = SpawnObservers(queue_container=q_container)

    colonelWorker = []
    for ii in range(1):
        colonelWorker.append(ColonelSubprocess(COLONEL_INSTANCE_IDENTIFIER[ii]))

    outputWorker = OutputProcessor(queue_container=q_container)
    distWorker = Distributor(queue_container=q_container)

    logger.info('Starting workers')
    for ii in range(1):
        colonelWorker[ii].start()
    distWorker.start()
    inputWorker.start()
    outputWorker.start()
    sleep(1)

    # =======
    # TESTING
    # =======
    logger.info('Setting wind farm')
    #x = np.array([500, 1000, 1000, 500, 500, 500, 1000, 1000], dtype=np.float)
    # 8.40187717  3.94382927  7.83099224  7.98440033
    x = np.array([8.40187717, 3.94382927, 7.83099224, 7.98440033, 9.11647358, 1.97551369, 3.35222756, 7.68229595],
                 dtype=np.float)
    x = np.array([10., 20.], dtype=np.float)

    #windfarm = WindFarm(q_container, NumberWT=x.size/2)
    #windfarm.get_AEP(x)
    #windfarm.get_gradAEP(x)

    # check if fd is working properly
    windfarm = dummy_windfarm(x.size/2, poly_order=2)

    logger.info('Gradient at x = <{}>'.format(x))
    
    logger.info('Calculating gradient using \'{}\' method'.format(OPT_GRADIENT))
    grad_colonel = windfarm.get_gradAEP(x)

    logger.info('Calculating gradient using \'central difference\' method')
    grad_cd = fd_central(x, windfarm)
    #grad_cd = fd_forward(x, windfarm, fd_order=0)

    grad_colonel = grad_colonel.reshape((-1,))

    logger.info('Gradient colonel:\n{}'.format(grad_colonel))
    logger.info('Gradient central difference:\n{}'.format(grad_cd))

    logger.info('Error absolute:\n{}'.format(np.absolute(grad_colonel - grad_cd)))
    logger.info('Error relative wrt fd:\n{}'.format(np.absolute((grad_colonel - grad_cd)/grad_cd)))
    logger.info('Error relative wrt colonel:\n{}'.format(np.absolute((grad_colonel - grad_cd)/grad_colonel)))

    # ==============
    # SAVING TO FILE
    # ==============
    output_dict = {'GradientType': OPT_GRADIENT,
                   'GradientOrder': FD_ORDER,
                   'Perturbation': FD_PERTURBATION,
                   'GradientVal': grad_colonel.tolist(),
                   #'GradientVal': grad_cd.tolist(),
                   'x': x.tolist()}
    logger.info(output_dict)
    with open(OUTPUT_FILENAME, 'a') as f:
        #json.dump(output_dict, f)
        f.write('{}'.format(output_dict))
        f.write(os.linesep)


    # CLOSE EVERYTHING
    outputWorker.terminate()
    distWorker.terminate()
    inputWorker.terminate()
    for ii in range(1):
        colonelWorker[ii].terminate()


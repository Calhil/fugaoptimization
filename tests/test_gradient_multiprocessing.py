import multiprocessing
import time

import numpy as np

from config import (CD_COEFF)
from tests.test_gradients import (fd_central,
                                  dummy_windfarm)


CPU_ID_MATRIX = ['1', '2', '3', '4']

def fd_central_multi(x, wf, q, num_cpu, fd_order=0, x_pert=20):
    """ Calculate AEP gradient using finite difference """
    assert 0 <= fd_order <= len(CD_COEFF) - 1
    
    #grad_fd = np.zeros_like(x)
    # perturbation matrix, assumes x_pert=y_pert
    h = range(-len(CD_COEFF[fd_order])/2, len(CD_COEFF[fd_order])/2 + 1)
    h.remove(0)
    h = np.array(h)*x_pert

    # prepare the variables
    x_array = np.zeros((len(h) * len(x), len(x)), dtype=np.float)
    grad_fd = np.zeros((len(h) * len(x),), dtype=np.float)

    for ix in range(len(x)):
        for ic in range(len(CD_COEFF[fd_order])):
            #idx = (ix*(len(x)) + ic*(len(CD_COEFF[fd_order]))) / 2
            idx = ix*len(h) + ic
            #print idx
            x_tmp = np.array(x)
            x_tmp[ix] += h[ic]
            x_array[idx, :] = x_tmp
    #print x_array

    #send the data for processing
    last_idx_x = 0
    items_to_send = [True]*len(x_array)
    items_received = [False]*len(x_array)
    no_items_processing = 0
    cpuid_xrow = [None]*num_cpu

    # send the initial num_cpu items
    for icpu in range(num_cpu):
        #print 'Sending data(', last_idx_x, ') to', CPU_ID_MATRIX[icpu]
        q[CPU_ID_MATRIX[icpu]].put(x_array[last_idx_x,:])
        cpuid_xrow[icpu] = last_idx_x
        last_idx_x += 1
        no_items_processing += 1

    # receive the items
    while True:
        # receive the processed data
        data_tmp = q['fd'].get()
        cpu_id = data_tmp.keys()[0]
        cpu_idx = int(cpu_id) - 1

        #print 'Received items from', cpu_id

        grad_idx = cpuid_xrow[cpu_idx]
        #print 'grad_idx', grad_idx
        grad_fd[grad_idx] = data_tmp[cpu_id]
        items_received[grad_idx] = True
        no_items_processing -= 1
        #print 'Items received:', items_received

        # send data if there are spare cpus
        if no_items_processing < num_cpu \
                and last_idx_x < len(x_array):
            #print 'Sending data(', last_idx_x, ') to', CPU_ID_MATRIX[cpu_idx]
            q[ CPU_ID_MATRIX[cpu_idx] ].put(x_array[last_idx_x,:])
            items_to_send[last_idx_x] = False

            cpuid_xrow[cpu_idx] = last_idx_x
            last_idx_x += 1
            no_items_processing += 1

        if np.all(items_received):
            #print 'All items received'
            break

    grad_output = np.zeros_like(x)
    for ix in range(len(x)):
        grad_tmp = np.zeros_like(CD_COEFF[fd_order])
        for ic in range(len(CD_COEFF[fd_order])):

            #idx = (ix*(len(x)) + ic*(len(CD_COEFF[fd_order]))) / 2
            idx = ix*len(h) + ic

            AEP = grad_fd[idx]
            #print AEP
            #logger.info('AEP = <{}> at x = <{}>'.format(AEP, x_tmp))
            grad_tmp[ic] = AEP * CD_COEFF[fd_order][ic]
        #print grad_tmp
        grad_output[ix] = np.sum(grad_tmp) / (1*x_pert)

    return grad_output

class dummy_process(multiprocessing.Process):
    
    def __init__(self, q, wf, id):
        super(dummy_process, self).__init__()
        self.id = id
        self.q = q
        self.wf = wf
        print 'Starting', id

    def run(self):
        # start waiting for data
        while True:
            x = self.q[self.id].get()
            #print self.id, 'received', x

            output = self.wf.get_AEP(x)

            d = {self.id: output}
            self.q['fd'].put(d)


if __name__ == "__main__":
    
    n_elements = 4
    x0 = np.array([10*i for i in range(n_elements*2)], dtype=np.float)
    windfarm = dummy_windfarm(n_elements, poly_order=2)

    tic = time.time()
    grad_cd = fd_central(x0, windfarm, fd_order=0)
    print time.time() - tic

    tic = time.time()
    grad_an = windfarm.get_gradAEP(x0)
    print time.time() - tic

    print grad_cd
    print grad_an


    # start workers
    q_dict = {'fd': multiprocessing.queues.SimpleQueue(),
              '1': multiprocessing.queues.SimpleQueue(),
              '2': multiprocessing.queues.SimpleQueue(),
              '3': multiprocessing.queues.SimpleQueue(),
              '4': multiprocessing.queues.SimpleQueue()}
    w1 = dummy_process(q_dict, windfarm, '1')
    w2 = dummy_process(q_dict, windfarm, '2')
    w3 = dummy_process(q_dict, windfarm, '3')
    w4 = dummy_process(q_dict, windfarm, '4')

    w1.start()
    w2.start()
    w3.start()
    w4.start()

    tic = time.time()
    grad_multi = fd_central_multi(x0, windfarm, q_dict, 4, fd_order=3)
    print time.time() - tic
    print grad_multi

    w1.terminate()
    w2.terminate()
    w3.terminate()
    w4.terminate()



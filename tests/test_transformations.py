
import time
import multiprocessing

import numpy as np

import Symbolic as sb
import NumericalMethods as nm

from config import logger
from config import WFLOP1_MAX_ELEMENTS_PER_ROW
from config import WFLOP1_NUMBER_ELEMENTS


tol = 1e-8
n_elements = WFLOP1_NUMBER_ELEMENTS
max_col = WFLOP1_MAX_ELEMENTS_PER_ROW

n_elements = 16
max_col = 4


def compare_transformations(vars, n_elements=n_elements, max_col=max_col):
    assert(len(vars) == 6)

    alpha = vars[0]
    beta = vars[1]
    bx = vars[2]
    by = vars[3]
    dx = vars[4]
    dy = vars[5]
    #X = [alpha, beta, bx, by, dx, dy]

    #mapping = nm.adjust_vector_shape(nm.get_lattice_mapping(n_elements, max_col))

    n_shear_matrix = nm.shear_matrix(beta)
    n_rotational_matrix = nm.rotation_matrix(alpha)
    n_translation_matrix = nm.translation_matrix(np.array([[dx], [dy]]))

    s_shear_matrix = np.array(sb.shear_matrix(beta).tolist(), dtype=np.float)
    s_rotational_matrix = np.array(sb.rotation_matrix(alpha).tolist(), dtype=np.float)
    s_translation_matrix = np.array(sb.translation_matrix(dx, dy).tolist(), dtype=np.float)

    s_mapping = np.array(sb.wf_coordinates(vars, n_elements, max_col).tolist(), dtype=np.float)
    n_mapping = nm.get_wt_coordinates(alpha, beta, bx, by, dx, dy,
                                      n_elements=n_elements, max_col=max_col,
                                      vector_format='asdas')

    #print vars
    #print n_shear_matrix
    #print n_rotational_matrix
    #print n_translation_matrix
    #print s_shear_matrix
    #print s_rotational_matrix
    #print s_translation_matrix
    #print n_mapping
    #print s_mapping
    #print n_shear_matrix == s_shear_matrix
    #print n_rotational_matrix == s_rotational_matrix
    #print n_translation_matrix == s_translation_matrix
    #print s_mapping == n_mapping

    #if not np.all(n_shear_matrix == s_shear_matrix):
    if not np.all(np.abs(n_shear_matrix - s_shear_matrix) <= tol):
        print 'Shear matrix'
        print vars
        print n_shear_matrix
        print s_shear_matrix
        #print np.abs(n_shear_matrix - s_shear_matrix) <= 1e-6

    if not np.all(np.abs(n_rotational_matrix - s_rotational_matrix) <= tol):
        print 'Rotational matrix'
        print vars
        print n_rotational_matrix
        print s_rotational_matrix
        print np.abs(n_rotational_matrix - s_rotational_matrix)

    if not np.all(np.abs(n_translation_matrix - s_translation_matrix) <= tol):
        print 'Translation matrix'
        print vars
        print n_translation_matrix
        print s_translation_matrix

    if not np.all(np.abs(s_mapping - n_mapping) <= tol):
        print 'Mapping'
        print vars
        print n_mapping
        print s_mapping

def cartesian(arrays, out=None):
    """
    Generate a cartesian product of input arrays.

    Parameters
    ----------
    arrays : list of array-like
        1-D arrays to form the cartesian product of.
    out : ndarray
        Array to place the cartesian product in.

    Returns
    -------
    out : ndarray
        2-D array of shape (M, len(arrays)) containing cartesian products
        formed of input arrays.

    Examples
    --------
    >>> cartesian(([1, 2, 3], [4, 5], [6, 7]))
    array([[1, 4, 6],
           [1, 4, 7],
           [1, 5, 6],
           [1, 5, 7],
           [2, 4, 6],
           [2, 4, 7],
           [2, 5, 6],
           [2, 5, 7],
           [3, 4, 6],
           [3, 4, 7],
           [3, 5, 6],
           [3, 5, 7]])

    """

    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    n = np.prod([x.size for x in arrays])
    if out is None:
        out = np.zeros([n, len(arrays)], dtype=dtype)

    m = n / arrays[0].size
    out[:,0] = np.repeat(arrays[0], m)
    if arrays[1:]:
        cartesian(arrays[1:], out=out[0:m,1:])
        for j in xrange(1, arrays[0].size):
            out[j*m:(j+1)*m,1:] = out[0:m,1:]
    return out

def initialize_worker():
    print 'Starting', multiprocessing.current_process().name

if __name__ == '__main__':
    logger.info('Testing gradients')

    # sample x
    alpha = 0.
    beta = 0.
    bx = 0.
    by = 0.
    dx = 0.
    dy = 0.
    vars = [alpha, beta, bx, by, dx, dy]

    tic = time.time()
    compare_transformations(vars, n_elements, max_col)
    logger.info('It took {}s to run the function'.format(time.time() - tic))

    alpha_set = np.linspace(0., 360.*np.pi/180., 20)
    beta_set = np.linspace(-90*np.pi/180., 90.*np.pi/180., 20)
    bx_set = np.linspace(0., 500., 2)
    by_set = np.linspace(0., 500., 2)
    dx_set = np.linspace(0., 2000., 2)
    dy_set = np.linspace(0., 2000., 2)

    if False: 
        logger.info('Starting tests')
        for idx, ialpha in enumerate(alpha_set):
            print idx, alpha
            tic = time.time()
            for ibeta in beta_set:
                for idx in dx_set:
                    for idy in dy_set:
                        for ibx in bx_set:
                            for iby in by_set:
                                vars = [ialpha, ibeta, ibx, iby, idx, idy]
                                compare_transformations(vars, n_elements, max_col)
            logger.info('It took {}s to run the inner loop'.format(time.time() - tic))

    # multithreaded checking
    if True:
        a = [alpha_set, beta_set, bx_set, by_set, dx_set, dy_set]
        var_list = cartesian(a).tolist()
        #print var_list
        pool_size = multiprocessing.cpu_count()
        print pool_size
        pool = multiprocessing.Pool(processes=pool_size, initializer=initialize_worker)
        pool.map(compare_transformations, var_list)
        pool.close()
        pool.join()

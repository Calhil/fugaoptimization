import multiprocessing
import io
import os
import subprocess
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import numpy as np
import time
import traceback
from collections import namedtuple

from NumericalMethods import binomial_coefficient

from config import (logger,
                    ARCHIVE_STATUS,
                    ARCHIVE_FOLDER_PATH,
                    WORKSPACE_PATH,
                    NUMBER_COLONEL_INSTANCES,
                    COLONEL_INSTANCE_IDENTIFIER,
                    COLONEL_EXECUTABLE_NAME,
                    COLONEL_OUTPUT_FILE,
                    COLONEL_OUTPUT_DIR_PREFIX,
                    FD_ORDER,
                    FD_SCHEME,
                    FD_COEFFICIENTS,
                    CD_COEFF,
                    FD_PERTURBATION,
                    WFLOP1_CONS_AREA,
                    WFLOP1_CONS_BOUNDARY,
                    WFLOP1_CONS_PROXIMITY,
                    WFLOP1_NUMBER_ELEMENTS,
                    WFLOP1_MAX_ELEMENTS_PER_ROW)
from ColonelInterface import ColonelInput, ColonelOutput

# list of valid signal names
__VALID_COMMANDS = ['New output file',
                    'Terminate',
                    'addCommand',
                    'save',
                    'saveAndReset',
                    'resetCommandList',
                    'getCommandList',
                    'getCommandListString',
                    'ProcessedOutput',
                    'ChangeTarget',
                    'getTurbineList',
                    'AEP',
                    'AEP turbine',
                    'Gradient turbine',
                    'GradientFD', 'gradientFD',
                    'GradientColonel', 'gradientColonel',
                    'GradientLattice', 'gradientlattice',
                    'Processed gradient']

def checkSignal(data):
    """ Function used to test the validity of processed signal """

    status, keyStatus = 1, []
    # check if signal is a dict
    if isinstance(data, dict):
        # check the keys
        #for iKey in ['SignalName', 'SignalData', 'SignalOrigin', 'SignalTarget']:
        for iKey in ['SignalName', 'SignalData', 'SignalOrigin']:
            # check if signal dictionary has all the required keys
            if iKey not in data:
                keyStatus.append(1)
                logger.error('\'{}\' key is missing'.format(iKey))
            else:
                # check if the signal name is valid
                if iKey == 'SignalName' and \
                        data[iKey] not in __VALID_COMMANDS:
                    logger.error('Invalid signal name {}'
                                 .format(data[iKey]))
                    keyStatus.append(1)

        if len(keyStatus) == 0:
            status = 0
    else:
        logger.error('{} needs to be a dict'.format(data))

    if status != 0:
        raise ValueError('{} is not valid'.format(data))

    return status


def getInstanceFromPath(path):
    """Retrieves the Colonel instance string given a path"""
    r = None
    if isinstance(path[0], str):
        # 0th element is actually an empty string ''
        # so we need to get the 1st element
        r = path[0].replace(WORKSPACE_PATH, '').split('/')[1]
        #logger.error('getInstanceFromPath: path = {}, WORKSPACE_PATH = {}, r = {}'
        #        .format(path[0], WORKSPACE_PATH, r))
    else:
        logger.error('getInstanceFromPath: path = <{}>'.format(path))
        raise
    return r


class ColonelSubprocess(multiprocessing.Process):

    def __init__(self, instanceString):
        multiprocessing.Process.__init__(self)

        self.CurrentPath = None
        self.ExecutablePath = None

        # check if a directory is already set up for this instance
        p = os.path.join(WORKSPACE_PATH, instanceString)
        if os.path.exists(p):
            self.CurrentPath = p
            self.ExecutablePath = os.path.join(self.CurrentPath, COLONEL_EXECUTABLE_NAME)
            logger.info('Linked to {}'.format(self.ExecutablePath))
        else:
            logger.error('Received wrong instance string <{}>'
                         .format(instanceString))

    def getColonelOutput(self, processHandle):
        """ Stdout/Stderr streaming function """
        for line in io.open(processHandle.stdout.fileno()):
            yield line.rstrip('\n')

    def run(self):
        # change directory to the workspace dir
        logger.debug('Changing current directory to {}'.format(self.CurrentPath))
        os.chdir(self.CurrentPath)

        # spawn Colonel
        logger.info('Starting Colonel instance'.format())
        logger.info('Process PID = {}'.format(os.getpid()))

        try:
            #ColonelExec = subprocess.Popen([self.ExecutablePath],
            ColonelExec = subprocess.Popen(['./{}'.format(COLONEL_EXECUTABLE_NAME)],
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.STDOUT,
                                        bufsize=1)

            #stdout_handle = ColonelExec.communicate()[0]
            logger.debug('Subprocess spawned'.format())
            
            # start listening for Colonel output
            while True:
                try:
                    for line in self.getColonelOutput(ColonelExec):
                        logger.debug('{}'.format(line))
                except OSError:
                    break
                except IOError:
                    break

            # wait for Colonel to exit
            ColonelExec.wait()
            logger.info('Work is done'.format())
        except KeyboardInterrupt:
            # TODO this is probably not even needed
            logger.error('Killing Colonel'.format())
            ColonelExec.kill()

        except Exception as e:
            raise e


class Distributor(multiprocessing.Process):
    """Class responsible for distributing signal from optimizer
       to InputProcessor"""
    Q = None    # container with all the worker input queues
    id = 'distributor'  # id used to represent this class in queue container
    signal_queue = []   # queue of signals

    # used to determine if we need to save the input file
    # of a particular instance when a 'save' signal is received
    # { 'instanceStr': True/False, ... }
    InstanceSave = {}

    def __init__(self, queue_container):
        multiprocessing.Process.__init__(self)
        self.Q = queue_container

        # set the initial save status
        for ii in COLONEL_INSTANCE_IDENTIFIER:
            self.InstanceSave[ii] = False

        logger.info('Initiating distributor')

    def __gradient(self, signal):
        """Function responsible for distributing signal to the respective
        finite difference scheme functions"""

        logger.debug('Calculating gradient using finite differences')

        if str.lower(FD_SCHEME) == 'forward':
            M_grad = self.__forward_difference(signal)
        elif str.lower(FD_SCHEME) == 'central':

            if NUMBER_COLONEL_INSTANCES == 1:
                M_grad = self.__central_difference(signal)
            else:
                M_grad = self.__central_difference_multi(signal)
        else:
            logger.error('Unimplemented finite difference method {}'.format(FD_SCHEME))
            raise TypeError

        # send the gradient back to the optimizer
        d = {'SignalName': 'Processed gradient',
             'SignalData': M_grad,
             'SignalOrigin': self.id}
        logger.debug('Gradient calculated. Sending it to optimizer')
        self.Q['optimizer'].put(d)

    def __gradient_lattice(self, signal):

        # list of the perturbed x vectors
        x_array = signal['SignalData']

        h = range(-len(CD_COEFF[FD_ORDER-1])/2, len(CD_COEFF[FD_ORDER-1])/2 + 1)
        h.remove(0)

        # assumes 6 lattice variables !
        aep_array = np.zeros((len(h) * 6,), dtype=np.float)

        #send the data for processing
        last_idx_x = 0
        items_to_send = [True]*len(x_array)
        items_received = [False]*len(x_array)
        no_items_processing = 0
        cpuid_xrow = [None]*NUMBER_COLONEL_INSTANCES

        # send the initial num_cpu items
        for icpu in range(NUMBER_COLONEL_INSTANCES):

            d = {'SignalName': 'AEP',
                    'SignalData': x_array[last_idx_x,:],
                    'SignalOrigin': self.id,
                    'SignalTarget': COLONEL_INSTANCE_IDENTIFIER[icpu],
                    'SignalReturn': self.id}
            self.Q['input'].put(d)

            cpuid_xrow[icpu] = last_idx_x
            last_idx_x += 1
            no_items_processing += 1

        # get the evaluated AEP
        logger.debug('Waiting for processed signal')

        # receive the items
        while True:
            # receive the aep data
            processedSignal = None
            while True:
                processedSignal = self.Q[self.id].get()
                #if processedSignal['SignalName'] != 'ProcessedOutput':
                if processedSignal['SignalName'] == 'AEP' and \
                        processedSignal['SignalOrigin'] == 'OutputProcessor':
                    break
                else:
                    logger.debug('Invalid signal from <{}>'
                                .format(processedSignal['SignalOrigin']))
                    self.Q[self.id].put(processedSignal)
            if checkSignal(processedSignal) == 0:
                logger.debug('ProcessedSignal = <{}>'.format(processedSignal))
                cpu_idx = int(processedSignal['SignalTarget'])
                aep_idx = cpuid_xrow[cpu_idx]
                aep_array[aep_idx] = processedSignal['SignalData']['Data'][0]
                items_received[aep_idx] = True
                no_items_processing -= 1

            # send data if there are spare cpus
            if no_items_processing < NUMBER_COLONEL_INSTANCES \
                    and last_idx_x < len(x_array):
                d = {'SignalName': 'AEP',
                        'SignalData': x_array[last_idx_x,:],
                        'SignalOrigin': self.id,
                        'SignalTarget': COLONEL_INSTANCE_IDENTIFIER[cpu_idx],
                        'SignalReturn': self.id}
                self.Q['input'].put(d)

                items_to_send[last_idx_x] = False
                cpuid_xrow[cpu_idx] = last_idx_x
                last_idx_x += 1
                no_items_processing += 1

            if np.all(items_received):
                #print 'All items received'
                break

        # send the gradient back to the optimizer
        d = {'SignalName': 'Processed gradient',
             'SignalData': aep_array,
             'SignalOrigin': self.id}
        logger.debug('Gradient calculated. Sending it to optimizer')
        self.Q['optimizer'].put(d)

        return None

    def __central_difference(self, signal):
        """Calculates gradient using central difference"""
        fd_order = FD_ORDER - 1
        assert 0 <= fd_order <= len(CD_COEFF) - 1

        x_pert = FD_PERTURBATION
        #x_pert = 1e-5
        x = signal['SignalData']

        grad_fd = np.zeros_like(x)
        # perturbation matrix, assumes x_pert=y_pert
        h = range(-len(CD_COEFF[fd_order])/2, len(CD_COEFF[fd_order])/2 + 1)
        h.remove(0)
        h = np.array(h)*x_pert

        #logger.info('h = {}'.format(h))

        for ix in range(len(x)):
            grad_tmp = np.zeros_like(CD_COEFF[fd_order])
            for ic in range(len(CD_COEFF[fd_order])):

                x_tmp = np.array(x)
                x_tmp[ix] += h[ic]

                # get AEP at x_tmp

                d = {'SignalName': 'AEP',
                     'SignalData': x_tmp,
                     'SignalOrigin': self.id,
                     'SignalTarget': COLONEL_INSTANCE_IDENTIFIER[0],
                     'SignalReturn': self.id}
                self.Q['input'].put(d)

                # get the evaluated AEP
                logger.debug('Waiting for processed signal')
                
                AEP = None
                processedSignal = None
                while True:
                    processedSignal = self.Q[self.id].get()
                    #if processedSignal['SignalName'] != 'ProcessedOutput':
                    if processedSignal['SignalName'] == 'AEP' and \
                            processedSignal['SignalOrigin'] == 'OutputProcessor':
                        break
                    else:
                        logger.debug('Invalid signal from <{}>'
                                    .format(processedSignal['SignalOrigin']))
                        self.Q[self.id].put(processedSignal)
                if checkSignal(processedSignal) == 0:
                    logger.debug('ProcessedSignal = <{}>'.format(processedSignal))
                    AEP = processedSignal['SignalData']['Data'][0]

                #logger.info('AEP = <{}>'.format(AEP))
                grad_tmp[ic] = AEP * CD_COEFF[fd_order][ic]

            grad_fd[ix] = np.sum(grad_tmp) / (1*x_pert)
        return grad_fd

    def __central_difference_multi(self, signal):
        """Calculates gradient using central difference"""
        fd_order = FD_ORDER - 1
        assert 0 <= fd_order <= len(CD_COEFF) - 1

        if 'step_size' in signal:
            logger.debug('Using modified step size = {}'.format(signal['step_size']))
            x_pert = signal['step_size']
        else:
            x_pert = FD_PERTURBATION

        x = signal['SignalData']
        # perturbation matrix, assumes x_pert=y_pert
        h = range(-len(CD_COEFF[fd_order])/2, len(CD_COEFF[fd_order])/2 + 1)
        h.remove(0)
        h = np.array(h)*x_pert

        #logger.info('h = {}'.format(h))

        # prepare the perturbed x vectors
        x_array = np.zeros((len(h) * len(x), len(x)), dtype=np.float)
        aep_array = np.zeros((len(h) * len(x),), dtype=np.float)

        for ix in range(len(x)):
            for ic in range(len(CD_COEFF[fd_order])):
                idx = ix*len(h) + ic

                x_tmp = np.array(x)
                x_tmp[ix] += h[ic]
                x_array[idx, :] = x_tmp

        #send the data for processing
        last_idx_x = 0
        items_to_send = [True]*len(x_array)
        items_received = [False]*len(x_array)
        no_items_processing = 0
        cpuid_xrow = [None]*NUMBER_COLONEL_INSTANCES

        # send the initial num_cpu items
        for icpu in range(NUMBER_COLONEL_INSTANCES):

            d = {'SignalName': 'AEP',
                    'SignalData': x_array[last_idx_x,:],
                    'SignalOrigin': self.id,
                    'SignalTarget': COLONEL_INSTANCE_IDENTIFIER[icpu],
                    'SignalReturn': self.id}
            self.Q['input'].put(d)
            #d = {'SignalName': 'AEP',
            #        'SignalData': x_tmp,
            #        'SignalOrigin': self.id,
            #        'SignalTarget': COLONEL_INSTANCE_IDENTIFIER[0],
            #        'SignalReturn': self.id}
            #self.Q['input'].put(d)

            cpuid_xrow[icpu] = last_idx_x
            last_idx_x += 1
            no_items_processing += 1

        # get the evaluated AEP
        logger.debug('Waiting for processed signal')


        #q, num_cpu, CPU_ID_MATRIX, grad_tmp = 0, 0, 0, 0

        # receive the items
        while True:
            # receive the aep data
            processedSignal = None
            while True:
                processedSignal = self.Q[self.id].get()
                #if processedSignal['SignalName'] != 'ProcessedOutput':
                if processedSignal['SignalName'] == 'AEP' and \
                        processedSignal['SignalOrigin'] == 'OutputProcessor':
                    break
                else:
                    logger.debug('Invalid signal from <{}>'
                                .format(processedSignal['SignalOrigin']))
                    self.Q[self.id].put(processedSignal)
            if checkSignal(processedSignal) == 0:
                logger.debug('ProcessedSignal = <{}>'.format(processedSignal))
                cpu_idx = int(processedSignal['SignalTarget'])
                aep_idx = cpuid_xrow[cpu_idx]
                aep_array[aep_idx] = processedSignal['SignalData']['Data'][0]
                items_received[aep_idx] = True
                no_items_processing -= 1


            # send data if there are spare cpus
            if no_items_processing < NUMBER_COLONEL_INSTANCES \
                    and last_idx_x < len(x_array):
                d = {'SignalName': 'AEP',
                        'SignalData': x_array[last_idx_x,:],
                        'SignalOrigin': self.id,
                        'SignalTarget': COLONEL_INSTANCE_IDENTIFIER[cpu_idx],
                        'SignalReturn': self.id}
                self.Q['input'].put(d)

                items_to_send[last_idx_x] = False
                cpuid_xrow[cpu_idx] = last_idx_x
                last_idx_x += 1
                no_items_processing += 1

            if np.all(items_received):
                #print 'All items received'
                break
        #  ====================
        
        # do the central difference calculations
        grad_output = np.zeros_like(x)
        for ix in range(len(x)):
            grad_tmp = np.zeros_like(CD_COEFF[fd_order])
            for ic in range(len(CD_COEFF[fd_order])):
                idx = ix*len(h) + ic
                AEP = aep_array[idx]
                grad_tmp[ic] = AEP * CD_COEFF[fd_order][ic]
            grad_output[ix] = np.sum(grad_tmp) / (1*x_pert)

        return grad_output

    def __forward_difference(self, signal):
        # set a new worker target for OutputProcessor
        #targetSignal = {'SignalName': 'ChangeTarget',
        #                'SignalData': None,
        #                'SignalOrigin': self.id,
        #                'SignalReturn': self.id}
        #self.Q['output'].put(targetSignal)
        ##TODO: wait for signal confirmation!!
        #logger.debug('Waiting for worker change confirmation')
        #while True:
        #    confsig = self.Q[self.id].get()
        #    if checkSignal(confsig) == 0:
        #        if confsig['SignalData'] == self.id:
        #            logger.debug('Successful worker target change')
        #            break
        #        else:
        #            logger.error('Unsuccessful worker change')
        #            self.Q[self.id].put(confsig)

        data = signal['SignalData']

        # TODO: CHANGE THIS!!
        #       To include scaling factors
        # Perturbation distance
        h = 10

        # PREPARE THE DATA HERE
        N_eval = None   # number of function evaluations
        M_coord = None  # matrix with column vector coordinates
        dimLen = len(data)  # assume Nx1 vector

        if FD_SCHEME in ['Forward']:
            # number of function evaluations
            N_eval = 1 + (len(FD_COEFFICIENTS[FD_ORDER-1])-1) * dimLen
            #logger.debug('N_eval = {}'.format(N_eval))
            
            # create a matrix with perturbed coordinates
            # this creates a matrix populated by coordinate vectors
            # shape is N x N_eval

            #logger.debug('data = {}, shape={}'.format(data, np.shape(data)))

            M_coord = np.repeat(data.reshape((data.shape[0], 1)), N_eval, axis=1)
            #logger.debug('M_coord.shape = {}'.format(M_coord.shape))
            # loop over columns
            #logger.debug('M_coord.shape={}'.format(np.shape(M_coord)))
            i_row = 0
            for i_col in range(1, N_eval):
                #logger.debug('row={}, col={}, dimLen={}'.format(i_row, i_col, dimLen))
                M_coord[i_row, i_col] += h

                # set the index for next row
                i_row += 1
                if i_row == dimLen:
                    i_row = 0            

            #logger.debug(M_coord)
        else:
            # there is no need to evaluate the f(x0) in some cases
            # of central difference
            logger.error('{} difference not implemented'.format(FD_SCHEME))

        # column index to be considered next
        lastColumn = 0

        # variable keeping track of which Colonel instance
        # received particular vector determined by column number
        #columnInstance = [None]*NUMBER_COLONEL_INSTANCES
        columnInstance = {kk: False for kk in COLONEL_INSTANCE_IDENTIFIER}
        
        # determines whether particular column was already processed
        columnStatus = [False]*N_eval

        logger.debug('Sending initial vectors')
        # send the initial N vectors
        for ii in COLONEL_INSTANCE_IDENTIFIER:
            #logger.debug('Sending column {} to Colonel {} <{}>'
            #             .format(lastColumn, ii, M_coord[:, lastColumn])) 
            # TODO: check indices !!!!!!!!!!
            d = {'SignalName': 'AEP',
                 'SignalData': M_coord[:, lastColumn],
                 'SignalOrigin': self.id,
                 'SignalTarget': ii,
                 'SignalReturn': self.id}

            self.Q['input'].put(d)

            columnInstance[ii] = lastColumn
            lastColumn += 1
            
            # dont assume the number of vectors to be processed
            # is larger than the number of Colonel instances
            if lastColumn > N_eval:
                break

        # matrix containing evaluated function values
        M_feval = [None]*N_eval

        # wait for those initial vectors to finish processing
        # and start sending the rest
        
        logger.debug('Sending the rest of the vectors')
        # loop untill all the vectors are processed
        while all(columnStatus) == False:
            logger.debug('Waiting for processed signal')
            
            # determine which Colonel instance finished processing 
            # and send it more data

            # TODO: check if were getting gradient here
            # wait for correct signal from OutputProcessor
            processedSignal = None
            while True:
                processedSignal = self.Q[self.id].get()
                #if processedSignal['SignalName'] != 'ProcessedOutput':
                if processedSignal['SignalName'] == 'AEP' and \
                        processedSignal['SignalOrigin'] == 'OutputProcessor':
                    break
                else:
                    logger.debug('Invalid signal from <{}>'
                                 .format(processedSignal['SignalOrigin']))
                    self.Q[self.id].put(processedSignal)
            logger.debug('Processed signal = <{}>'.format(processedSignal))

            if checkSignal(processedSignal) == 0:
                # TODO add checks here
                #if processedSignal['SignalOrigin'] == 'OutputProcessor' \
                #        and processedSignal['SignalName'] == 'Processed gradient' \
                #        and isinstance(processedSignal['SignalData'], list):

                logger.debug('ProcessedSignal = <{}>'.format(processedSignal))
                # this determines which vector this Colonel
                # instance has processed
                colIdx = columnInstance[processedSignal['SignalTarget']]
                M_feval[colIdx] = processedSignal['SignalData']['Data'][0]
                #logger.debug('Columns processed {}/{} <{}>'.format(
                #    columnStatus.count(True), N_eval, columnStatus))

                # set the particular column as processed
                columnStatus[colIdx] = True

                # send more data for processing if there is any
                if lastColumn < N_eval:

                    d = {'SignalName': 'AEP',
                        'SignalData': M_coord[:, lastColumn],
                        'SignalOrigin': self.id,
                        'SignalTarget': processedSignal['SignalTarget'],
                        'SignalReturn': self.id}
                    
                    #logger.debug('Sending column {} to Colonel {} <{}>'
                    #             .format(lastColumn, processedSignal['SignalTarget'],
                    #                     M_coord[:, lastColumn]))
                    self.Q['input'].put(d)

                    columnInstance[processedSignal['SignalTarget']] = lastColumn
                    lastColumn += 1
            else:
                logger.error('Invalid signal <{}>'.format(processedSignal))

        # apply the FD coefficients
        logger.debug('Processing evaluated functions')
        #logger.debug(M_feval)

        M_grad = np.zeros((dimLen, 1))  # assume we only consider AEP here
        if FD_SCHEME in ['Forward', 'Backward']:
            for iVar in range(dimLen):
                for iCol, iCoeff in enumerate(FD_COEFFICIENTS[FD_ORDER-1]):

                    # determine index for this coefficients
                    idx = 0
                    if iCol > 0:
                        idx = 1 + ((iCol-1)*dimLen) + iVar

                    #logger.debug('iVar={}, iCol={}, idx={}'.format(iVar, iCol, idx))
                    M_grad[iVar, 0] += iCoeff * M_feval[idx]

            M_grad /= h

        else:
            logger.error('{} difference not implemented'.format(FD_SCHEME))

        #logger.inf('Gradient calculated')
        #logger.info(M_grad)

        # reset worker target for OutputProcessor
        #targetSignal = {'SignalName': 'ChangeTarget',
        #                'SignalData': None,
        #                'SignalOrigin': self.id,
        #                'SignalReturn': 'optimizer'}
        #self.Q['output'].put(targetSignal)

        ##TODO: wait for signal confirmation!!
        #logger.debug('Waiting for worker change confirmation')
        #while True:
        #    confsig = self.Q[self.id].get()
        #    if checkSignal(confsig) == 0:
        #        if confsig['SignalData'] == 'optimizer':
        #            logger.debug('Successful worker target change')
        #            break
        #        else:
        #            logger.error('Unsuccessful worker change')
        #            self.Q[self.id].put(confsig)
        return M_grad

    def add_command(self, d_signal):
        # check if the turbine number is going to change
        if d_signal['SignalData']['commandName'] in \
                ['insert turbine', 'remove turbine']:
            # send the command to all of the Colonel instances
            for ii in COLONEL_INSTANCE_IDENTIFIER:
                d_signal['SignalTarget'] = ii
                #d_signal['SignalTarget'] = '{}'.format(ii)
                self.Q['input'].put(d_signal)
                self.InstanceSave[ii] = True
        else:
            # turbine number stays the same, so we only need
            # to consider the default Colonel instance
            instance = COLONEL_INSTANCE_IDENTIFIER[0]
            d_signal['SignalTarget'] = instance
            self.Q['input'].put(d_signal)
            self.InstanceSave[instance] = True

    def get_AEP(self, d_signal):
        assert checkSignal(d_signal) == 0

        if 'SignalTarget' not in d_signal:
            d_signal['SignalTarget'] = COLONEL_INSTANCE_IDENTIFIER[0]
        d_signal['SignalOrigin'] = self.id
        # forward signal to input processor
        self.Q['input'].put(d_signal)

        # get aep
        # wait for OutputProcessor signal
        while True:
            aep_signal = self.Q[self.id].get()
            if aep_signal['SignalName'] == 'AEP' and \
                    aep_signal['SignalOrigin'] == 'OutputProcessor':
                # send the AEP back
                break
            else:
                logger.debug('Invalid AEP signal <{}>'
                             .format(aep_signal))
                self.Q['distributor'].put(aep_signal)

        aep_signal['SignalOrigin'] = self.id
        logger.debug('Sending AEP signal to optimizer')
        self.Q['optimizer'].put(aep_signal)

    def get_AEP_turbine(self, d_signal):
        assert checkSignal(d_signal) == 0

        if 'SignalTarget' not in d_signal:
            d_signal['SignalTarget'] = COLONEL_INSTANCE_IDENTIFIER[0]
        d_signal['SignalOrigin'] = self.id
        # forward signal to input processor
        self.Q['input'].put(d_signal)

        # get aep
        # wait for OutputProcessor signal
        while True:
            aep_signal = self.Q[self.id].get()
            if aep_signal['SignalName'] == 'AEP turbine' and \
                    aep_signal['SignalOrigin'] == 'OutputProcessor':
                # send the AEP back
                break
            else:
                logger.debug('Invalid AEP turbine signal <{}>'
                             .format(aep_signal))
                self.Q['distributor'].put(aep_signal)

        aep_signal['SignalOrigin'] = self.id
        logger.debug('Sending AEP turbine signal to optimizer')
        self.Q['optimizer'].put(aep_signal)

    def get_gradient_turbine(self, d_signal):
        assert checkSignal(d_signal) == 0

        if 'SignalTarget' not in d_signal:
            d_signal['SignalTarget'] = COLONEL_INSTANCE_IDENTIFIER[0]
        d_signal['SignalOrigin'] = self.id
        # forward signal to input processor
        self.Q['input'].put(d_signal)

        # get aep
        # wait for OutputProcessor signal
        while True:
            aep_signal = self.Q[self.id].get()
            if aep_signal['SignalName'] == 'Gradient turbine' and \
                    aep_signal['SignalOrigin'] == 'OutputProcessor':
                # send the AEP back
                break
            else:
                logger.debug('Invalid Gradient turbine signal <{}>'
                             .format(aep_signal))
                self.Q['distributor'].put(aep_signal)

        aep_signal['SignalOrigin'] = self.id
        logger.debug('Sending Gradient turbine signal to optimizer')
        self.Q['optimizer'].put(aep_signal)

    def getTurbineList(self, d_signal):
        # process getTurbineList command
        if d_signal['SignalOrigin'] in ['output', 'OutputProcessor']:
            # send this back to optimizer
            logger.debug('Forwarding getTurbineList to optimizer')
            d_signal['SignalOrigin'] = self.id
            self.Q['optimizer'].put(d_signal)

        elif d_signal['SignalOrigin'] == 'optimizer':
            logger.debug('Sending <{}> to optimizer'
                         .format(d_signal['SignalName']))
            self.Q['input'].put(d_signal)
        else:
            logger.error('Unimplemented signalorigin \'{}\' for getTurbineList'
                         .format(d_signal['SignalOrigin']))

    def gradient_colonel(self, d_signal):
        d_signal['SignalOrigin'] = self.id
        d_signal['SignalTarget'] = COLONEL_INSTANCE_IDENTIFIER[0]

        self.Q['input'].put(d_signal)

        # receive and check the signal from OutputProcessor
        grad_signal = self.Q[self.id].get()
        
        grad_signal['SignalOrigin'] = self.id
        self.Q['optimizer'].put(grad_signal)

    def save(self, d_signal):
        # save only the instances where the layout changed
        for ii in COLONEL_INSTANCE_IDENTIFIER:
            if self.InstanceSave[ii]:
                d_signal['SignalTarget'] = ii
                self.Q['input'].put(d_signal)
                # reset the save status
                self.InstanceSave[ii] = False

    def save_reset(self, d_signal):
        logger.debug('Saving and resetting commandList')
        s = {'SignalName': 'save',
                'SignalData': None,
                'SignalOrigin': 'Distributor'}

        r = {'SignalName': 'resetCommandList',
                'SignalData': None,
                'SignalOrigin': 'Distributor'}

        for ii in COLONEL_INSTANCE_IDENTIFIER:
            # save
            s['SignalTarget'] = ii
            self.Q['input'].put(s)
            self.InstanceSave[ii] = False

            # reset the command list
            r['SignalTarget'] = ii
            self.Q['input'].put(r)

    def run(self):
        logger.info('Starting')
        logger.info('Process PID = {}'.format(os.getpid()))

        while True:
            try:
                # get data from queue
                qDict = None
                qDict = self.Q[self.id].get()

                logger.debug('Received <{}>'.format(qDict))

                # check if signal target is specified
                #
                #if 'SignalTarget' not in qDict:
                #    qDict['SignalTarget'] = COLONEL_INSTANCE_IDENTIFIER[0]
                #    logger.debug('{}: Unspecified SignalTarget, using default'
                #                 .format(self.name))

                if checkSignal(qDict) == 0:
                    if qDict['SignalName'] == 'AEP':
                        self.get_AEP(qDict)

                    elif qDict['SignalName'] == 'AEP turbine':
                        self.get_AEP_turbine(qDict)

                    elif qDict['SignalName'] == 'Gradient turbine':
                        self.get_gradient_turbine(qDict)

                    elif qDict['SignalName'] in ['gradientFD', 'GradientFD']:
                        tic = time.time()
                        self.__gradient(qDict)
                        logger.debug('Calculating gradient took {}s'.format(time.time()-tic))

                    elif qDict['SignalName'] in ['gradientColonel', 'GradientColonel']:
                        tic = time.time()
                        self.gradient_colonel(qDict)
                        logger.debug('Calculating Colonel gradient took {}s'.format(time.time()-tic))

                    elif qDict['SignalName'] in ['GradientLattice', 'gradientlattice']:
                        tic = time.time()
                        self.__gradient_lattice(qDict)
                        logger.debug('Calculating FD lattice gradient took {}s'.format(time.time()-tic))

                    elif qDict['SignalName'] == 'addCommand':
                        self.add_command(qDict)

                    elif qDict['SignalName'] == 'getTurbineList':
                        self.getTurbineList(qDict)

                    elif qDict['SignalName'] == 'save':
                        self.save(qDict)

                    elif qDict['SignalName'] == 'saveAndReset':
                        self.save_reset(qDict)

                    else:
                        logger.warning('Unimplemented signal {}'
                                       .format(qDict['SignalName']))
                
                else:
                    logger.error('Wrong signal <{}>'
                                 .format(qDict['SignalName']))
            except KeyboardInterrupt:
                logger.warning('Received keyboard interrupt')
                break
            except Exception as e:
                error_str = traceback.format_exc()
                logger.error(error_str)
                self.Q['main'].put([e, error_str])
                #raise e


class OutputProcessor(multiprocessing.Process):
    Q = None   # container with all the worker input queues
    id = 'output'  # id used to represent this class in queue container

    #worker_target = 'distributor'  # queue key specifying where to send signals

    outputFile = {}  # instance of ColonelOutput class
    outputContents = {}
    #newContentsStatus = {}

    def __init__(self, queue_container):
        multiprocessing.Process.__init__(self)

        logger.info('Initiating')

        # data queues
        self.Q = queue_container

        # create an instance of ColonelOutput class
        for ii in range(NUMBER_COLONEL_INSTANCES):
            instance = COLONEL_INSTANCE_IDENTIFIER[ii]
            self.outputFile[instance] = ColonelOutput(instance)

            # set the outputContents dict structure
            self.outputContents[instance] = None

    def processOutputFile(self, qSignal):
        # determine which Colonel instance produced output
        instance = getInstanceFromPath(qSignal['SignalData'])
        logger.debug('instance={}, signalData={}'
                .format(instance, qSignal['SignalData']))

        # ask InputProcessor for current commandlist
        askSignal  = {'SignalName': 'getCommandList',
                      'SignalData': None,
                      'SignalOrigin': 'OutputProcessor',
                      'SignalTarget': instance}

        # TODO: remove
        #logger.debug('Sending getCommandList signal to InputProcessor')
        self.Q['input'].put(askSignal)

        # TODO:
        # DETERMINE IF THE COMMANDS PRODUCE ANY OUTPUT AT ALL
        # IF NOT THE COMMAND LIST CAN BE RESET

        # wait for a reply from InputProcessor
        commandSignal = None
        while commandSignal is None:
            commandSignal = self.Q[self.id].get()
            checkSignal(commandSignal)
            # check if the signal is from InputProcessor
            if commandSignal['SignalOrigin'] != 'InputProcessor':
                # TODO remove this
                #logger.warning('{}: Wrong signal origin <{}>'
                #            .format(self.name, commandSignal['SignalOrigin']))
                #logger.warning('Signal = <{}>'.format(commandSignal))

                # put the signal back at the end of the queue
                self.Q[self.id].put(commandSignal)
                commandSignal = None

        CommandList = commandSignal['SignalData']

        # check if there are new commands to process
        if len(CommandList) > 0:
            logger.debug('Received new output file signal')

            # output file processing
            # TODO: do a full output data <-> input command
            #       association right now only farm aep
            #       can be processed

            # read the new output file
            self.outputFile[instance].read()
            newContentsString = self.outputFile[instance].getOutputString()
            newContentsStatus = False

            # check if the output file contents have changed
            if self.outputContents[instance] is not None:
                # found new contents
                if self.outputContents[instance] != newContentsString:
                    newContentsStatus = True
                    self.outputContents[instance] = newContentsString
            else:
                logger.debug('Setting new output contents')
                self.outputContents[instance] = newContentsString
                newContentsStatus = True

            # proceed with output processing if there is any data
            # in the string
            if len(self.outputFile[instance].getOutputString()) > 0 and newContentsStatus:

                # Determine which commands produce output and associate it
                # lMin, lMax = 0, 0
                OutputCommands = []  # list of commands that produced output
                for iCommand in CommandList['Data']:
                    # find the command lines that produce output
                    if len(iCommand['OutputLines']) > 0:
                        logger.debug('\'{}\' produced output'
                                        .format(iCommand['CommandName']))
                        OutputCommands.append(iCommand['CommandName'])
                # TODO Why do I have an assertion here??
                assert len(np.unique(OutputCommands)) == 1

                #QueueData = None
                #initial = False
                # check if its the initial command list
                #if CommandList['CommandGroupName'] == 'Initial':
                #    logger.debug('Got initial command group')
                #    QueueData = {'WFName': 'Initial',
                #                 'LayoutData': self.outputFile[instance].getTurbineList()}
                #    initial = True
                #    # TODO fix this
                #    # workaround for resetting the command list in the case
                #    # when commands dont produce any output but still influence
                #    # the state of the problem.
                #    # reset all the commandLists
                #    #for ii in COLONEL_INSTANCE_IDENTIFIER:
                #    #    logger.debug('Resetting commandList for Colonel <{}>'
                #    #                .format(ii))
                #    #    resetSignal = {'SignalName': 'resetCommandList',
                #    #                    'SignalData': None,
                #    #                    'SignalOrigin': 'OutputProcessor',
                #    #                    'SignalTarget': ii}
                #    #    self.queue_outInput.put(resetSignal)
                #else:
                #    logger.debug('Got AEP command group')
                #    QueueData = self.outputFile[instance].getAEP()

                # send processed output data on the queue to the optimizer
                #if QueueData is not None:
                # -----------------------

                # since the data is procesed we can reset the command list
                resetSignal = {'SignalName': 'resetCommandList',
                                'SignalData': None,
                                'SignalOrigin': 'OutputProcessor',
                                'SignalTarget': instance}
                self.Q['input'].put(resetSignal)

                # reset the outputcontents so they are properly
                # set for the next iteration 
                self.outputContents[instance] = None

                # just to be safe delete the old output file
                self.outputFile[instance].delete_outputfile()

                logger.debug('Sending processed \'{}\' to distributor'
                                .format(OutputCommands))

                # Prepare and send the signal
                processedSignal = {'SignalOrigin': 'OutputProcessor',
                                   'SignalTarget': instance}

                # set a proper signal name depending on the output command
                if OutputCommands[0] in ['get Farm AEP', 'get total AEP']:
                    processedSignal['SignalName'] = 'AEP'
                    processedSignal['SignalData'] = self.outputFile[instance].getAEP()

                elif OutputCommands[0] in ['get aep turbine']:
                    processedSignal['SignalName'] = 'AEP turbine'
                    processedSignal['SignalData'] = self.outputFile[instance].getAEP_turbine()

                elif OutputCommands[0] in ['get aep gradient turbine all ']:
                    processedSignal['SignalName'] = 'Gradient turbine'
                    processedSignal['SignalData'] = self.outputFile[instance].get_gradient_turbine()

                elif OutputCommands[0] in ['get aep turbine']:
                    processedSignal['SignalName'] = 'AEP turbine'
                    processedSignal['SignalData'] = self.outputFile[instance].getAEP_turbine()

                elif OutputCommands[0] in ['get turbine list']:
                    processedSignal['SignalName'] = 'getTurbineList'
                    processedSignal['SignalData'] = {
                            'WFName': 'Initial',
                            'LayoutData': self.outputFile[instance].getTurbineList()}

                elif OutputCommands[0] in ['get Farm AEP gradient']:
                    processedSignal['SignalName'] = 'GradientColonel'
                    processedSignal['SignalData'] = self.outputFile[instance]\
                                                        .getGradient()
                else:
                    logger.error('Unknown command produced output')
                    raise TypeError

                #if initial:
                #    processedSignal['SignalName'] = 'getTurbineList'
                self.Q['distributor'].put(processedSignal)

                # -----------------------
                #else:
                #    logger.error('Invalid data in the output file')

            else:  # len(self.outputFile.OutputContents) > 0
                logger.debug('No new data in output file')
        else:  # len(CommandList) == 0:
            logger.debug('No new commands to process')
            logger.debug('CommandList = {}'.format(CommandList))

    def run(self):

        logger.info('Starting')
        logger.info('Process PID = {}'.format(os.getpid()))

        while True:
            try:
                # get data from queue
                logger.debug('Waiting for queue data')
                qSignal = self.Q[self.id].get()

                if checkSignal(qSignal) == 0:
                    logger.debug('Received \'{}\''.format(qSignal['SignalName']))

                    if qSignal['SignalName'] == 'New output file':
                        self.processOutputFile(qSignal)

                    elif qSignal['SignalName'] == 'Terminate':
                        logger.info('Received terminate signal. Exitting')
                        logger.error('This is not implemented yet')
                        break
                    else:
                        logger.debug('Received wrong signal')
                else: 
                    logger.error('Invalid queue data <{}>'.format(qSignal))
            except KeyboardInterrupt:
                logger.warning('Received keyboard interrupt')
                break
            except Exception as e:
                error_str = traceback.format_exc()
                logger.error(error_str)
                self.Q['main'].put([e, error_str])
                #raise e

    def setNewWorketTarget(self, qSignal):
        assert qSignal == 0

        if checkSignal(qSignal) == 0:
            logger.debug('Setting new worker target to <{}>'
                         .format(qSignal['SignalReturn']))
            self.worker_target = qSignal['SignalReturn']
            
            # send back confirmation
            d_confirm= {'SignalName': 'ChangeTarget',
                        'SignalData': self.worker_target,
                        'SignalOrigin': 'OutputProcessor'}
            self.Q['distributor'].put(d_confirm)


class InputProcessor(multiprocessing.Process):
    """
    Wrapper around ColonelInput class
    Created mainly because its easier to manage input commands this way
    """
    Q = None   # container with all the worker input queues
    id = 'input'  # id used to represent this class in queue container

    __inputFile = {}     # dict of ColonelInput instances

    def __init__(self, queue_container):
        logger.info('Initiating InputProcessor')

        multiprocessing.Process.__init__(self)
        self.Q = queue_container
        for ii in COLONEL_INSTANCE_IDENTIFIER:
            self.__inputFile[ii] = ColonelInput(ii)

    def __calculateAEP(self, instance):
        self.addCommand(instance, 'calculate AEP', None)

    def __getAEP(self, instance):
        self.addCommand(instance, 'get total AEP', None)

    def __getAEP_turbine(self, instance, n_turbine):
        self.addCommand(instance, 'get aep turbine', n_turbine)


    def __get_gradient(self, instance):
        self.addCommand(instance, 'get Farm AEP gradient', None)

    def __getTotalAEP(self, instance):
        self.__getAEP(instance)

    def __moveTurbine(self, instanceIdentifier, argumentList):
        #commandData = {'commandName': commandStr,
        #               'commandArguments': argumentsLst,
        #               'commandGroupName':groupName}
        #commandSignal = {'SignalName': 'addCommand',
        #                 'SignalData': commandData,
        #                 'SignalOrigin': 'Optimizer'}
        self.addCommand(instanceIdentifier, 'move turbine', argumentList)

    def addCommand(self, instanceIdentifier, commandName, argumentList, 
                   groupName='Optimization'):
        self.__inputFile[instanceIdentifier].addCommand(
                commandName, argumentList, groupName)

    def get_AEP(self, d_signal):
        logger.debug('Processing AEP signal')
        # move turbines
        colonel_instance = d_signal['SignalTarget']
        x_vec = d_signal['SignalData']
        n_turbines = len(x_vec)/2
        for ii in range(n_turbines):
            args = [ii, x_vec[ii], x_vec[n_turbines + ii]]
            self.__moveTurbine(colonel_instance, args)

        self.__calculateAEP(colonel_instance)
        self.__getAEP(colonel_instance) 
        self.save(colonel_instance)

    def get_AEP_turbine(self, d_signal):
        logger.debug('Processing AEP turbine signal')
        # move turbines
        colonel_instance = d_signal['SignalTarget']
        x_vec = d_signal['SignalData']
        n_turbines = len(x_vec)/2

        for ii in range(n_turbines):
            args = [ii, x_vec[ii], x_vec[n_turbines + ii]]
            self.__moveTurbine(colonel_instance, args)

        self.__calculateAEP(colonel_instance)

        for ii in range(n_turbines):
            self.__getAEP_turbine(colonel_instance, [ii])

        self.save(colonel_instance)

    def get_gradient_turbine(self, d_signal):
        logger.debug('Processing gradient turbine signal')
        # move turbines
        colonel_instance = d_signal['SignalTarget']
        x_vec = d_signal['SignalData']
        n_turbines = len(x_vec)/2

        for ii in range(n_turbines):
            args = [ii, x_vec[ii], x_vec[n_turbines + ii]]
            self.__moveTurbine(colonel_instance, args)

        self.__calculateAEP(colonel_instance)

        self.addCommand(colonel_instance, 'get aep gradient turbine all ', None)

        self.save(colonel_instance)

    def getCommandList(self, instanceIdentifier):
        logger.debug('Sending command list')
        return self.__inputFile[instanceIdentifier].getCommandList()

    def getCommandListString(self, instanceIdentifier):
        return self.__inputFile[instanceIdentifier].getCommandListString()

    def getTurbineList(self, instanceIdentifier):
        self.addCommand(instanceIdentifier,
                        'get turbine list',
                        None,
                        'Initial')
        self.save(instanceIdentifier)
    
    def gradient_colonel(self, d_signal):
        """
        Processes the obtained position vector to produce 
        corresponding Colonel commands
        """
        # number of turbines, assumes only (x,y) position componenets
        numTurb = len(d_signal['SignalData']) / 2
        instance = d_signal['SignalTarget']

        for iTurb in range(numTurb):
            l = [iTurb,
                 d_signal['SignalData'][iTurb],
                 d_signal['SignalData'][numTurb + iTurb]]
            self.__moveTurbine(instance, l)

        self.__calculateAEP(instance)
        self.__get_gradient(instance)
        self.save(instance)

    def reset(self, instanceIdentifier):
        logger.debug('Resetting commandList for <{}>'.format(instanceIdentifier))
        self.__inputFile[instanceIdentifier].resetCommandList()

    def run(self):
        logger.info('Starting')
        logger.info('Process PID = {}'.format(os.getpid()))

        while True:
            try:
                logger.debug('Waiting for queue data')
                queueData = self.Q[self.id].get()
                logger.debug('Received <{}>'.format(queueData))

                if checkSignal(queueData) == 0:
                    logger.debug('Received <{}>'.format(queueData['SignalName']))

                    # determine the target Colonel instance
                    # use 1st instance as a default one
                    #iColonel = COLONEL_INSTANCE_IDENTIFIER[0]
                    iColonel = None
                    if queueData['SignalTarget'] is not None:
                        #iColonel = COLONEL_INSTANCE_IDENTIFIER.index(
                        #        queueData['SignalTarget'])
                        iColonel = queueData['SignalTarget']

                    # process the queue signals
                    if queueData['SignalName'] == 'AEP':
                        self.get_AEP(queueData)

                    elif queueData['SignalName'] == 'AEP turbine':
                        self.get_AEP_turbine(queueData)

                    elif queueData['SignalName'] == 'Gradient turbine':
                        self.get_gradient_turbine(queueData)

                    elif queueData['SignalName'] == 'addCommand':
                        self.addCommand(iColonel, 
                                queueData['SignalData']['commandName'],
                                queueData['SignalData']['commandArguments'],
                                queueData['SignalData']['commandGroupName'])

                    elif queueData['SignalName'] == 'save':
                        #self.__inputFile[iColonel].save()
                        self.save(iColonel)

                    elif queueData['SignalName'] == 'resetCommandList':
                        self.reset(iColonel)

                    elif queueData['SignalName'] == 'getCommandList':
                        dataDict = {'SignalName': queueData['SignalName'],
                                    'SignalData': self.getCommandList(iColonel),
                                    'SignalOrigin': 'InputProcessor',
                                    'SignalTarget': iColonel}
                        # TODO: remove
                        #logger.debug('{}: Sending commandlist <{}>'
                        #             .format(self.name, dataDict))
                        self.Q['output'].put(dataDict)

                    elif queueData['SignalName'] == 'getCommandListString':
                        dataDict = {'SignalName': queueData['SignalName'],
                                    'SignalData': self.getCommandListString(iColonel),
                                    'SignalOrigin': 'InputProcessor'}
                        self.Q['output'].put(dataDict)

                    elif queueData['SignalName'] == 'getTurbineList':
                        self.getTurbineList(iColonel)

                    elif queueData['SignalName'] in ['gradientColonel', 
                                                     'GradientColonel']:
                        self.gradient_colonel(queueData)

                    else:
                        logger.error('{} is not a valid signal name'
                                     .format(queueData['SignalName']))
                else:
                    logger.error('Invalid queue data <{}>'.format(queueData))

            except KeyboardInterrupt:
                logger.warning('Received keyboard interrupt')
                break
            except Exception as e:
                error_str = traceback.format_exc()
                logger.error(error_str)
                self.Q['main'].put([e, error_str])
                #raise e

    def save(self, instanceIdentifier):
        self.__inputFile[instanceIdentifier].save()


class verifier(multiprocessing.Process):
    id = 'verifier'

    def __init__(self, q_container):
        multiprocessing.Process.__init__(self)
        self.Q = q_container
        logger.info('Initiating')

    def run(self):
        logger.info('Starting')
        logger.info('Process PID = {}'.format(os.getpid()))

        data = None
        try:
            while True:
                data = self.Q[self.id].get()
                logger.debug('{}'.format(data))
                #self.Q_out.put(data)
        except KeyboardInterrupt:
            logger.info('Received KeyboardInterrupt. Exitting.')
        except Exception as e:
            error_str = traceback.format_exc()
            logger.error(error_str)
            self.Q['main'].put([e, error_str])
            #raise e


class DataCollector(multiprocessing.Process):
    """
    Worker class used instead of Plotting worker for data collection
    """
    id = 'plotter'

    def __init__(self, q_container,
                 n_elements=WFLOP1_NUMBER_ELEMENTS,
                 max_col=WFLOP1_MAX_ELEMENTS_PER_ROW,
                 event=None, *args, **kwargs):
        logger.info('Initializing')

        multiprocessing.Process.__init__(self)
        self.q_container = q_container
        self.n_elements = n_elements
        self.max_col = max_col

        # TODO try to update this automatically
        # Number of constraints
        self.num_boundcon = 0
        if WFLOP1_CONS_BOUNDARY:
            # assume the boundary arae is a 4 sided polygon
            self.num_boundcon += WFLOP1_NUMBER_ELEMENTS*4 + WFLOP1_NUMBER_ELEMENTS*4
        if WFLOP1_CONS_AREA:
            self.num_boundcon += 1
        if WFLOP1_CONS_PROXIMITY:
            self.num_boundcon += binomial_coefficient(WFLOP1_NUMBER_ELEMENTS, 2)

        self.e_terminate = event

    def run(self):
        DataContainer = namedtuple('DataContainer', ['aep', 'alpha', 'beta', 'bx', 'by', 'dx', 'dy'])

        # preallocate variables
        self.layout_data = np.array([], dtype=np.float)
        self.data = DataContainer(np.array([]), np.array([]), np.array([]), np.array([]),
                                  np.array([]), np.array([]), np.array([]))
        self.data_cons = [np.array([]) for _ in range(self.num_boundcon+3)]

        # iter    objective    inf_pr   inf_du lg(mu)  ||d||  lg(rg) alpha_du alpha_pr  ls
        self.data_iter = [np.array([]) for _ in range(10)]

        # main run loop
        while not self.e_terminate.is_set():
            try:
                self.get_data()
            except KeyboardInterrupt:
                logger.warning('Received KeyboardInterrupt')
                break
            except Exception as e:
                error_str = traceback.format_exc()
                logger.error(error_str)
                self.q_container['main'].put([e, error_str])
   
    def get_data(self):
        if not self.q_container[self.id].empty():
            # get the data from queue
            signal = self.q_container[self.id].get()

            if 'objective' in signal:
                data = signal['objective']
                #self.data.layout.resize(data[0].shape, refcheck=False)
                #self.data.layout[:] = - data[0]
                self.layout_data = data[0]

                self.data.aep.resize(len(self.data.aep) + 1)
                self.data.aep[-1:] = - data[2]

                self.data.alpha.resize(len(self.data.alpha) + 1)
                self.data.alpha[-1:] = data[1][0]

                self.data.beta.resize(len(self.data.beta) + 1)
                self.data.beta[-1:] = data[1][1]

                self.data.bx.resize(len(self.data.bx) + 1)
                self.data.bx[-1:] = data[1][2]

                self.data.by.resize(len(self.data.by) + 1)
                self.data.by[-1:] = data[1][3]

                self.data.dx.resize(len(self.data.dx) + 1)
                self.data.dx[-1:] = data[1][4]

                self.data.dy.resize(len(self.data.dy) + 1)
                self.data.dy[-1:] = data[1][5]

                if ARCHIVE_STATUS:
                    with open(os.path.join(ARCHIVE_FOLDER_PATH, 'progression.csv'), 'a') as f:
                        np.savetxt(f, np.transpose(
                                [self.data.aep[-1:], self.data.alpha[-1:],
                                self.data.beta[-1:], self.data.bx[-1:], self.data.by[-1:],
                                self.data.dx[-1:], self.data.dy[-1:]]))
                    with open(os.path.join(ARCHIVE_FOLDER_PATH, 'layout.csv'), 'a') as f:
                        np.savetxt(f, self.layout_data.reshape((1,-1)))

            elif 'constraints' in signal:
                data = signal['constraints']
                #self.data_cons = np.concatenate((self.data_cons, data.transpose()))
                for ic in range(len(data)):
                    self.data_cons[ic].resize(len(self.data_cons[ic])+1)
                    self.data_cons[ic][-1] = data[ic]

                if ARCHIVE_STATUS:
                    tmp_cons_list = np.array([con[-1] for con in self.data_cons if len(con) > 0])
                    with open(os.path.join(ARCHIVE_FOLDER_PATH, 'constraints.csv'), 'a') as f:
                        np.savetxt(f, tmp_cons_list.reshape((1,-1)))

            # Ipopt iteration data
            elif 'callback' in signal:
                data = signal['callback']
                for ii in range(len(data)-1):
                    self.data_iter[ii].resize(len(self.data_iter[ii])+1)
                    self.data_iter[ii][-1] = data[ii+1]
                if ARCHIVE_STATUS:
                    tmp_iter_list = np.array([iter[-1] for iter in self.data_iter if len(iter) > 0])
                    with open(os.path.join(ARCHIVE_FOLDER_PATH, 'iteration_data.csv'), 'a') as f:
                        np.savetxt(f, tmp_iter_list.reshape((1, -1)))

            else:
                logger.error('Unknown key <{}> in data signal'.format(data.keys))


# filesystem watchers
class FilesystemHandler(FileSystemEventHandler):

    WatchedPath = None
    Q = None
    # list of output file paths to watch
    OutputFileList = [os.path.join(WORKSPACE_PATH, ii, 
        COLONEL_OUTPUT_DIR_PREFIX, COLONEL_OUTPUT_FILE) 
            for ii in COLONEL_INSTANCE_IDENTIFIER]

    def __init__(self, workspace_path=WORKSPACE_PATH, queue_container=None):
        if isinstance(workspace_path, str):
            logger.info('Watchdog: Starting to watch {}'.format(workspace_path))
            self.WatchedPath = workspace_path
        else:
            logger.error('Filename <{}> has to be a string'.format(workspace_path))
        self.Q = queue_container

    def setWatchedPath(self, filename):
        if os.path.exists(filename):
            logger.info('Watchdog: Setting a file to watch {}'.format(filename))
            self.WatchedPath = filename
        else:
            logger.error('Watchdog: {} does not exist'.format(filename))

    def REMOVEon_created(self, event):
        if event.src_path in self.OutputFileList:
            logger.debug('New {}'.format(event.src_path))

            logger.debug('Watchdog: Output file \'{}\' was created'
                        .format(event.src_path))

            # prepare the queue data
            outputDict = {'SignalName': 'New output file',
                          'SignalData': [event.src_path],
                          'SignalOrigin': 'Watchdog'}

            #send a signal through queue
            self.Q['output'].put(outputDict)

    def on_modified(self, event):
        if event.src_path in self.OutputFileList:
            logger.debug('Watchdog: Output file \'{}\' was changed'
                        .format(event.src_path))

            # prepare the queue data
            outputDict = {'SignalName': 'New output file',
                          'SignalData': [event.src_path],
                          'SignalOrigin': 'Watchdog'}

            #send a signal through queue
            self.Q['output'].put(outputDict)


def SpawnObservers(queue_container, outputFilename=WORKSPACE_PATH):
    Q = queue_container

    logger.debug('SpawnObservers got {}'.format(outputFilename))

    # set event handler
    event_handler = FilesystemHandler(outputFilename, queue_container=Q)

    # start observer
    logger.info('Starting file observer')
    observer = Observer()
    observer.schedule(event_handler, path=os.path.dirname(outputFilename), recursive=True)
    observer.start()

    return observer

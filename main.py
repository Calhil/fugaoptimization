"""

"""
import os
#import sys
from shutil import rmtree
#from multiprocessing import Queue 
from multiprocessing.queues import SimpleQueue
from time import sleep
import multiprocessing

import config
from config import logger
from config import NUMBER_COLONEL_INSTANCES
from config import COLONEL_INSTANCE_IDENTIFIER
from config import WFLOP1_MAX_ELEMENTS_PER_ROW as max_col
from config import WFLOP1_NUMBER_ELEMENTS as n_elements
from config import PLOTTING_ENABLED
from config import WFLOP_REGION
#from ColonelInterface import ColonelInput
#from ColonelInterface import ColonelOutput
from workers import SpawnObservers
from workers import InputProcessor, OutputProcessor, ColonelSubprocess
from workers import Distributor
#from workers import verifier
from Optimization import OptimizationWorker
if PLOTTING_ENABLED:
    from Plotting import PlotterPyQt as Plotter
else:
    from workers import DataCollector as Plotter
#from Plotting import PlotterMplt as Plotter
from examples import WFLOP_1
from examples import (WFLOP_2, WFLOP_3)
from hornsrev import (hornsrev,
                    hornsrev_cartesian)
from Vesterhav_syd import VesterhavSyd
from bathymetry import (bathymetry_lattice,
                        bathymetry_cartesian)


if __name__ == "__main__":

    try:
        # ==========================
        # multiple Colonel instances
        # ==========================
        config.RunInitialConfig()

        # signal queue
        # 1 input queue per worker that needs any explicit input
        # main is only used for multiprocess error handling
        worker_ids = ['distributor', 'optimizer', 'input', 'output', 'main', 'plotter']
        q_container = {id: SimpleQueue() for id in worker_ids}

        # terminate event
        e_terminate = multiprocessing.Event()

        logger.info('Initializing workers')
        inputWorker = InputProcessor(q_container)

        # start watching for changes in output file
        obs = SpawnObservers(queue_container=q_container)

        # start Colonel instances
        colonelWorker = []
        for ii in range(NUMBER_COLONEL_INSTANCES):
            colonelWorker.append(ColonelSubprocess(COLONEL_INSTANCE_IDENTIFIER[ii]))

        outputWorker = OutputProcessor(queue_container=q_container)

        plt_worker = Plotter(q_container, n_elements, max_col, event=e_terminate)
        
        #v = verifier(q_DisInp, q1)
        #v.start()

        # queue distributor
        distWorker = Distributor(queue_container=q_container)

        # WFLOP initialization
        if WFLOP_REGION == 'simple':
            wflop = WFLOP_3
            wflop = WFLOP_1
            wflop = WFLOP_2
        
        elif WFLOP_REGION == 'vesterhavsyd':
            wflop = VesterhavSyd

        elif WFLOP_REGION == 'hornsrev':
            wflop = hornsrev
            wflop = hornsrev_cartesian

        elif WFLOP_REGION == 'bathymetry':
            wflop = bathymetry_lattice
            wflop = bathymetry_cartesian

        # optimizer
        optWorker = OptimizationWorker(q_container, wflop, terminate_event=e_terminate)

        logger.info('Starting workers')
        for ii in range(NUMBER_COLONEL_INSTANCES):
            # this delay was added to prevent Colonel instance from 
            # crashing when accessing fuga files
            colonelWorker[ii].start()
            sleep(1)
        #sleep(0.5)
        distWorker.start()
        inputWorker.start()
        # it hangs here?
        sleep(1)
        optWorker.start()
        #sleep(0.5)
        outputWorker.start()
        #sleep(0.5)
        plt_worker.start()

        # intercept errors from other processes
        try:
            while True:
                logger.debug('Active children: <{}>'.format(multiprocessing.active_children()))

                q_error = q_container['main'].get()
                logger.error('Received an exception')
                if isinstance(q_error[0], Exception):
                    if len(q_error) == 2:
                        logger.error(q_error[1])
                    raise q_error[0]
        #except KeyboardInterrupt:
        #    logger.warning('Exitting')
        #    logger.debug('Active children: <{}>'.format(multiprocessing.active_children()))
        finally:
            pass

    except KeyboardInterrupt:
        logger.debug('Active children: <{}>'.format(multiprocessing.active_children()))
        logger.error('Killing workers.')
        obs.stop()
        for ii in range(NUMBER_COLONEL_INSTANCES):
            colonelWorker[ii].terminate()
        inputWorker.terminate()
        outputWorker.terminate()
        optWorker.terminate()
        plt_worker.terminate()

        # check queues
        for q_name in q_container.keys():
            if not q_container[q_name].empty():
                logger.debug('Queue <{}> is not empty'.format(q_name))
            else:
                logger.debug('Queue <{}> is empty'.format(q_name))

        # flush the error msg   from workers import ARCHIVE_FOLDER_PATH as Plotters
        if not q_container['main'].empty():
            logger.info('Flushing error messages')
            while not q_container['main'].empty():
                q_error = q_container['main'].get()
                logger.error('Received an exception')
                if isinstance(q_error[0], Exception):
                    if len(q_error) == 2:
                        logger.error(q_error[1])
                    raise q_error[0]
        else:
            logger.info('Error queue is empty')
        
    except:
        raise

    # cleanup
    finally:
        #logger.debug('Waiting for colonel to finish work')
        try:
            for ii in range(NUMBER_COLONEL_INSTANCES):
                colonelWorker[ii].join()
            #obs.join()
            #outputWorker.join()
            #optWorker.join()
            logger.debug('Colonel finished work')

        finally:
            # remove the temporary workspace dir
            if os.path.exists(config.WORKSPACE_PATH):
                if config.DELETE_WORKSPACE_DIR:
                    logger.info('Removing temporary workspace directory')
                    #os.removedirs(config.WORKSPACE_PATH)
                    rmtree(config.WORKSPACE_PATH)
            else:
                logger.warning('Temporary workspace directory missing.')

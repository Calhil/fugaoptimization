"""

"""

from copy import deepcopy

import numpy as np

from workers import checkSignal
from config import (logger,
                    OPT_GRADIENT,
                    GRADIENT_COORD_SYS)

class BaseWFLOP(object):
    """
    This is the base WFLOP class.
    All problems should create a child class and inherit methods from it.
    
    Methods:
    -------
    get_initial_setup - returns the variales needed for particular optimizer
    objfun          - objective function
    confun          - constraint function
    grad_objfun     - gradient of the objective function
    jac_confun      - jacobian of the constraint function
    hess_objfun     - hessian of the objective function
    hess_confun     - hessian of the constraint function
    """
    
    id = 'optimizer'

    def __init__(self, *args, **kwargs):
        pass

    def initial_setup(self):
        raise NotImplementedError

    def objfun(self, *args, **kwargs):
        raise NotImplementedError

    def confun(self, *args, **kwargs):
        raise NotImplementedError

    def grad_objfun(self, *args, **kwargs):
        raise NotImplementedError

    def jac_confun(self, *args, **kwargs):
        """
        This function expects a flat np.ndarray as an output
        in case flag == False
        """
        raise NotImplementedError

    def hess_objfun(self, *args, **kwargs):
        raise NotImplementedError

    def hess_confun(self, *args, **kwargs):
        raise NotImplementedError


class WindFarm(object):
    """
    Class representing the wind farm in the WAsP workspace
    It keeps track of changes in the wind turbine layout
    """
    # variable representing the WT layout and other data about a wind farm
    # layout = {'WFName': NameString,
    #           'LayoutData': [{
    #                   'Number': int,
    #                   'Id': str,
    #                   'Model': str,
    #                   'x': float,
    #                   'y': float,
    #                   'Status': str},
    #                   {...}, ...
    #                   ]
    # }
    layout = None
    q_dict = None    # queue used to output commands to the InputProcessor
    id = 'optimizer'
    #__inputFile = None

    def __init__(self, queue_container, initialLayout=None,
                 NumberWT=None, X=None):
        """
        initialLayout
        NumberWT - number of wind turbines set in the optimization routine
        X - position vector, same as in the optimization routine
        """
        #self.__inputFile = inputFile
        self.q_dict = queue_container

        # send request for turbine list
        rDict = {'SignalName': 'getTurbineList',
                'SignalData': None,
                'SignalOrigin': 'optimizer',
                'SignalTarget': '0'}
        self.q_dict['distributor'].put(rDict)
        logger.debug('Sent turbine list request')

        initialSignal = self.q_dict['optimizer'].get()
        if checkSignal(initialSignal) != 0:
            logger.error('Received wrong initial wind farm layout')
        
        InitialLayout = initialSignal['SignalData']
        #if InitialWFlayout['WFName'] in ['Initial', 'Turbine List']:
        #    logger.debug('Received initial WF layout')
        #    wf_layout = WindFarm(self.q_dict,
        #                        initialLayout=InitialWFlayout,
        #                        NumberWT=NumberWT)
        #else:
        #    logger.error('Non-initial layout in queue')

        if InitialLayout is not None and \
                self.__checkLayout(InitialLayout ) == 0:
            self.layout = InitialLayout 

            # adjust the layout to the optimization parameters
            # ------------------------------------------------

            #self.__inputFile.resetCommandList()

            if NumberWT is not None and X is not None:
                if NumberWT != len(X/2):
                    logger.error('NumberWT and the length of X has to be the same')

            if NumberWT is not None and NumberWT > 0:
                self.setNumberWindTurbines(NumberWT)

            # set the X,Y coordinates
            if X is not None:
                self.setCoordinates(X)
                self.saveAndReset()

        else:
            logger.debug('InitialLayout = <{}>'.format(InitialLayout))
            logger.error('WindFarm.__init__: Received wrong layout')
            raise TypeError

    def addCommand(self, commandStr, argumentsLst=None, groupName='Optimization'):
        commandData = {'commandName': commandStr,
                       'commandArguments': argumentsLst,
                       'commandGroupName':groupName}
        commandSignal = {'SignalName': 'addCommand',
                         'SignalData': commandData,
                         'SignalOrigin': 'Optimizer'}
        self.q_dict['distributor'].put(commandSignal)
        
    def getWTNumber(self):
        return len(self.layout['LayoutData'])

    def get_colonel_layout(self):
        pass

    def get_coordinates(self):
        n_turbines = self.getWTNumber()
        tmp_x = np.zeros((n_turbines*2, 1))
        for ii in range(n_turbines):
            self.layout['LayoutData'][ii]['x'] = tmp_x[ii]
            self.layout['LayoutData'][ii]['y'] = tmp_x[n_turbines + ii]

        return tmp_x

    def get_wt_coordinates(self):
        pass

    def processCommands(self):
        commandSignal = {'SignalName': 'save',
                         'SignalData': None,
                         'SignalOrigin': 'Optimizer'}
        self.q_dict['distributor'].put(commandSignal)

    def get_AEP(self, x_vec):
        """
        Returns AEP obtained using Colonel having the position vector
        """

        d_aep = {'SignalName': 'AEP',
                 'SignalData': x_vec,
                 'SignalOrigin': 'Optimizer'}
        self.q_dict['distributor'].put(d_aep)

        logger.debug('Waiting for AEP data from queue')
        s_data = self.q_dict[self.id].get()
        
        #logger.debug('WindFarm.get_AEP(): received <{}>'
        #             .format(queueData))
        AEP = None
        if checkSignal(s_data) == 0:
            if s_data['SignalData']['Name'] == 'AEP':
                # <net AEP (GWh)> < gross AEP (GWh)> <capacity factor> 
                AEP = s_data['SignalData']['Data'][0]
                #print 'AEP =', s_data['SignalData']['Data']
            else:
                logger.error('WindFarm.get_AEP received wrong data <{}>'
                            .format(s_data))
        return AEP

    def get_AEP_for_turbine(self, x_vec):
        """
        Returns AEP for each of the specified turbines
        obtained using Colonel having the position vector
        """

        d_aep = {'SignalName': 'AEP turbine',
                'SignalData': x_vec,
                 'SignalOrigin': 'Optimizer'}
        self.q_dict['distributor'].put(d_aep)

        logger.debug('Waiting for AEP data from queue')
        s_data = self.q_dict[self.id].get()
        
        #logger.debug('WindFarm.get_AEP(): received <{}>'
        #             .format(queueData))
        AEP_list = None
        if checkSignal(s_data) == 0:
            if s_data['SignalData']['Name'] == 'AEP turbine':
                # <net AEP (GWh)> < gross AEP (GWh)> <capacity factor> 
                AEP_list = s_data['SignalData']['Data']
                #print 'AEP =', s_data['SignalData']['Data']
            else:
                logger.error('WindFarm.get_AEP received wrong data <{}>'
                            .format(s_data))
        return AEP_list

    def get_gradAEP(self, x_vec, step_size=None, gradient_source=None, coordinate_system=None):
        # determine gradient calculation type
        if gradient_source is None:
            gradient_source = OPT_GRADIENT
        if coordinate_system is None:
            coordinate_system = GRADIENT_COORD_SYS

        d = {}
        if gradient_source == 'FD' and coordinate_system == 'cartesian':
            d['SignalName'] = 'GradientFD'
        elif gradient_source == 'Colonel':
            d['SignalName'] = 'GradientColonel'
        elif gradient_source == 'FD' and coordinate_system == 'lattice':
            d['SignalName'] = 'GradientLattice'
        else:
            logger.error('Unimplemented gradient type: <{}>'.format(gradient_source))
        d['SignalData'] = x_vec
        d['SignalOrigin'] = 'Optimizer'
        
        if step_size is not None:
            d['step_size'] = step_size

        # send a request to calculate the gradient at x
        self.q_dict['distributor'].put(d)

        # wait for the gradient to be calculated
        logger.debug('Waiting for gradient')
        grad_signal = self.q_dict[self.id].get()

        logger.debug('Gradient received. <{}>'.format(grad_signal))
        grad_aep = grad_signal['SignalData']

        # process Colonel derived gradient
        if gradient_source == 'Colonel':
            grad_aep = np.array(grad_aep)
            grad_aep = np.hstack((grad_aep[:,0], grad_aep[:,1])).reshape((-1, 1))

        return grad_aep

    def get_gradAEP_turbine(self, x_vec):
        # determine gradient calculation type
        d = {}
        d['SignalName'] = 'Gradient turbine'
        d['SignalData'] = x_vec
        d['SignalOrigin'] = 'Optimizer'

        # send a request to calculate the gradient at x
        self.q_dict['distributor'].put(d)

        # wait for the gradient to be calculated
        logger.debug('Waiting for gradient')
        grad_signal = self.q_dict[self.id].get()

        logger.debug('Gradient turbine received. <{}>'.format(grad_signal))
        tmp = np.array(grad_signal['SignalData']['Data'], dtype=np.float).reshape((-1, len(x_vec) + 1))
        # get rid of the last column with turbine numbers
        grad_aep_turbine = tmp[:, :-1]

        return grad_aep_turbine

    def saveAndReset(self):
        """
        Saves the input file and resets the commandList
        for all the Colonel instances
        This is used only when changing the WF layout
        since it produces no output
        """
        commandSignal = {'SignalName': 'saveAndReset',
                         'SignalData': None,
                         'SignalOrigin': 'Optimizer'}
        self.q_dict['distributor'].put(commandSignal)

    def setCoordinates(self, XY):
        assert type(XY) == np.ndarray
        logger.debug('Setting wind turbine coordinates')

        # check the vector
        if len(XY) % 2 == 0:
            # check if the number of coordinates is the same 
            # as the number of wind turbines in the layout

            n_turbines = len(XY)/2

            # TODO remove this
            logger.debug('len(XY)/2 = {}'.format(n_turbines))
            
            # adjust the WT number of necessary
            self.setNumberWindTurbines(n_turbines)


            # set the coordinates
            for ii in range(n_turbines):
                logger.debug('Setting coordinates for turbine {}'.format(ii))

                # set the coordinates for this class
                self.layout['LayoutData'][ii]['x'] = XY[ii]
                self.layout['LayoutData'][ii]['y'] = XY[n_turbines + ii]

                # set coordinates in Colonel
                self.addCommand('move turbine',
                        [int(self.layout['LayoutData'][ii]['Number']),
                         XY[ii],
                         XY[n_turbines + ii]])
        else:
            logger.error('Invalid coordinate vector received <{}>'.format(XY))

    def setLayout(self, layoutDict):
        """ Sets the WT layout given a proper layoutDict """
        if isinstance(layoutDict, dict)\
                and len(layoutDict) > 0:
            self.layout = layoutDict
        else:
            logger.error('WindFarm.setLayout: Received wrong layout')

    def setNumberWindTurbines(self, Number):
        # adjust the number of wind turbines
        lenLayout = self.getWTNumber() 
        
        # add wind turbines
        if Number > lenLayout:
            logger.debug('numberWT={}, layoutLen={}'.format(Number, lenLayout))
            logger.debug('Adding wind turbines to the layout.')
            # assuming there are already some entries in the layout
            # since it was checked by __checkLayout

            # copy the entries
            for ii in range(Number - lenLayout):
                # copies the last element
                self.layout['LayoutData'].append(
                        deepcopy(self.layout['LayoutData'][-1]))

                # add commands to the input file
                self.addCommand('insert turbine',
                         #[#int(self.layout['LayoutData'][-1]['Id'])+1, 
                         ['\"{}\"'.format(int(self.layout['LayoutData'][-1]['Id'])+1), 
                         self.layout['LayoutData'][-1]['Model'], 
                         self.layout['LayoutData'][-1]['x'],
                         self.layout['LayoutData'][-1]['y']])
            self.saveAndReset()

        # remove wind turbines
        elif Number < lenLayout:
            # This is just a simple loop to remove 
            # the last (lenLayout-Number) of turbines
            logger.debug('Removing wind turbines from the layout.')
            # TODO there probably is a much better way to do this
            # ie. copying only a part of the list to a new variable
            for ii in range(lenLayout - Number):
                # the command has to be added first
                # to get the data before turbine is removed from the list
                self.addCommand('remove turbine',
                        [int(self.layout['LayoutData'][-1]['Number'])])
                        #[int(self.layout['LayoutData'][-1]['Id'].replace('"', ''))])

                self.layout['LayoutData'].pop()
            self.saveAndReset()

        else:
            logger.debug('No need to change the WT layout')

    def __checkLayout(self, WFLayout):
        """ Checks the WFLayout data structure """
        status = 1
        if isinstance(WFLayout, dict):
            if 'WFName' in WFLayout:
                if 'LayoutData' in WFLayout:
                    if len(WFLayout['LayoutData']) != 0:
                        # checking only the first element in the list !!
                        dataStatus = []
                        for iKey in ['Number', 'Id', 'Model', 'x', 'y', 'Status']:
                            if iKey not in WFLayout['LayoutData'][0]:
                                logger.error('{}[\'LayoutData\'[0] has no key {}'.format(WFLayout, iKey))
                                dataStatus.append(1)
                        if len(dataStatus) == 0:
                            # WFLayout is valid
                            status = 0
                            logger.debug('Layout is valid')
                    else:
                        logger.error('{} has no layout data'.format(WFLayout['WFName']))
                else:
                    logger.error('{} has no key \'WFLayout\''.format(WFLayout['WFName']))
            else:
                logger.error('{} has no key \'WFName\''.format(WFLayout))
        else:
            logger.error('{} has to be a dictionary'.format(WFLayout))

        return status


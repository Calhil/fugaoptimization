"""
WFLOP test cases

"""
#from collections import namedtuple
import time

import numpy as np
import sympy as sp
import latex

import  NumericalMethods as nm
import Symbolic as sb
from WFLOP import BaseWFLOP
from WFLOP import WindFarm
#from workers import checkSignal
from config import (logger,
                    WFLOP1_MAX_ELEMENTS_PER_ROW,
                    WFLOP1_NUMBER_ELEMENTS,
                    WFLOP1_SCALING,
                    WFLOP2_SCALING,
                    WFLOP1_CONS_BOUNDARY,
                    WFLOP1_CONS_AREA,
                    WFLOP1_CONS_PROXIMITY,
                    WFLOP1_SCALING_CON,
                    WFLOP3_SCALING,
                    NUMBER_COLONEL_INSTANCES,
                    GRADIENT_COORD_SYS,
                    CD_COEFF,
                    FD_ORDER,
                    OPT_GRADIENT)

np.set_printoptions(linewidth=150, precision=16, suppress=False, formatter={'float': lambda x: format(x, '6.8E')})


def save_latex_to_pdf(expr, filename):
    output_expr = r"""\documentclass[12pt]{article} 

    \usepackage{amsmath}
    \usepackage{amsfonts}
    \usepackage{euler}

    \begin{document}

    """ + sp.latex(expr, mode='inline') + \
    r"""
    \end{document}
    """

    pdf = latex.build_pdf(output_expr)
    pdf.save_to(filename)




class WFLOP_1(BaseWFLOP):

    scale = WFLOP1_SCALING  # scaling factors tuple
    scale_con = WFLOP1_SCALING_CON
    id = 'optimizer'
     
    def test_aep_grad(self):

        logger.info('Testing AEP and gradient')

        x_tmp = np.array([1000, 0, 1000, 0], dtype=np.float)
        ##x_tmp = np.array([0, 0], dtype=np.float)
        n_x, n_y = 101, 101
        #n_x, n_y = 10, 10
        x_space = np.linspace(0, 2000, n_x)
        y_space = np.linspace(0, 2000, n_y)
        ##y_space = np.array([900, 1000, 1050], dtype=np.float)
        aep_array = np.zeros((n_x, n_y))
        #
        grad_array = np.zeros((n_x*len(y_space), 2 + 2*WFLOP1_NUMBER_ELEMENTS))

        tic = time.time()

        for idx_x, x_i in enumerate(x_space):
            for idx_y, y_i in enumerate(y_space):
                logger.info('Doing {}.{}'.format(idx_x, idx_y))
                # MOVE 1 TURBINE TO THIS COORDS
                x_tmp[1] = x_i
                x_tmp[3] = y_i

                # GRADIENT
                grad = self.wf.get_gradAEP(x_tmp)

                grad_array[idx_x * len(y_space) + idx_y, 0] = x_i
                grad_array[idx_x * len(y_space) + idx_y, 1] = y_i
                grad_array[idx_x * len(y_space) + idx_y, 2:] = grad.reshape((-1,))

                with open('/dev/shm/grad_smooth_tmp.csv', 'a') as f:
                    np.savetxt(f, grad_array[idx_x * len(y_space) + idx_y, :].reshape((1, -1)))

                # AEP
                aep = self.wf.get_AEP(x_tmp)
                aep_array[idx_x, idx_y] = aep

                with open('/dev/shm/aep_array_smooth_new_tmp.csv', 'a') as f:
                    np.savetxt(f, np.array([x_i, y_i, aep]).reshape((1, -1)))

        with open('/dev/shm/aep_array_smooth_new.csv', 'a') as f:
            np.savetxt(f, aep_array)
        with open('/dev/shm/grad_smooth.csv', 'a') as f:
            np.savetxt(f, grad_array)

        logger.info('It took {}s to finish'.format(time.time() - tic))
        time.sleep(9999999)

    def __init__(self, queue_list):
        logger.info('Initiating WFLOP#1')
        self.q_container = queue_list

        self.wf = WindFarm(self.q_container, NumberWT=WFLOP1_NUMBER_ELEMENTS)

        # wind farm boundary point coordinates
        self.boundary_poly = np.array([[0., 2000., 2000.,    0.],
                                       [0.,    0., 2000., 2000.]], dtype=np.float)

        # initial shape of the lattice
        #x_lattice = nm.adjust_vector_shape(nm.get_lattice_mapping(
        #    WFLOP1_NUMBER_ELEMENTS, WFLOP1_MAX_ELEMENTS_PER_ROW))
        self.n_elements = WFLOP1_NUMBER_ELEMENTS
        self.max_col = WFLOP1_MAX_ELEMENTS_PER_ROW

        # symbolic stuff
        self.vars = sb.WFLOP1_vars()
        logger.info('Deriving constraints for WFLOP1')
        self.con_expr = sb.WFLOP1_constraints(self.vars, self.boundary_poly)

        # function calculating constraint
        self.con_fun = sb.get_confun(self.vars, self.con_expr)

        # function calculating the jacobian of constraints
        logger.info('Deriving jacobian of constraints for WFLOP1')
        jaccon_expr = sb.get_jaccon_expr(self.vars, self.con_expr, simplify_expr=False)

        self.jaccon_fun = sb.get_jaccon_fun(self.vars, jac_constraints=jaccon_expr)

        # list of non zero rows and cols of jacobian
        self.jaccon_nz = sb.get_jac_nz_pos(jaccon_expr)

        # function returning dx/dvars used in gradient of the obj function
        dxdvars_expr = sb.WFLOP1_dxdvars(self.vars, self.n_elements, self.max_col)
        self.dxdvars_fun = sb.get_dxdvars_fun(self.vars, dxdvars_expr=dxdvars_expr)

        #logger.info(dxdvars_expr)
        #logger.info('Constraints:')
        #logger.info(sp.latex(self.con_expr))
        #save_latex_to_pdf(sp.simplify(self.con_expr), '/dev/shm/constraints.pdf')

        #logger.info('Jacobian of the constraints:')
        #logger.info(latex(jaccon_expr))
        #save_latex_to_pdf(sp.simplify(jaccon_expr), '/dev/shm/jacobian_constraints.pdf')

        #logger.info('dx/dvars:')
        #logger.info(latex(dxdvars_expr))
        #save_latex_to_pdf(sp.simplify(dxdvars_expr), '/dev/shm/dxdvars.pdf')

    def check_vector_errors(self):
        x0 = np.array([1, 1, 500, 500, 100, 100], dtype=np.float)
        print x0

        x_cartesian = nm.get_wt_coordinates(*x0, n_elements=self.n_elements,
                                  max_col=self.max_col)

        # use this to save Colonel gradients
        #gradients = self.wf.get_gradAEP(x_cartesian)
        #print gradients.reshape((-1,1))
        #
        #dxdvars = self.dxdvars_fun(*x0)
        #grad_lattice = gradients.T.dot(dxdvars)
        #print grad_lattice
        #print
        #np.savetxt('Data/Colonel_grad_cartesian.dat', gradients)
        #np.savetxt('Data/Colonel_grad_lattice.dat', grad_lattice)
        #raise

        grad_colonel_cart = np.loadtxt('Data/Colonel_grad_cartesian.dat').reshape((1,-1))
        grad_colonel_latt = np.loadtxt('Data/Colonel_grad_lattice.dat').reshape((1,-1))

        print 
        print grad_colonel_latt
        print 

        output_file = 'Data/Gradient_testing.dat'

        dxdvars = self.dxdvars_fun(*x0)
        for ih in np.linspace(11, 100, 25):
            logger.info('==============')
            logger.info('Step size <{}>'.format(ih))
            grad_cart = self.wf.get_gradAEP(x_cartesian, step_size=ih).reshape((1,-1))
            grad_lattice = np.array(grad_cart.dot(dxdvars)).reshape((1,-1))
            print grad_lattice
            print 

            err_abs_cart = np.linalg.norm(grad_colonel_cart - grad_cart) 
            err_rel_cart_colfd = np.linalg.norm(grad_colonel_cart - grad_cart) / np.linalg.norm(grad_colonel_cart)
            err_rel_cart_fdcol = np.linalg.norm(grad_cart - grad_colonel_cart) / np.linalg.norm(grad_cart)
            err_abs_latt = np.linalg.norm(grad_colonel_latt - grad_lattice)
            err_rel_latt_colfd = np.linalg.norm(grad_colonel_latt - grad_lattice) / np.linalg.norm(grad_colonel_latt)
            err_rel_latt_fdcol = np.linalg.norm(grad_lattice - grad_colonel_latt) / np.linalg.norm(grad_lattice)

            with open(output_file, 'a') as f:
                tmp = [ih] + grad_cart.tolist()[0] + grad_lattice.tolist()[0] + [err_abs_cart, err_rel_cart_colfd,
                       err_rel_cart_fdcol, err_abs_latt, err_rel_latt_colfd, err_rel_latt_fdcol]
                tmp = np.array(tmp, dtype=np.float).reshape((1,-1)) 
                np.savetxt(f, tmp)

    def check_lattice_errors(self):
        output_file = 'Data/Gradient_lattice_testing2.dat'

        for i_spacing in np.linspace(10, 1000, 100):
            logger.info('==============')
            logger.info('Spacing <{}>'.format(i_spacing))

            x0 = np.array([1, 1, i_spacing, i_spacing, 100, 100], dtype=np.float)
            x_cartesian = nm.get_wt_coordinates(*x0, n_elements=self.n_elements,
                                  max_col=self.max_col)

            dxdvars = self.dxdvars_fun(*x0)

            grad_colonel_cart = self.wf.get_gradAEP(x_cartesian, gradient_source='Colonel').reshape((1,-1))
            grad_colonel_latt = grad_colonel_cart.dot(dxdvars)
            print grad_colonel_latt

            #grad_cart = self.wf.get_gradAEP(x_cartesian, step_size=1).reshape((1,-1))
            #grad_lattice = np.array(grad_cart.dot(dxdvars)).reshape((1,-1))
            
            x0 = np.array([x0[ii]/self.scale[ii+1] for ii in range(len(x0))])
            grad_lattice = self.grad_objfun(x0, gradient_source='FD', coordinate_system='lattice')
            grad_lattice = [grad_lattice[i] * (self.scale[0]/self.scale[i+1]) for i in range(len(x0))]
            grad_lattice = np.array(grad_lattice, dtype=np.float).reshape((1,-1))

            print grad_lattice
            print 

            #err_abs_cart = np.linalg.norm(grad_colonel_cart - grad_cart) 
            #err_rel_cart_colfd = np.linalg.norm(grad_colonel_cart - grad_cart) / np.linalg.norm(grad_colonel_cart)
            #err_rel_cart_fdcol = np.linalg.norm(grad_cart - grad_colonel_cart) / np.linalg.norm(grad_cart)
            err_abs_latt = np.linalg.norm(grad_colonel_latt - grad_lattice)
            err_rel_latt_colfd = np.linalg.norm(grad_colonel_latt - grad_lattice) / np.linalg.norm(grad_colonel_latt)
            err_rel_latt_fdcol = np.linalg.norm(grad_lattice - grad_colonel_latt) / np.linalg.norm(grad_lattice)

            #tmp = [i_spacing] + grad_cart.tolist()[0] + grad_colonel_cart.tolist()[0] + \
            #        grad_lattice.tolist()[0] + grad_colonel_latt.tolist()[0] + \
            #        [err_abs_cart, err_rel_cart_colfd, err_rel_cart_fdcol, err_abs_latt, err_rel_latt_colfd, err_rel_latt_fdcol]
            tmp = [i_spacing] + grad_lattice.tolist()[0] + grad_colonel_latt.tolist()[0] + \
                    [err_abs_latt, err_rel_latt_colfd, err_rel_latt_fdcol]
            tmp = np.array(tmp, dtype=np.float).reshape((1,-1)) 

            with open(output_file, 'a') as f:
                np.savetxt(f, tmp)

    def initial_setup(self):
        logger.info('Setting up WFLOP#1')

        # Variable order:
        # ==============
        # x[0] - alpha  - rotation angle of the WF
        # x[1] - beta   - shear angle fo the WF
        # x[2] - b_x    - distance between WT in a row
        # x[3] - b_y    - distance between rows
        # x[4] - d_x    - horizontal translation of the WF
        # x[5] - d_y    - vertical translation of the WF

        #self.test_aep_grad()


        #self.check_vector_errors()
        #self.check_lattice_errors()
        #time.sleep(999999999999999)


        # constraint bounds
        # -----------------
        b_u, b_l, area_l, area_u, prox_l, prox_u = [], [], [], [], [], [] 
        # TODO
        # add scaling!
        if WFLOP1_CONS_BOUNDARY:
            # linalg
            b_u = np.zeros((4*WFLOP1_NUMBER_ELEMENTS,), dtype=np.float)
            #b_u = np.zeros((4*self.n_elements,), dtype=np.float)
            b_l = np.ones(b_u.shape)*-1e19

            # winding number
            #b_l = np.ones((self.n_elements,), dtype=np.float)*np.pi
            #b_u = np.ones((self.n_elements,), dtype=np.float)*1e20

            #n_boundary_cons = (self.boundary_poly[0,:]) * self.n_elements
        
        # TODO
        # add scaling!
        if WFLOP1_CONS_AREA:
            area_l = np.array([-2000.*2000.], dtype=np.float)
            #area_l *= self.scale.d_x*self.scale.d_y
            
            area_u = np.array([2000.*2000.], dtype=np.float)
            #area_u *= self.scale.d_x*self.scale.d_y
            #n_area_cons = 1

        # TODO
        # add scaling!
        if WFLOP1_CONS_PROXIMITY:
            #prox_l = np.array([50], dtype=np.float)
            #prox_u = np.array([1000], dtype=np.float)
            #prox_l = np.array([200., 200.], dtype=np.float)
            #prox_u = np.array([1e19, 1e19], dtype=np.float)
            
            # number of proximity constraints
            n_prox_cons = nm.binomial_coefficient(self.n_elements, 2)

            prox_l = np.array([200.]*n_prox_cons, dtype=np.float)
            prox_u = np.array([1e19]*n_prox_cons, dtype=np.float)

        
        x_min = min(self.boundary_poly[0])
        x_max = max(self.boundary_poly[0])
        y_min = min(self.boundary_poly[1])
        y_max = max(self.boundary_poly[1])
        # number of variables
        n = len(self.vars)

        # xl is the lower bound of x as bounded constraints 
        #xl = np.array([0., -(90-5)*np.pi/180., 50., 50., x_min, y_min], dtype=np.float)
        #xl = np.array([-1e19, -1e19, 200., 200., x_min, y_min], dtype=np.float)
        xl = np.array([-1e19, 0., 200., 200., x_min, y_min], dtype=np.float)
        #xl = np.array([0, 0, 200., 200., x_min, y_min], dtype=np.float)
        xl = np.array([xl[i]/self.scale[i+1] for i in range(len(xl))])

        # xu is the upper bound of x as bounded constraints 
        #xu = np.array([2*np.pi, (90-5)*np.pi/180., 2000., 2000., x_max, y_max], dtype=np.float)
        #xu = np.array([2*np.pi, 0., 2000., 2000., x_max, y_max], dtype=np.float)
        #xu = np.array([1e19, 1e19, 2000., 2000., x_max, y_max], dtype=np.float)
        xu = np.array([1e19, 2.*np.pi, 2000., 2000., x_max, y_max], dtype=np.float)
        #xu = np.array([0, 0, 2000., 2000., x_max, y_max], dtype=np.float)
        xu = np.array([xu[i]/self.scale[i+1] for i in range(len(xu))])

        # m is the number of constraints, 
        m = len(self.con_expr)

        # gl is the lower bound of constraints 
        gl = np.concatenate((b_l, area_l, prox_l))

        # gu is the upper bound of constraints both gl,
        # gu should be one dimension arrays with length m 
        gu = np.concatenate((b_u, area_u, prox_u))
        # nnzj is the number of nonzeros in Jacobi matrix 

        nnzj = len(self.jaccon_nz[0])
        # nnzh is the number of non-zeros in Hessian matrix, you can set it to 0 
        nnzh = 0
        # initial coordinates
        #x0 = np.array([30.*np.pi/180, 20.*np.pi/180, 300, 300, 200., 200.], dtype=np.float)  # doesnt converge
        #x0 = np.array([8.6256102926054634,
        #               16.303640102141475,
        #               259.53309464812861,
        #               256.38950501375939,
        #               905.79936010809081,
        #               660.07172667560576], dtype=np.float)
        x0 = np.array([20.*np.pi/180, 90.*np.pi/180, 200, 200, 500, 500], dtype=np.float)
        #x0 = np.array([-20.*np.pi/180, -10.*np.pi/180, 200, 200, 1900, 1900], dtype=np.float)
        #x0 = np.array([30.*np.pi/180, 20.*np.pi/180, 500, 500, 100, 100], dtype=np.float)
        x0 = np.array([x0[ii]/self.scale[ii+1] for ii in range(len(x0))])
        #x0 = (xl+xu)/2.
        
        logger.info('Number of variables = {}'.format(n))
        logger.info('Number of constraints = {}'.format(m))
        logger.info('Number of nonzero elements in jaccon = {}'.format(nnzj))
        logger.info('Variable Constraints:')
        logger.info('Lower: <{}>'.format(xl))
        logger.info('Upper: <{}>'.format(xu))
        logger.info('Constraints bounds:')
        logger.info('Lower: <{}>'.format(gl))
        logger.info('Upper: <{}>'.format(gu))
        logger.info('Initial position'.format())
        logger.info('alpha = {}'.format(x0[0]))
        logger.info('beta = {}'.format(x0[1]))
        logger.info('b_x = {}'.format(x0[2]))
        logger.info('b_y = {}'.format(x0[3]))
        logger.info('d_x = {}'.format(x0[4]))
        logger.info('d_y = {}'.format(x0[5]))

        return n, xl, xu, m, gl, gu, nnzj, nnzh, x0

    def apply_scaling(self, expression):
        return False

    def remove_scaling(self, vars):
        return [vars[ii]*self.scale[ii+1] for ii in range(len(vars))]
    
    def bathymetry_cost(self, X):
        """
        Calculates corresponding foundation cost of the wind farm
        given an X position vector

        Right now it just ads the y componenets of the wind turbines
        """

        f_cost = np.sum(X[WFLOP1_NUMBER_ELEMENTS:])

        return f_cost

    def objfun(self, vars, *args, **kwargs):
        logger.debug('Calling objective function')

        logger.debug('Vars BEFORE scaling: <{}>'.format(vars))
        vars = self.remove_scaling(vars)
        logger.debug('obj: vars AFTER scaling: <{}>'.format(vars))

        X = nm.get_wt_coordinates(*vars, n_elements=self.n_elements,
                                  max_col=self.max_col)
        logger.debug('X AFTER scaling: <{}>'.format(X))

        AEP = self.wf.get_AEP(X)

        # apply scaling
        AEP /=  self.scale.aep

        self.q_container['plotter'].put({'objective': [X, vars, AEP]})

        return AEP

    def confun(self, vars, *args, **kwargs):
        logger.debug('Calling constraints')

        logger.debug('confun BEFORE vars=<{}>'.format(vars))

        vars = self.remove_scaling(vars)
        cons = self.con_fun(*vars)

        logger.debug('confun AFTER vars=<{}>'.format(vars))
        
        # apply scaling
        cons_scaled = cons / self.scale_con

        # send data for plotting
        self.q_container['plotter'].put({'constraints': cons})

        return cons_scaled

    def grad_objfun(self, vars, gradient_source=None, coordinate_system=None, *args, **kwargs):
        logger.debug('Calling gradient of the objective function')

        if gradient_source is None:
            gradient_source = OPT_GRADIENT
        if coordinate_system is None:
            coordinate_system = GRADIENT_COORD_SYS

        # remove the scaling
        vars = self.remove_scaling(vars)
        logger.debug('grad_obj: vars AFTER scaling: <{}>'.format(vars))

        if coordinate_system == 'cartesian':
            logger.debug('Gradient in the cartesian coord. sysytem')
            X = nm.get_wt_coordinates(*vars, n_elements=self.n_elements,
                                    max_col=self.max_col)
            #logger.info('Processed vars into X = <{}>'.format(X))
            grad_aep = self.wf.get_gradAEP(X,
                    gradient_source=gradient_source,
                    coordinate_system=coordinate_system)

            dxdvars = self.dxdvars_fun(*vars)

            #grad_obj_vars = np.dot(np.array(grad_obj).T, dxdvars)
            grad_obj_vars = np.array(np.array(grad_aep).T.dot(dxdvars))

        elif gradient_source == 'FD' and coordinate_system == 'lattice':
            logger.debug('Calculating gradient in the lattice coordinate system')
            if NUMBER_COLONEL_INSTANCES == 1:
                grad_obj_vars = self.__gradient_lattice(vars)
            else:
                grad_obj_vars = self.__gradient_lattice_multi(vars)
        else:
            raise TypeError('GRADIENT_COORD_SYS has invalid value <{}>'.format(coordinate_system))

        # reapply scaling
        grad_aep_scaled = [grad_obj_vars[0][i] / (self.scale[0]/self.scale[i+1]) \
                           for i in range(len(vars))]
        grad_aep_scaled = np.array(grad_aep_scaled, dtype=np.float)

        logger.debug('grad_aep_scaled = {}'.format(grad_aep_scaled))


        return grad_aep_scaled

    def __gradient_lattice(self, vars):
        """Calculate AEP gradient in the lattice coordinate system using central diff"""
        assert 0 <= FD_ORDER - 1 <= len(CD_COEFF)

        # perturbation distances in the lattice coord. system
        vars_pert = [0.5/180.*np.pi, 0.5/180.*np.pi, 1., 1., 1., 1.]

        # create perturbed matrices
        grad_vars = np.zeros_like(vars)

        # perturbation matrix, assumes x_pert=y_pert
        h = range(-len(CD_COEFF[FD_ORDER-1])/2, len(CD_COEFF[FD_ORDER-1])/2 + 1)
        h.remove(0)

        pert = [np.array(h)*vars_pert[i] for i in range(len(vars_pert))]

        for ivar in range(len(vars)):
            grad_tmp = np.zeros_like(CD_COEFF[FD_ORDER-1])
            
            for ic in range(len(CD_COEFF[FD_ORDER - 1])):
                x_tmp = np.array(vars)
                x_tmp[ivar] += pert[ivar][ic]

                x_from_vars = nm.get_wt_coordinates(*x_tmp, n_elements=self.n_elements,
                                                    max_col=self.max_col)
                AEP = self.wf.get_AEP(x_from_vars)
                grad_tmp[ic] = AEP * CD_COEFF[FD_ORDER-1][ic]
            grad_vars[ivar] = np.sum(grad_tmp / vars_pert[ivar])

        return [grad_vars]

    def __gradient_lattice_multi(self, vars):
        assert 0 <= FD_ORDER - 1 <= len(CD_COEFF)

        # perturbation distances in the lattice coord. system
        vars_pert = [0.5/180.*np.pi, 0.5/180.*np.pi, 1., 1., 1., 1.]

        # create perturbed matrices
        grad_vars = np.zeros_like(vars)

        # perturbation matrix, assumes x_pert=y_pert
        h = range(-len(CD_COEFF[FD_ORDER-1])/2, len(CD_COEFF[FD_ORDER-1])/2 + 1)
        h.remove(0)

        pert = [np.array(h)*vars_pert[i] for i in range(len(vars_pert))]

        # prepare the perturbed x vectors
        x_array = np.zeros((len(h) * len(vars), self.n_elements*2), dtype=np.float)
        #aep_array = np.zeros((len(h) * len(x),), dtype=np.float)

        for ivar in range(len(vars)):
            grad_tmp = np.zeros_like(CD_COEFF[FD_ORDER-1])
            for ic in range(len(CD_COEFF[FD_ORDER - 1])):

                idx = ivar*len(h) + ic

                x_tmp = np.array(vars)
                x_tmp[ivar] += pert[ivar][ic]
                x_from_vars = nm.get_wt_coordinates(*x_tmp, n_elements=self.n_elements,
                                                    max_col=self.max_col)
                x_array[idx, :] = x_from_vars

        aep_array = self.wf.get_gradAEP(x_array, gradient_source='FD', coordinate_system='lattice')

        # do the central difference calculations
        grad_vars = np.zeros_like(vars)
        for ivar in range(len(vars)):
            grad_tmp = np.zeros_like(CD_COEFF[FD_ORDER-1])
            for ic in range(len(CD_COEFF[FD_ORDER-1])):
                idx = ivar*len(h) + ic
                AEP = aep_array[idx]
                grad_tmp[ic] = AEP * CD_COEFF[FD_ORDER-1][ic]
            grad_vars[ivar] = np.sum(grad_tmp) / vars_pert[ivar]

        return [grad_vars]

    def jac_confun(self, vars, flag, *args, **kwargs):
        """
        Calculates the Jacobi matrix. It takes two arguments, 
        the first is the variable x and the second is a Boolean flag 
        if the flag is true, it supposed to return a tuple (row, col) 
                to indicate the sparse Jacobi matrixs structure. 
        if the flag is false if returns the values of the Jacobi matrix 
                with length nnz
        """
        logger.debug('Calling jacobian of constraints')
        #logger.debug('ipopt_jaccon: X = {}, flag = {}'.format(X, flag))
        if flag:
            # return sparse jacobian structure
            return self.jaccon_nz
        else:
            vars = self.remove_scaling(vars)
            
            jac = self.jaccon_fun(*vars)

            # apply scaling
            # TODO:
            # distinguish between different constraint types!!!!!!
            jac_scaled = np.array(jac) / self.scale_con * np.array(self.scale[1:])

            return jac_scaled[self.jaccon_nz[0], self.jaccon_nz[1]]


class WFLOP_2(BaseWFLOP):
    """
    Basic WFLOP problem.
    Turbines are positioned freely and their location vectors only have [x,y] componenets
    """ 
    #SCALING = namedtuple('Scaling', ['aep', 'x', 'y'])

    # scaling factors tuple
    scale = WFLOP2_SCALING
    
    id = 'optimizer'

    def __init__(self, queue_list):
        logger.info('Initiating WFLOP#1')
        self.q_container = queue_list

        self.n_elements = WFLOP1_NUMBER_ELEMENTS

        self.wf = WindFarm(self.q_container, NumberWT=self.n_elements)

        #x_tmp = np.array([0., 0.]*self.n_elements, dtype=np.float)
        #tmpaep = self.wf.get_AEP(x_tmp)
        #logger.info('AEP = {}'.format(tmpaep))
        #tmpgrad = self.wf.get_gradAEP(x_tmp)
        #logger.info('Calculating initial gradient at {}'.format(x_tmp))
        #logger.info('Gradient = <{}>'.format(tmpgrad))

        # wind farm boundary point coordinates
        self.boundary_poly = np.array([[0., 2000., 2000.,    0.],
                                       [0.,    0., 2000., 2000.]], dtype=np.float)
        self.boundary_poly[0,:] *= self.scale.x
        self.boundary_poly[1,:] *= self.scale.y

        self.vars = sb.WFLOP2_vars(self.n_elements)
        self.con_expr = sb.WFLOP2_constraints(self.vars, self.boundary_poly,
                                              scaling_factor=self.scale)
        #logger.info('Constraints:')
        #logger.info(sslf.con_expr)

        # function calculating constraint
        self.con_fun = sb.get_confun(self.vars, self.con_expr)
        # function calculating the jacobian of constraints
        jaccon_expr = sb.get_jaccon_expr(self.vars, self.con_expr)

        #logger.info('Jacobian of the constraints')
        #logger.info(jaccon_expr)

        self.jaccon_fun = sb.get_jaccon_fun(self.vars, jac_constraints=jaccon_expr)

        # list of non zero rows and cols of jacobian
        self.jaccon_nz = sb.get_jac_nz_pos(jaccon_expr)

        sp.pprint(self.con_expr)

    def initial_setup(self):
        logger.info('Setting up WFLOP#2')

        # SCALING FACTOR
        #self.sf = 1e6 

        # Variable order:
        # ==============
        # x[0] - alpha  - rotation angle of the WF
        # x[1] - beta   - shear angle fo the WF
        # x[2] - b_x    - distance between WT in a row
        # x[3] - b_y    - distance between rows
        # x[4] - d_x    - horizontal translation of the WF
        # x[5] - d_y    - vertical translation of the WF

        # constraint bounds
        #b_l, b_u = np.array(sb.get_boundary_bounds(self.boundary_poly), dtype=np.float)
        #b_l = np.array([0.]*len(self.con_expr))
        #b_u = np.array([2000.]*len(self.con_expr))

        b_u, b_l, prox_l, prox_u = [], [], [], [] 

        if WFLOP1_CONS_BOUNDARY:
            b_u = np.zeros((self.boundary_poly.shape[1]*self.n_elements,), dtype=np.float)
            b_l = np.ones(b_u.shape)*-1e19
        if WFLOP1_CONS_PROXIMITY:
            n_prox_cons = nm.binomial_coefficient(self.n_elements, 2)
            prox_l = np.array([100.]*n_prox_cons, dtype=np.float)
            prox_u = np.array([1e19]*n_prox_cons, dtype=np.float)

        # number of variables
        n = self.n_elements*2

        # xl is the lower bound of x as bounded constraints 
        xl = np.array([0.]*n, dtype=np.float)
        xl /= self.scale.x
        #xl = np.array([xl[i]*self.scale[i+1] for i in range(len(xl))])

        # xu is the upper bound of x as bounded constraints 
        xu = np.array([2000.]*n, dtype=np.float)
        xu /= self.scale.x
        #xu = np.array([xu[i]*self.scale[i+1] for i in range(len(xu))])

        # m is the number of constraints, 
        m = len(self.con_expr)
        # gl is the lower bound of constraints 
        gl = np.concatenate((b_l, prox_l))
        # gu is the upper bound of constraints 
        #         both gl, gu should be one dimension arrays with length m 
        gu = np.concatenate((b_u, prox_u))
        # nnzj is the number of nonzeros in Jacobi matrix 
        nnzj = len(self.jaccon_nz[0])
        # nnzh is the number of non-zeros in Hessian matrix, you can set it to 0 
        nnzh = 0
        # initial coordinates
        #x0 = np.array([np.pi, 0., 400, 400, 1000., 1000.], dtype=np.float)
        #x0 = np.array([300.*ii for ii in range(n)] + [300.*ii for ii in range(n)])
        #x0 = np.array([ 5.938608229098191744e-07, -9.931560605939237618e-09, 3.752299729911342256e-07, 6.624716753246052576e+02,
        #                9.968163603509324275e+02, 1.006786114927396397e+03, 1.094244413094384567e-06, 1.999999999941594751e+03,
        #                1.348781735510965063e+03, 1.999999999946925982e+03, 1.999999999961405592e+03, 1.999999999839292514e+03,
        #                1.432694502994081492e-07, 1.314657060534122593e+03, 6.206455172339799446e+02, 1.999999999849630740e+03,
        #                8.076872416856569998e+02, 2.662917589455431524e-07, 1.999999999925934844e+03, 6.214619392362272947e+02,
        #                1.999999999946938942e+03, 1.298438851716898398e+03, 1.619881731260815688e-07, 1.999999999932231958e+03],
        #                dtype=np.float)
        x0 = np.random.random((2*self.n_elements,))*2000.
        x0[:self.n_elements] /= self.scale.x
        x0[self.n_elements:] /= self.scale.y

        #x0 = np.array([100., 1900., 1800., 200., 100.,  200., 1800., 1700.], dtype=np.float)
        #x0[:self.n_elements] /= self.scale.x
        #x0[self.n_elements:] /= self.scale.y

        # this results in the same position for all of the wind turbines
        #x0 = (xu + xl)/2.

        logger.info('Number of variables = {}'.format(n))
        logger.info('Number of constraints = {}'.format(m))
        logger.info('Number of nonzero elements in jaccon = {}'.format(nnzj))
        logger.info('Variable Constraints:')
        logger.info('Lower: <{}>'.format(xl))
        logger.info('Upper: <{}>'.format(xu))
        logger.info('Constraints bounds:')
        logger.info('Lower: <{}>'.format(gl))
        logger.info('Upper: <{}>'.format(gu))
        logger.info('Initial position'.format())
        logger.info('x0 = {}'.format(x0))

        return n, xl, xu, m, gl, gu, nnzj, nnzh, x0

    def objfun(self, vars, *args, **kwargs):
        #logger.debug('Calling objective function')

        # remove scaling
        X = np.zeros_like(vars)
        X[:] = vars
        X[:self.n_elements] *= self.scale.x
        X[self.n_elements:] *= self.scale.y

        #logger.info('Calling objective function at vars = <{}>'.format(vars))
        #logger.info('Calling objective function at x = <{}>'.format(X))
        AEP = self.wf.get_AEP(X) / self.scale.aep
        #logger.info('AEP = {}'.format(AEP))

        for qi, qk in self.q_container.items():
            logger.debug('Q[{}] queue is {}'.format(qi, qk.empty()))

        self.q_container['plotter'].put({'objective': [X, vars, AEP]})

        return AEP

    def confun(self, vars, *args, **kwargs):
        logger.debug('Calling constraints')
        
        # remove scaling
        X = np.zeros_like(vars)
        X[:] = vars
        X[:self.n_elements] *= self.scale.x
        X[self.n_elements:] *= self.scale.y

        cons = self.con_fun(*X) 

        # send data for plotting
        self.q_container['plotter'].put({'constraints': cons})

        return cons

    def grad_objfun(self, vars, *args, **kwargs):
        #logger.debug('Calling gradient of the objective function')

        # remove scaling
        X = np.zeros_like(vars)
        X[:] = vars
        X[:self.n_elements] *= self.scale.x
        X[self.n_elements:] *= self.scale.y

        #logger.info('Calling gradient function at vars = <{}>'.format(vars))
        #logger.info('Calling gradient function at x = <{}>'.format(X))
        grad_aep = self.wf.get_gradAEP(X) / self.scale.aep
        logger.debug('GradObj = <{}>'.format(grad_aep.T))

        return grad_aep

    def jac_confun(self, vars, flag, *args, **kwargs):
        logger.debug('Calling jacobian of constraints')
        #logger.debug('ipopt_jaccon: X = {}, flag = {}'.format(X, flag))
        if flag:
            # return sparse jacobian structure
            return self.jaccon_nz
        else:

            # remove scaling
            X = np.zeros_like(vars)
            X[:] = vars
            X[:self.n_elements] *= self.scale.x
            X[self.n_elements:] *= self.scale.y

            # return jacobian
            jac = self.jaccon_fun(*X)

            #logger.info(X)
            #logger.info(jac)

            return jac[self.jaccon_nz[0], self.jaccon_nz[1]]

class WFLOP_3(BaseWFLOP):

    scale = WFLOP3_SCALING  # scaling factors tuple
    #scale_con = WFLOP3_SCALING_CON
    id = 'optimizer'
     

    def __init__(self, queue_list):
        logger.info('Initiating WFLOP#3')
        self.q_container = queue_list

        self.wf = WindFarm(self.q_container, NumberWT=WFLOP1_NUMBER_ELEMENTS)

        # wind farm boundary point coordinates
        self.boundary_poly = np.array([[0., 2000., 2000.,    0.],
                                       [0.,    0., 2000., 2000.]], dtype=np.float)

        # initial shape of the lattice
        #x_lattice = nm.adjust_vector_shape(nm.get_lattice_mapping(
        #    WFLOP1_NUMBER_ELEMENTS, WFLOP1_MAX_ELEMENTS_PER_ROW))
        self.n_elements = WFLOP1_NUMBER_ELEMENTS
        self.max_col = WFLOP1_MAX_ELEMENTS_PER_ROW

        # symbolic stuff
        self.vars = sb.WFLOP1_vars()
        logger.info('Deriving constraints for WFLOP3')
        self.con_expr = sb.WFLOP1_constraints(self.vars, self.boundary_poly)

        # function calculating constraint
        self.con_fun = sb.get_confun(self.vars, self.con_expr)

        # function calculating the jacobian of constraints
        logger.info('Deriving jacobian of constraints for WFLOP3')
        jaccon_expr = sb.get_jaccon_expr(self.vars, self.con_expr, simplify_expr=False)

        self.jaccon_fun = sb.get_jaccon_fun(self.vars, jac_constraints=jaccon_expr)

        # function returning dx/dvars used in gradient of the obj function
        dxdvars_expr = sb.WFLOP1_dxdvars(self.vars, self.n_elements, self.max_col)
        self.dxdvars_fun = sb.get_dxdvars_fun(self.vars, dxdvars_expr=dxdvars_expr)

        # prepare the new jacobian to get nz positions
        n_cons_lattice = len(self.con_expr)
        self.n_cons = self.n_elements + n_cons_lattice

        #sp.pprint(dxdvars_expr)

        # this can get quite big with a lot of constraitns
        #dxdvars_tmp = np.array(self.dxdvars_fun(*[1., 1., 1., 1., 1., 1.]))

        jac_structure = sp.zeros((self.n_cons, 7))
        jac_structure[:self.n_elements, 0] = - sp.ones((self.n_elements, 1))
        jac_structure[self.n_elements:, 0] = sp.zeros((n_cons_lattice, 1))
        jac_structure[:self.n_elements, 1:] = sp.ones((self.n_elements, 6))
        #jac_structure[:self.n_elements, 1:] = np.array(
        #        sp.ones((self.n_elements, 2*self.n_elements))).dot(dxdvars_tmp)
        jac_structure[self.n_elements:, 1:] = jaccon_expr

        # list of non zero rows and cols of jacobian
        self.jaccon_nz = sb.get_jac_nz_pos(jac_structure)  # only for lattice vars


    def initial_setup(self):
        logger.info('Setting up WFLOP#1')

        # Variable order:
        # ==============
        # x[0] - alpha  - rotation angle of the WF
        # x[1] - beta   - shear angle fo the WF
        # x[2] - b_x    - distance between WT in a row
        # x[3] - b_y    - distance between rows
        # x[4] - d_x    - horizontal translation of the WF
        # x[5] - d_y    - vertical translation of the WF

        # constraint bounds
        # -----------------
        tau_lower = np.zeros((self.n_elements,), dtype=np.float)
        tau_upper = np.ones((self.n_elements,), dtype=np.float)*1e19

        b_u, b_l, area_l, area_u, prox_l, prox_u = [], [], [], [], [], [] 
        # TODO
        # add scaling!
        if WFLOP1_CONS_BOUNDARY:
            # linalg
            b_u = np.zeros((4*WFLOP1_NUMBER_ELEMENTS,), dtype=np.float)
            #b_u = np.zeros((4*self.n_elements,), dtype=np.float)
            b_l = np.ones(b_u.shape)*-1e19

            # winding number
            #b_l = np.ones((self.n_elements,), dtype=np.float)*np.pi
            #b_u = np.ones((self.n_elements,), dtype=np.float)*1e20

            #n_boundary_cons = (self.boundary_poly[0,:]) * self.n_elements
        
        # TODO
        # add scaling!
        if WFLOP1_CONS_AREA:
            area_l = np.array([-2000.*2000.], dtype=np.float)
            #area_l *= self.scale.d_x*self.scale.d_y
            
            area_u = np.array([2000.*2000.], dtype=np.float)
            #area_u *= self.scale.d_x*self.scale.d_y
            #n_area_cons = 1

        # TODO
        # add scaling!
        if WFLOP1_CONS_PROXIMITY:
            #prox_l = np.array([50], dtype=np.float)
            #prox_u = np.array([1000], dtype=np.float)
            #prox_l = np.array([200., 200.], dtype=np.float)
            #prox_u = np.array([1e19, 1e19], dtype=np.float)
            
            # number of proximity constraints
            n_prox_cons = nm.binomial_coefficient(self.n_elements, 2)

            prox_l = np.array([400.]*n_prox_cons, dtype=np.float)
            prox_u = np.array([1e19]*n_prox_cons, dtype=np.float)

        
        x_min = min(self.boundary_poly[0])
        x_max = max(self.boundary_poly[0])
        y_min = min(self.boundary_poly[1])
        y_max = max(self.boundary_poly[1])

        # number of variables
        n = len(self.vars) + 1

        # xl is the lower bound of x as bounded constraints 
        xl = np.array([-1e19, -1e19, 0., 200., 200., x_min, y_min], dtype=np.float)
        #xl = np.array([xl[i]/self.scale[i+1] for i in range(len(xl)) if i > 0])
        xl[1:] /= self.scale[2:]

        # xu is the upper bound of x as bounded constraints 
        xu = np.array([0., 1e19, np.pi, 2000., 2000., x_max, y_max], dtype=np.float)
        #xu = np.array([xu[i]/self.scale[i+1] for i in range(len(xu)) if i > 0])
        xu[1:] /= self.scale[2:]

        # m is the number of constraints, 
        m = self.n_cons

        # gl is the lower bound of constraints 
        gl = np.concatenate((tau_lower, b_l, area_l, prox_l))

        # gu is the upper bound of constraints both gl,
        # gu should be one dimension arrays with length m 
        gu = np.concatenate((tau_upper, b_u, area_u, prox_u))
        # nnzj is the number of nonzeros in Jacobi matrix 

        print gl.shape
        print gu.shape

        nnzj = len(self.jaccon_nz[0])
        # nnzh is the number of non-zeros in Hessian matrix, you can set it to 0 
        nnzh = 0
        # initial coordinates
        x0 = np.array([-8, 20.*np.pi/180, 0.*np.pi/180, 200, 200, 500, 500], dtype=np.float)
        #x0 = np.array([x0[ii]/self.scale[ii+1] for ii in range(len(x0))])
        x0[1:] /= self.scale[2:]
        
        logger.info('Number of variables = {}'.format(n))
        logger.info('Number of constraints = {}'.format(m))
        logger.info('Number of nonzero elements in jaccon = {}'.format(nnzj))
        logger.info('Variable Constraints:')
        logger.info('Lower: <{}>'.format(xl))
        logger.info('Upper: <{}>'.format(xu))
        logger.info('Constraints bounds:')
        logger.info('Lower: <{}>'.format(gl))
        logger.info('Upper: <{}>'.format(gu))
        logger.info('Initial position'.format())
        logger.info('x0 = {}'.format(x0))

        return n, xl, xu, m, gl, gu, nnzj, nnzh, x0

    def remove_scaling(self, vars):
        return [vars[ii]*self.scale[ii+2] for ii in range(len(vars))]

    def objfun(self, vars, *args, **kwargs):
        logger.debug('Calling objective function')

        vars_lattice = vars[1:]
        vars_scaled = self.remove_scaling(vars_lattice)

        X = nm.get_wt_coordinates(*vars_scaled, n_elements=self.n_elements,
                                  max_col=self.max_col)

        self.q_container['plotter'].put({'objective': [X, vars_scaled, vars[0]]})
        
        # TODO dont scale tau!!
        #obj_fun = vars[0] / self.scale.tau
        obj_fun = vars[0]

        # obj function is just tau
        return obj_fun

    def confun(self, vars, *args, **kwargs):
        logger.debug('Calling constraints')

        logger.debug('confun BEFORE vars=<{}>'.format(vars))

        # AEP_i(x) >= tau ==> AEP_i(x) - tau >= 0

        vars_lattice = vars[1:]
        vars_scaled = self.remove_scaling(vars_lattice)

        X = nm.get_wt_coordinates(*vars_scaled, n_elements=self.n_elements,
                                  max_col=self.max_col)
        
        aep_i = self.wf.get_AEP_for_turbine(X) + vars[0]

        cons = self.con_fun(*vars_scaled)

        logger.debug('confun AFTER vars=<{}>'.format(vars_scaled))
        
        # apply scaling
        #cons_scaled = cons / self.scale_con

        cons = np.vstack((aep_i.reshape((-1,1)), cons))

        # send data for plotting
        self.q_container['plotter'].put({'constraints': cons})

        return cons

    def grad_objfun(self, vars, *args, **kwargs):
        logger.debug('Calling gradient of the objective function')
        logger.debug('gradObj: x = <{}>'.format(vars))

        grad_obj = np.array([1. , 0.,  0., 0., 0., 0., 0.], dtype=np.float)

        return grad_obj

    def jac_confun(self, vars, flag, *args, **kwargs):
        """
        Calculates the Jacobi matrix. It takes two arguments, 
        the first is the variable x and the second is a Boolean flag 
        if the flag is true, it supposed to return a tuple (row, col) 
                to indicate the sparse Jacobi matrixs structure. 
        if the flag is false if returns the values of the Jacobi matrix 
                with length nnz
        """
        logger.debug('Calling jacobian of constraints')
        #logger.debug('ipopt_jaccon: X = {}, flag = {}'.format(X, flag))
        if flag:
            # return sparse jacobian structure
            return self.jaccon_nz
        else:
            
            vars_lattice = self.remove_scaling(vars[1:])
            
            # lattice constraints
            jac_lattice = self.jaccon_fun(*vars_lattice)

            # apply scaling
            jac_lattice = np.array(jac_lattice) * np.array(self.scale[2:])

            X = nm.get_wt_coordinates(*vars_lattice, n_elements=self.n_elements,
                                      max_col=self.max_col)
            grad_aep = np.array(self.wf.get_gradAEP_turbine(X))
            dxdvars = self.dxdvars_fun(*vars_lattice)

            grad_aep_lattice = np.array(grad_aep.dot(dxdvars))

            # scaling
            for i_row in range(self.n_elements):
                for j_col in range(6):
                    grad_aep_lattice[i_row, j_col] /= (self.scale[0]/self.scale[j_col+2])

            jac = np.zeros((self.n_cons, 7))
            jac[:self.n_elements, 0] = np.ones((self.n_elements,))
            jac[:self.n_elements, 1:] = grad_aep_lattice
            jac[self.n_elements:, 1:] = jac_lattice

            return jac[self.jaccon_nz[0], self.jaccon_nz[1]]

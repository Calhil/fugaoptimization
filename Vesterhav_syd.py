"""
Class for optimization of Vesterhav Syd wind farm.
The layout is based on WFLOP1 - lattice layout)
"""

import numpy as np

import Symbolic as sb
import NumericalMethods as nm

from config import (logger,
                    WFLOP1_MAX_ELEMENTS_PER_ROW,
                    WFLOP1_NUMBER_ELEMENTS,
                    WFLOP1_CONS_BOUNDARY,
                    WFLOP1_CONS_AREA,
                    WFLOP1_CONS_PROXIMITY)
from examples import WFLOP_1
from WFLOP import WindFarm

class VesterhavSyd(WFLOP_1):
    """
    This is the base WFLOP class.
    All problems should create a child class and inherit methods from it.
    
    Methods:
    -------
    get_initial_setup - returns the variales needed for particular optimizer
    objfun          - objective function
    confun          - constraint function
    grad_objfun     - gradient of the objective function
    jac_confun      - jacobian of the constraint function
    hess_objfun     - hessian of the objective function
    hess_confun     - hessian of the constraint function
    """
    
    id = 'optimizer'

    def __init__(self, queue_list, *args, **kwargs):
        logger.info('Initiating Vesterhav Syd')
        self.q_container = queue_list

        self.wf = WindFarm(self.q_container, NumberWT=WFLOP1_NUMBER_ELEMENTS)

        # wind farm boundary point coordinates
        self.boundary_poly = np.array([[0., 2000., 2000.,    0.],
                                       [0.,    0., 2000., 2000.]], dtype=np.float)

         
        self.excluded_poly = np.array([[-10., 2010., 2010.,  -10.],
                                       [-10.,  -10., 1000., 1000.]], dtype=np.float)
        # initial shape of the lattice
        #x_lattice = nm.adjust_vector_shape(nm.get_lattice_mapping(
        #    WFLOP1_NUMBER_ELEMENTS, WFLOP1_MAX_ELEMENTS_PER_ROW))
        self.n_elements = WFLOP1_NUMBER_ELEMENTS
        self.max_col = WFLOP1_MAX_ELEMENTS_PER_ROW

        # symbolic stuff
        self.vars = sb.WFLOP1_vars()
        logger.info('Deriving constraints for WFLOP1')
        self.con_expr = sb.WFLOP1_constraints(self.vars, self.boundary_poly)

        # Excluded part of the polygon
        self.excluded_boundary = sb.get_boundary_constraints_expr(self.vars,
                                        self.excluded_poly)

        self.con_expr = self.con_expr.row_insert(self.con_expr.rows, self.excluded_boundary)

        # function calculating constraint
        self.con_fun = sb.get_confun(self.vars, self.con_expr)

        import sympy as sp
        sp.pprint_use_unicode()
        sp.pprint(self.con_expr)

        # function calculating the jacobian of constraints
        logger.info('Deriving jacobian of constraints for WFLOP1')
        jaccon_expr = sb.get_jaccon_expr(self.vars, self.con_expr, simplify_expr=False)

        self.jaccon_fun = sb.get_jaccon_fun(self.vars, jac_constraints=jaccon_expr)

        # list of non zero rows and cols of jacobian
        self.jaccon_nz = sb.get_jac_nz_pos(jaccon_expr)

        # function returning dx/dvars used in gradient of the obj function
        dxdvars_expr = sb.WFLOP1_dxdvars(self.vars, self.n_elements, self.max_col)
        self.dxdvars_fun = sb.get_dxdvars_fun(self.vars, dxdvars_expr=dxdvars_expr)

    def initial_setup(self):
        logger.info('Setting up Vesterhav Syd')

        # Variable order:
        # ==============
        # x[0] - alpha  - rotation angle of the WF
        # x[1] - beta   - shear angle fo the WF
        # x[2] - b_x    - distance between WT in a row
        # x[3] - b_y    - distance between rows
        # x[4] - d_x    - horizontal translation of the WF
        # x[5] - d_y    - vertical translation of the WF
    
        # manual 1
        #x_tmp = np.array([0, 666, 1333, 2000, 0, 666, 1333, 2000, 0, 666, 1333, 2000,
        #                  0, 0, 0, 0, 1000, 1000, 1000, 1000, 2000, 2000, 2000, 2000], dtype=np.float)
        # manual 2
        #x_tmp = np.array([0, 0, 0, 0, 1000, 1000, 1000, 1000, 2000, 2000, 2000, 2000,
        #                  0, 666, 1333, 2000, 0, 666, 1333, 2000, 0, 666, 1333, 2000], dtype=np.float)

        #x_tmp = np.array([1000., 1000.], dtype=np.float)
        #aep_tmp = self.wf.get_AEP(x_tmp)
        #logger.info(aep_tmp)

        #x_tmp = np.array([1000, 0, 1000, 0], dtype=np.float)
        #n_x, n_y = 21, 21
        #x_space = np.linspace(700, 1300, n_x)
        #y_space = np.linspace(700, 1300, n_y)
        ##y_space = np.array([900, 1000, 1050], dtype=np.float)
        ##aep_array = np.zeros((n_x, n_y))
        #
        #grad_array = np.zeros((n_x*len(y_space), 2 + 2*WFLOP1_NUMBER_ELEMENTS))

        #for idx_x, x_i in enumerate(x_space):
        #    for idx_y, y_i in enumerate(y_space):
        #        logger.info('Doing {}.{}'.format(idx_x, idx_y))
        #        x_tmp[1] = x_i
        #        x_tmp[3] = y_i

        #        #aep = self.wf.get_AEP(x_tmp)
        #        #aep_array[idx_x, idx_y] = aep

        #        grad = self.wf.get_gradAEP(x_tmp)

        #        grad_array[idx_x * len(y_space) + idx_y, 0] = x_i
        #        grad_array[idx_x * len(y_space) + idx_y, 1] = y_i
        #        grad_array[idx_x * len(y_space) + idx_y, 2:] = grad.reshape((-1,))

        ##with open('/dev/shm/aep_array.csv', 'a') as f:
        ##    np.savetxt(f, aep_array)
        #with open('/dev/shm/grad_array.csv', 'a') as f:
        #    np.savetxt(f, grad_array)

        #import time
        #time.sleep(9999999)

        # constraint bounds
        # -----------------
        b_u, b_l, area_l, area_u, prox_l, prox_u = [], [], [], [], [], [] 
        exb_l, exb_u = [], []
        # TODO
        # add scaling!
        if WFLOP1_CONS_BOUNDARY:
            # linalg
            b_u = np.zeros((4*WFLOP1_NUMBER_ELEMENTS,), dtype=np.float)
            #b_u = np.zeros((4*self.n_elements,), dtype=np.float)
            b_l = np.ones(b_u.shape)*-1e19

            # winding number
            #b_l = np.ones((self.n_elements,), dtype=np.float)*np.pi
            #b_u = np.ones((self.n_elements,), dtype=np.float)*1e20
            #n_boundary_cons = (self.boundary_poly[0,:]) * self.n_elements
            exb_l = np.zeros((4*WFLOP1_NUMBER_ELEMENTS,), dtype=np.float)
            exb_u = np.ones(exb_l.shape)*1e19
        
        # TODO
        # add scaling!
        if WFLOP1_CONS_AREA:
            area_l = np.array([-2000.*2000.], dtype=np.float)
            #area_l *= self.scale.d_x*self.scale.d_y
            
            area_u = np.array([2000.*2000.], dtype=np.float)
            #area_u *= self.scale.d_x*self.scale.d_y
            #n_area_cons = 1

        # TODO
        # add scaling!
        if WFLOP1_CONS_PROXIMITY:
            #prox_l = np.array([50], dtype=np.float)
            #prox_u = np.array([1000], dtype=np.float)
            #prox_l = np.array([200., 200.], dtype=np.float)
            #prox_u = np.array([1e19, 1e19], dtype=np.float)
            
            # number of proximity constraints
            n_prox_cons = nm.binomial_coefficient(self.n_elements, 2)

            prox_l = np.array([200.]*n_prox_cons, dtype=np.float)
            prox_u = np.array([1e19]*n_prox_cons, dtype=np.float)

        
        x_min = min(self.boundary_poly[0])
        x_max = max(self.boundary_poly[0])
        y_min = min(self.boundary_poly[1])
        y_max = max(self.boundary_poly[1])
        # number of variables
        n = len(self.vars)

        # xl is the lower bound of x as bounded constraints 
        #xl = np.array([0., -(90-5)*np.pi/180., 50., 50., x_min, y_min], dtype=np.float)
        #xl = np.array([-1e19, -1e19, 200., 200., x_min, y_min], dtype=np.float)
        xl = np.array([-1e19, -np.pi*1./2., 200., 200., x_min, y_min], dtype=np.float)
        #xl = np.array([0, 0, 200., 200., x_min, y_min], dtype=np.float)
        xl = np.array([xl[i]/self.scale[i+1] for i in range(len(xl))])

        # xu is the upper bound of x as bounded constraints 
        #xu = np.array([2*np.pi, (90-5)*np.pi/180., 2000., 2000., x_max, y_max], dtype=np.float)
        #xu = np.array([2*np.pi, 0., 2000., 2000., x_max, y_max], dtype=np.float)
        #xu = np.array([1e19, 1e19, 2000., 2000., x_max, y_max], dtype=np.float)
        xu = np.array([1e19, np.pi/2., 2000., 2000., x_max, y_max], dtype=np.float)
        #xu = np.array([0, 0, 2000., 2000., x_max, y_max], dtype=np.float)
        xu = np.array([xu[i]/self.scale[i+1] for i in range(len(xu))])

        # m is the number of constraints, 
        m = len(self.con_expr)

        # gl is the lower bound of constraints 
        gl = np.concatenate((b_l, area_l, prox_l, exb_l))

        # gu is the upper bound of constraints both gl,
        # gu should be one dimension arrays with length m 
        gu = np.concatenate((b_u, area_u, prox_u, exb_u))
        # nnzj is the number of nonzeros in Jacobi matrix 

        nnzj = len(self.jaccon_nz[0])
        # nnzh is the number of non-zeros in Hessian matrix, you can set it to 0 
        nnzh = 0
        # initial coordinates
        #x0 = np.array([30.*np.pi/180, 20.*np.pi/180, 300, 300, 200., 200.], dtype=np.float)  # doesnt converge
        #x0 = np.array([8.6256102926054634,
        #               16.303640102141475,
        #               259.53309464812861,
        #               256.38950501375939,
        #               905.79936010809081,
        #               660.07172667560576], dtype=np.float)
        x0 = np.array([0.*np.pi/180, 0.*np.pi/180, 200, 200, 1100, 1100], dtype=np.float)
        #x0 = np.array([-20.*np.pi/180, -10.*np.pi/180, 200, 200, 1900, 1900], dtype=np.float)
        #x0 = np.array([30.*np.pi/180, 20.*np.pi/180, 500, 500, 100, 100], dtype=np.float)
        x0 = np.array([x0[ii]/self.scale[ii+1] for ii in range(len(x0))])
        #x0 = (xl+xu)/2.
        
        logger.info('Number of variables = {}'.format(n))
        logger.info('Number of constraints = {}'.format(m))
        logger.info('Number of nonzero elements in jaccon = {}'.format(nnzj))
        logger.info('Variable Constraints:')
        logger.info('Lower: <{}>'.format(xl))
        logger.info('Upper: <{}>'.format(xu))
        logger.info('Constraints bounds:')
        logger.info('Lower: <{}>'.format(gl))
        logger.info('Upper: <{}>'.format(gu))
        logger.info('Initial position'.format())
        logger.info('alpha = {}'.format(x0[0]))
        logger.info('beta = {}'.format(x0[1]))
        logger.info('b_x = {}'.format(x0[2]))
        logger.info('b_y = {}'.format(x0[3]))
        logger.info('d_x = {}'.format(x0[4]))
        logger.info('d_y = {}'.format(x0[5]))

        return n, xl, xu, m, gl, gu, nnzj, nnzh, x0


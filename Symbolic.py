"""
Functions and classes used in symbolic computations
"""
import sympy as sp
import numpy as np
from sympy.utilities.autowrap import ufuncify
from collections import namedtuple

from NumericalMethods import get_lattice_mapping, adjust_vector_shape
from config import WFLOP1_MAX_ELEMENTS_PER_ROW
from config import WFLOP1_NUMBER_ELEMENTS
from config import logger
from config import WFLOP1_CONS_BOUNDARY
from config import WFLOP1_CONS_AREA
from config import WFLOP1_CONS_PROXIMITY

Point = namedtuple('Point', ['x', 'y'])

# numpy error handling
np.seterr(over='raise')


def angle_between_vectors(v1, v2, simplify_expr=False):
    """
    Calculates signed angle between vectors v1 and v2
    angle = atan2 ( (v1 cross v2) / (v1 dot v2) )
    """
    v1_dot_v2 =   v1[0] * v2[0] + v1[1] * v2[1]
    v1_cross_v2 = v1[0] * v2[1] - v1[1] * v2[0]

    angle = sp.atan2(v1_cross_v2, v1_dot_v2)
    if simplify_expr:
        return sp.simplify(angle)
    else:
        return angle


def con_gen_numpy(vars, cons):
    """
    Generate a numpy function that returns a list of problem constraints
    """
    return sp.lambdify([vars], sp.Matrix(cons), 'numpy')


def dist_point_line(p, l1, l2):
    """
    Calculates distance from point p to a line determined by 2 points: l1 and l2
    Points have to be either a list or namedtuple
    p - point of interest
    l1 - point on the line
    l2 - point on the line
    """
    # works with namedtuples and lists
    # ((b.y - a.y)*p.x - (b.x - a.x)*p.y + b.x*a.y - b.y*a.x) / sp.sqrt((b.y - a.y)**2 + (b.x - a.x)**2)
    #return (l2[1] - l1[1])*p[0] - (l2[0] - l1[0])*p[1] + l2[0]*l1[1] - l2[1]*l1[0]
    return  ((l2[1] - l1[1])*p[0] - (l2[0] - l1[0])*p[1] + l2[0]*l1[1] - l2[1]*l1[0]) / sp.sqrt((l2[1] - l1[1])**2 + (l2[0] - l1[0])**2)


def dist_points(p1, p2, simplify_expr=False):
    """
    Returns an expression for the distance between 2 points
    """
    assert type(simplify_expr) == bool
    
    d = sp.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)

    if simplify_expr == True:
        return sp.simplify(d)
    else:
        return d


def dxdvars(vars, n_elements, max_col,
            data_repr='optimizer', simplify_expr=False):
    """
    Returns a sympy expression in the form of a vector of [dx_i/dvars_i]

    vars - list of WFLOP#1 variables
    n_elements - number of WT
    max_col - maximum number of elements per row
    data_repr - how the output data should be represented
            'optimizer':    y vector stored under x vector
            anything else:  [[dx0/dvars],[dy0/dvars],[dx1/dvars],[dy1/dvars], ...]
    simplify_expr - a flag determining whether to simplify the returned expression
                    True by default
    """
    #logger.info('Deriving dx/dvars')
    #logger.info(vars)

    [alpha, beta, bx, by, dx, dy] = vars

    Position = wf_coordinates(vars)

    # adjust the matrix shape to be compatible with ipopt
    if data_repr == 'optimizer':
        M_tmp = sp.zeros(1, 2*n_elements)
        for i_element in range(n_elements):
            M_tmp[0, i_element] = Position[0, i_element]
            M_tmp[0, i_element + n_elements] = Position[1, i_element]
        Position = M_tmp
    else:
        raise Exception('Unimplemented data_repr={}'.format(data_repr))

    # return gradient
    if simplify_expr:
        return sp.simplify(Position.jacobian(vars))
    else:
        return Position.jacobian(vars)


def get_area_from_vectors(v1, v2, simplify_expr=False):
    """
    Calculates area defined by two vectors
    Input:
    v1 - sp.Matrix
    v2 - sp.Matrix
    """
    assert type(v1) == sp.Matrix
    assert type(v2) == sp.Matrix
    assert type(simplify_expr) == bool

    # this doesnt work for 2d vectors
    # return v1.cross(v2)
    
    # this results in real/im parts in jacobian
    #return sp.functions.Abs(v1[0]*v2[1] - v1[1]*v2[0])

    #return sp.functions.Abs(sp.functions.re(v1[0]*v2[1] - v1[1]*v2[0]))
    #area = sp.Matrix([sp.sqrt( (v1[0]*v2[1] - v1[1]*v2[0])**2)])

    # Area without the norm, values get be negative
    # this should not result in a discontinuity
    area = sp.Matrix([v1[0]*v2[1] - v1[1]*v2[0]])

    if simplify_expr == True:
        return sp.simplify(area)
    else:
        return area


def get_boundary_bounds(ppoly):
    """
    Returns the lower and upper bounds for the boundary constraints
    given a list of polygon vertices:
    ppoly = [[x0, x1, ..., x_N], [y0, y1, ..., y_N]]
    """
    if not isinstance(ppoly, sp.Matrix):
        ppoly = sp.Matrix(ppoly)

    # determine the lower and upper constraint bounds
    # TODO: this can probably be moved to the loop below
    # TODO: check if all of the bounds are the same!!
    p_center = Point(np.sum(ppoly[0,:])/ppoly.cols, np.sum(ppoly[1,:]/ppoly.cols))
    c_l, c_u = [], []
    for col in range(ppoly.cols):
        # choose the line segment points
        a = Point(ppoly[0, col], ppoly[1, col])
        if col == ppoly.cols -1:
            b = Point(ppoly[0, 0], ppoly[1, 0])
        else:
            b = Point(ppoly[0, col+1], ppoly[1, col+1])
        sign = dist_point_line(p_center, a, b)
        if sign < 0:
            c_l.append(-2.0*pow(10.0, 19))
            c_u.append(0.)
        else:
            c_l.append(0.)
            c_u.append(2.0*pow(10.0, 19))

    return c_l*4, c_u*4


def boundary_bounds_linalg(polypts):
    # get A,B coeff matrices
    if not isinstance(polypts, sp.Matrix):
        polypts = sp.Matrix(polypts)

    B = []
    for i_line in range(polypts.cols):
        p1 = Point(polypts[0, i_line], polypts[1, i_line])

        if i_line != polypts.cols-1:
            p2 = Point(polypts[0, i_line+1], polypts[1, i_line+1])
        else:
            p2 = Point(polypts[0, 0], polypts[1, 0])

        a, b, c = line_coeffs_from_pts(p1, p2)
        B.append(c)

    return B


def get_boundary_constraints_expr(vars, ppoly, n_elements=WFLOP1_NUMBER_ELEMENTS,
                                  max_col=WFLOP1_MAX_ELEMENTS_PER_ROW,
                                  problem_type='WFLOP1', simplify_expr=False):
    """
    Calculates the boundary constraints given a list of sympy variables
    and polygon vertices of the boundary
    """
    assert type(simplify_expr) == bool

    if not isinstance(ppoly, sp.Matrix):
        ppoly = sp.Matrix(ppoly)

    # 1) Get the WF points and boundary vertices
    if problem_type == 'WFLOP1':
        #a, b = (divmod(n_elements, max_col)[0]-1), max_col-1
        a, b = divmod(n_elements, max_col)[0], max_col-1
        # TODO check indexes here
        #wfpoints = sp.Matrix([[0, 0, b, b],
        #                      [0, a, 0, a],
        #                      [1, 1, 1, 1]])
        #wfpoints = sp.Matrix([[0, 0, a, a],
        #                    [0, b, 0, b],
        #                    [1, 1, 1, 1]])
        #wfpoints[0, :] *= vars[2]
        #wfpoints[1, :] *= vars[3]

        #T_rot = rotation_matrix(vars[0])
        #T_shear = shear_matrix(vars[1])
        #T_trans = translation_matrix(vars[4], vars[5])
        #wfpoints = T_trans*T_rot*T_shear*wfpoints
        # delete the last unit row
        #wfpoints.row_del(2)

        wfpoints = wf_coordinates(vars)

    elif problem_type == 'WFLOP2':
        n_elements = len(vars) / 2
        wfpoints = sp.Matrix(vars).reshape(2, n_elements)
    else:
        logger.error()
        raise AttributeError('Invalid problem_type = {}'.format(problem_type))

    # determine distance expressions
    boundcon_expr = []
    for pcol in range(wfpoints.cols):
        p = Point(wfpoints[0, pcol], wfpoints[1, pcol])

        for lcol in range(ppoly.cols):
            # choose the line segment points
            a = Point(ppoly[0, lcol], ppoly[1, lcol])
            if lcol == ppoly.cols-1:
                b = Point(ppoly[0, 0], ppoly[1, 0])
            else:
                b = Point(ppoly[0, lcol+1], ppoly[1, lcol+1])

            d = dist_point_line(p, a, b)
            boundcon_expr.append(d)

    if simplify_expr == True:
        boundcon_expr = sp.simplify(boundcon_expr)

    return sp.Matrix(boundcon_expr)


def get_boundary_constraints_linalg_expr(vars, polypts, simplify_expr=False):
    """
    Returns an expressions for boundary constraints 
    Uses linear algebra equation [A B] [x y]T <= C

    """
    if not isinstance(polypts, sp.Matrix):
        polypts = sp.Matrix(polypts)

    # get A,B coeff matrices
    A = sp.zeros(polypts.cols, 2)
    B = sp.zeros(polypts.cols, 1)
    for i_line in range(polypts.cols):
        p1 = Point(polypts[0, i_line], polypts[1, i_line])

        if i_line != polypts.cols-1:
            p2 = Point(polypts[0, i_line+1], polypts[1, i_line+1])
        else:
            p2 = Point(polypts[0, 0], polypts[1, 0])

        a, b, c = line_coeffs_from_pts(p1, p2)
        A[i_line, 0] = a
        A[i_line, 1] = b
        B[i_line] = c

    # get vertex points or just all of the wf locations
    #vertices = get_wf_vertices(vars)
    vertices = wf_coordinates(vars)

    bcons = []
    for i_point in range(vertices.cols):
        # main constraint expression
        b = A*vertices[:, i_point] - B
        bcons += b.tolist()

    if simplify_expr:
        return sp.simplify(sp.Matrix(bcons))
    else:
        return sp.Matrix(bcons)


def get_boundary_constraints_windnum_expr(vars, polypts, simplify_expr=False):
    """
    Derives an expression for boundary constraints using winding number
    If a point is inside the polygon the winding number is equal to 2pi
    If its outside the polygon its 0.
    """

    if not isinstance(polypts, sp.Matrix):
        polypts = sp.Matrix(polypts)

    # get vertices or wt coordinates

    vertices = get_wf_vertices(vars)
    #vertices = wf_coordinates(vars)

    bcons = []
    for i_vert in range(vertices.cols):
        alpha = 0.
        p1 = Point(vertices[0, i_vert], vertices[1, i_vert])
        for i_line in range(polypts.cols):
            p2 = Point(polypts[0, i_line], polypts[1, i_line])
            if i_line < polypts.cols-1:
                p3 = Point(polypts[0, i_line+1], polypts[1, i_line+1])
            else:
                p3 = Point(polypts[0, 0], polypts[1, 0])

            v1 = get_vector(p1, p2)
            v2 = get_vector(p1, p3)
            
            alpha += angle_between_vectors(v1, v2)

        #sp.pprint(alpha)
        bcons.append(alpha)

    bcons = sp.Matrix(bcons)
    if simplify_expr:
        logger.warning('This expression is very complex and might take '
                       'a lot of time to simplify')
        return sp.simplify(bcons)
    else:
        return bcons


def get_area_fun(vars, area_expr):
    """
    Returns a function for calculating area
    """
    return sp.lambdify(vars, area_expr, 'numpy')


def get_confun(vars, cons):
    """
    Returns function calculating constraints
    """
    return con_gen_numpy(vars, cons)


def get_dxdvars_fun(vars, n_elements=WFLOP1_MAX_ELEMENTS_PER_ROW,
                    max_col=WFLOP1_MAX_ELEMENTS_PER_ROW, dxdvars_expr=None,
                    *args, **kwargs):
    """
    Returns a function calculating dx/dvars 
    """
    if dxdvars_expr is None:
        dxdvars_expr = WFLOP1_dxdvars(vars, n_elements, max_col, *args, **kwargs)

    return sp.lambdify((vars), dxdvars_expr, 'numpy')


def expr_gen_numpy(vars, expr):
    """
    Returns a numpy function for expression expr and input variables vars
    """
    return sp.lambdify(vars, expr, 'numpy')


def get_jaccon_expr(vars, cons, simplify_expr=False):
    """
    Returns a sympy expression for jacobian of constraints
    """
    assert isinstance(cons, sp.Matrix)
    assert type(simplify_expr) == bool

    #logger.info('Deriving jacobian of the constraints')
    
    if simplify_expr:
        return sp.simplify(cons.jacobian(vars))
    else:
        return cons.jacobian(vars)


def get_jaccon_fun(vars, constraints=None, jac_constraints=None, simplify_expr=False):
    """
    Returns a function for calculating the constraints
    """
    if jac_constraints is None:
        if constraints is None:
            raise AssertionError('You need to specify either the constraints'
                                 'or the jacobian of constraints')
        jac_constraints = get_jaccon_expr(vars, constraints)
    
    return jac_gen_numpy(vars, jac_constraints)


def get_jac_nz_pos(jac_cons_expr):
    """
    Determines the nonzero positions in Jacobian matrix
    and returns a list of positions in the form:
    [[row0, col0], [row1, col1], ...]
    """
    #vars = WFLOP1_vars()
    #cons = WFLOP1_constraints(vars)
    #J = sp.Matrix(cons).jacobian(vars)
    J = jac_cons_expr

    l_rows, l_cols = [], []
    for row in range(J.rows):
        for col in range(J.cols):
            if J[row, col] != 0:
                l_rows.append(row)
                l_cols.append(col)
    return (np.array(l_rows), np.array(l_cols))
    #return (np.array(l_cols), np.array(l_rows))


def get_jac_proximity(vars, proximity_expr, simplify_expr=False):
    assert type(simplify_expr) == bool

    logger.info('Deriving jacobian of the proximity constraints')

    jac_prox = proximity_expr.jacobian(vars)
    if simplify_expr == True:
        return sp.simplify(jac_prox)
    else:
        return jac_prox


def get_proximity_expr_wflop1(vars, n_elements, max_col,
                              simplify_expr=False, cons_type='full'):
    """
    Returns an expression for proximity constraints in the WFLOP#1
    """
    assert type(simplify_expr) == bool

    logger.info('Deriving proximity constraints')
    
    cons = []
    if cons_type == 'simplified':
        # only consider 2 cases, since were dealing with a lattice

        # create vertices of a 4 node wind farm
        vertices = get_wf_vertices(vars, n_elements=4, max_col=2)
        # derive the distance equation
        d1 = dist_points(vertices[:, 0], vertices[:, -1], simplify_expr=simplify_expr)
        d2 = dist_points(vertices[:, 1], vertices[:, 2], simplify_expr=simplify_expr)

        # vertical distance between rows, adjusted by shear angle
        #h = vars[2] * max_col * sp.tan(sp.pi/2. - vars[1])
        #return sp.Matrix([h])
        cons = [d1, d2]
    elif cons_type == 'full':
        # consider all the possible permutations
        coords = wf_coordinates(vars)
        n_elements = len(coords) / 2
        for i in range(n_elements):
            for j in range(n_elements):
                if i < j:
                    d = dist_points([coords[0, i], coords[1, i]],
                                    [coords[0, j], coords[1, j]])
                    cons.append(d)
    else:
        raise

    return sp.Matrix(cons)


def get_proximity_expr_wflop2(vars):
    
    n_elements = len(vars) / 2
    coords = sp.Matrix(vars).reshape(2, n_elements)

    cons = []
    # consider all the possible permutations
    for i in range(n_elements):
        for j in range(n_elements):
            if i < j:
                d = dist_points([coords[0, i], coords[1, i]],
                                [coords[0, j], coords[1, j]])
                cons.append(d)
    return sp.Matrix(cons)


def get_proximity_expr_wflop2_bad(vars, simplify_expr=False):

    n_elements = len(vars)/2

    cons = []
    for i_element in range(n_elements-1):
        p1 = Point(vars[i_element], vars[n_elements + i_element])

        for j_element in range(i_element+1, n_elements):
            p2 = Point(vars[j_element], vars[n_elements + j_element])
            cons.append(dist_points(p1, p2))

    if simplify_expr:
        return sp.simplify(cons)
    else:
        return sp.Matrix(cons)


def get_vector(p_start, p_end):
    """
    Returns a column vector given a starting and an ending point
    Input:
    p_start - [x, y] or [[x], [y]]
    p_end   - [x, y] or [[x], [y]]
    """
    return sp.Matrix([[p_end[0] - p_start[0]],
                      [p_end[1] - p_start[1]]])


def wf_coordinates(vars, n_elements=WFLOP1_NUMBER_ELEMENTS,
                   max_col=WFLOP1_MAX_ELEMENTS_PER_ROW,
                   simplify_expr=False):
    
    mapping = adjust_vector_shape(get_lattice_mapping(n_elements, max_col))
    mapping = sp.Matrix(mapping)

    # row/col dist
    mapping[0,:] *= vars[2]  # bx
    mapping[1,:] *= vars[3]  # by

    T_rot = rotation_matrix(vars[0])
    T_shear = shear_matrix(vars[1])
    T_trans = translation_matrix(vars[4], vars[5])
    vertices = T_trans*T_rot*T_shear*mapping
    vertices.row_del(2) # delete the last unit row

    if simplify_expr == True:
        return sp.simplify(vertices)
    else:
        return vertices


def get_wf_vertices(vars, n_elements=WFLOP1_NUMBER_ELEMENTS,
                    max_col=WFLOP1_MAX_ELEMENTS_PER_ROW,
                    simplify_expr=False):
    """
    Returns 4 lattice vertices of a wind farm defined by vars, n_elements and max_col
    vars - alpha, beta, bx, by, dx, dy
    n_elements - number of wind turbines
    max_col - maximum number of turbines per lattice row
    """
    assert type(simplify_expr) == bool

    a, b = (divmod(n_elements, max_col)[0]-1), max_col-1

    vertices = sp.Matrix([[0, 0, b, b],
                          [0, a, 0, a],
                          [1, 1, 1, 1]])
    vertices[0, :] *= vars[2]
    vertices[1, :] *= vars[3]

    T_rot = rotation_matrix(vars[0])
    T_shear = shear_matrix(vars[1])
    T_trans = translation_matrix(vars[4], vars[5])
    vertices = T_trans*T_rot*T_shear*vertices
    vertices.row_del(2) # delete the last unit row

    if simplify_expr == True:
        return sp.simplify(vertices)
    else:
        return vertices
    

def get_jacarea_expr(vars, area_expr=None, simplify_expr=False):
    """
    Returns expression for jacobian of area
    """
    assert type(simplify_expr) == bool
    
    if area_expr is None:
        vertices = get_wf_vertices(vars)
        v1 = get_vector(vertices[:,0], vertices[:,1])
        v2 = get_vector(vertices[:,0], vertices[:,2])
        area_expr = get_area_from_vectors(v1, v2)

    jac_area = area_expr.jacobian(vars)

    if simplify_expr:
        return sp.simplify(jac_area)
    else:
        return jac_area


def get_jacarea_fun(vars, jacarea_expr=None, simplify_expr=False):
    """
    Returns function calculating jacobian of the area
    """
    assert type(simplify_expr) == True
    
    if jacarea_expr is None:
        jacarea_expr = get_jacarea_expr(vars, simplify_expr=simplify_expr)

    return sp.lambdify(vars, jacarea_expr, 'numpy')


def jaccon_gen_fortran(vars, confun):
    return ufuncify(vars, sp.Matrix(confun).jacobian(vars))


def jac_gen_numpy(vars, jaccon_expr):
    """
    Generates a numpy function for calculating the jacobian of constraints
    vars - list of variables with respect to which the Jacobian should be calculated
    confun - list  of constrained functions
    """
    return sp.lambdify(vars, jaccon_expr, 'numpy')


def jaccon_gen_sympy(vars, confun):
    return lambda vars_0: sp.Matrix(confun).jacobian(vars).subs(dict(zip(vars, vars_0)))


def line_coeffs_from_pts(p1, p2):
    """
    Returns coefficients of the line equation in matrix form given 2 points.
    Ax + By = C
    
    Input:
    -----
    p1, p2 = point coordinates
    """
    assert len(p1) == 2
    assert len(p2) == 2
    
    A = p2[1] - p1[1]
    B = p1[0] - p2[0]
    C = p1[0]*p2[1] - p2[0]*p1[1]
    
    return A, B, C


def rotation_matrix(alpha):
    return sp.Matrix([[sp.cos(alpha), -sp.sin(alpha), 0],
                      [sp.sin(alpha),  sp.cos(alpha), 0],
                      [0,              0,             1]])


def shear_matrix(beta):
    return sp.Matrix([[1,    sp.sin(sp.pi/2 - beta),   0],
                      [0,    sp.cos(sp.pi/2 - beta),   0],
                      [0,    0,              1]])


def translation_matrix(dx, dy):
    return sp.Matrix([[1,    0,  dx],
                      [0,    1,  dy],
                      [0,    0,  1]])


def WFLOP1_constraints(vars, boundary_vertices=None, n_elements=WFLOP1_NUMBER_ELEMENTS,
                       max_col=WFLOP1_MAX_ELEMENTS_PER_ROW, scaling_factor=None):
    """
    Constraint functions for WFLOP#1
    """
    assert isinstance(vars, list)
    assert boundary_vertices is not None

    wf_vertices = get_wf_vertices(vars)
    v1 = get_vector(wf_vertices[:,0], wf_vertices[:,1])
    v2 = get_vector(wf_vertices[:,0], wf_vertices[:,2])

    cons = []
    # Boundary constraints
    # --------------------
    if WFLOP1_CONS_BOUNDARY:
        boundary_expr = get_boundary_constraints_expr(vars, boundary_vertices)
        #boundary_expr = get_boundary_constraints_linalg_expr(vars, boundary_vertices)

        #boundary_expr = get_boundary_constraints_windnum_expr(vars, boundary_vertices)
        cons.append(boundary_expr)
    
    # Area constraints
    # ----------------
    if WFLOP1_CONS_AREA:
        area_expr = get_area_from_vectors(v1, v2)
        cons.append(area_expr)

    # Proximity constraints
    # ---------------------
    if WFLOP1_CONS_PROXIMITY:
        prox_expr = get_proximity_expr_wflop1(vars, n_elements, max_col)
        cons.append(prox_expr)

    cons = sp.Matrix(cons)

    return cons


def WFLOP1_dxdvars(vars, n_elements, max_col, scaling_factor=None, *args, **kwargs):
    """
    Calculates dx_i/dvars_i for variables from WFLOP1
    """
    expr = dxdvars(vars, n_elements, max_col, *args, **kwargs)

    #if scaling_factor is not None:
    #    dsubs = {var: getattr(scaling_factor, str(var))*var for var in vars}
    #    assert len(dsubs) == len(vars)
    #    logger.info('Applying scaling factor')
    #    expr = expr.subs(dsubs)

    return expr


def WFLOP1_vars():
    """
    Returns a list of variables for WFLOP#1
    alpha  - rotation angle of the WF
    beta   - shear angle fo the WF
    b_x    - distance between WT in a row
    b_y    - distance between rows
    d_x    - horizontal translation of the WF
    d_y    - vertical translation of the WF
    """
    alpha, beta, bx, by, dx, dy = sp.symbols('alpha, beta b_x, b_y, d_x, d_y', real=True)
    return [alpha, beta, bx, by, dx, dy]


def WFLOP2_constraints(vars, boundary_vertices=None, n_elements=WFLOP1_NUMBER_ELEMENTS,
                       scaling_factor=None):

    cons = []

    #cons = sp.Matrix([pcons])
    if WFLOP1_CONS_BOUNDARY:
        bcons = get_boundary_constraints_expr(vars, boundary_vertices, n_elements,
                                          problem_type='WFLOP2')
        cons.append(bcons)

    if WFLOP1_CONS_PROXIMITY:
        pcons = get_proximity_expr_wflop2(vars)
        cons.append(pcons)

    cons = sp.Matrix(cons)
    return cons


def WFLOP2_vars(n_elements):

    x = [sp.Symbol('x_{}'.format(ii)) for ii in range(n_elements)]
    y = [sp.Symbol('y_{}'.format(ii)) for ii in range(n_elements)]

    return x + y

"""
Config variables, etc.
"""
import logging
import os
#import random
#import string
import datetime
from collections import namedtuple


# ==============
# logging config
# ==============
__LOG_FILE_PATH = 'stdout.log'
__FORMAT = '[%(asctime)s.%(msecs)03d][%(levelname)-07s][%(processName)-20s]:  %(message)s'
__DATE_FORMAT = '%s'

# create logger
logger = logging.getLogger('MainProject')
logger.setLevel(logging.DEBUG)

# create formatter
ftr = logging.Formatter(__FORMAT, __DATE_FORMAT)

# create console handler
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console.setFormatter(ftr)

# create file handler
fhandler = logging.FileHandler(__LOG_FILE_PATH, mode='w')
fhandler.setLevel(logging.DEBUG)
fhandler.setFormatter(ftr)

# add all log handlers
logger.addHandler(console)
logger.addHandler(fhandler)


# ==================
# COLONEL EXECUTABLE
# ==================
__colonelExecutableDirPath = os.path.expanduser(
                '~/Documents/Studia/Master Thesis/Software/Colonel/Colonel Lazarus')
COLONEL_EXECUTABLE_NAME = 'ColonelLazarus'
COLONEL_EXECUTABLE_PATH = os.path.join(__colonelExecutableDirPath, COLONEL_EXECUTABLE_NAME)


# ==============
# PARALLEL STUFF
# ==============
NUMBER_COLONEL_INSTANCES = 1


# =================
# WORKSPACE FOLDERS
# =================
RAMDISK_PATH = '/dev/shm'
# new unique directory
DELETE_WORKSPACE_DIR = True # should workspace dir be deleted at the end?
#DELETE_WORKSPACE_DIR = False # should workspace dir be deleted at the end?
__tmpDirPrefix = 'Colonel'
__tmpDirName = __tmpDirPrefix + datetime.datetime.now().strftime('%Y%m%d%H%M%S')
WORKSPACE_PATH = os.path.join(RAMDISK_PATH, __tmpDirName)   # main temporary workspace directory
COLONEL_INPUT_DIR_PREFIX = 'IN'
COLONEL_OUTPUT_DIR_PREFIX = 'OUT'
#COLONEL_INPUT_PATH = os.path.join(WORKSPACE_PATH, __colonelInputDirPrefix)
COLONEL_INPUT_FILE = 'ColonelInput.txt'  # has to be the same as in the init file!!
#COLONEL_OUTPUT_PATH = os.path.join(WORKSPACE_PATH, __colonelOutputDirPrefix)
COLONEL_OUTPUT_FILE = 'ColonelOutput.txt'
COLONEL_INIT_PATH = '/home/bartosz/Documents/Studia/Master Thesis/Software/Colonel/ColonelWorkspaces/ColonelInit.txt'
DEFAULT_INIT_FILENAME = 'ColonelInit.txt'
#DEFAULT_OUTPUT_FILENAME = 'ColonelOutput.txt'

#__randomStringLength = 16
#randString = lambda vv: ''.join(random.choice(string.lowercase + string.uppercase + \
#        string.digits) for _ in range(vv))
# global list variable containing random Colonel string identifiers
#COLONEL_INSTANCE_IDENTIFIER = [randString(__randomStringLength ) for _ in \
#        range(NUMBER_COLONEL_INSTANCES)]
# a simple integer representing instance number is enough
COLONEL_INSTANCE_IDENTIFIER = ['{}'.format(_) for _ in range(NUMBER_COLONEL_INSTANCES)]


# =========================
# INPUT/OUTPUT DATA STORAGE
# =========================
ARCHIVE_NAME = datetime.datetime.now().strftime('%Y%m%d%H%M%S')    # use current date and time as filename 
#ARCHIVE_FOLDER_PATH = os.path.abspath('./Data')
ARCHIVE_FOLDER_PATH = os.path.abspath(os.path.join('./Data', ARCHIVE_NAME))
ARCHIVE_STATUS = True  # determine whether we should save the input/output files and other data
ARCHIVE_FILE_PATH = os.path.join(ARCHIVE_FOLDER_PATH, ARCHIVE_NAME + '.commands')


# =================
# FINITE DIFFERENCE
# =================
#FD_SCHEME = 'Forward'
FD_SCHEME = 'Central'
FD_ORDER = 1
FD_COEFFICIENTS = [[-1., 1.],                                           # 1
                   [-3./2, 2., -1./2],                                  # 2
                   [-11./6, 3., -3./2, 1./3],                           # 3
                   [-25./12, 4., -3., 4./3, -1./4],                     # 4
                   [-137/.60, 5., -5., 10./3, -5./4, 1./5],             # 5
                   [-49./20, 6., -15./2, 20./3, -15./4, 6./5, -1./6],   # 6
                   [-363./140, 7., -21./2, 35./3, -35./4, 21./5, -7./6, 1./7],
                   [-761./280, 8., -14., 56./3, -35./2, 56./5, -14./3, 8./7, -1./8]]
CD_COEFF = [[-1/2., 1/2.],
            [1./12, -2./3, 2./3, -1./12],
            [-1./60, 3./20, -3./4, 3./4, -3./20, 1./60],
            [1./280, -4./105, 1./5, -4./5, 4./5, -1./5, 4./105, -1./280]]
#FD_PERTURBATION = 200.
#FD_PERTURBATION = 100.
#FD_PERTURBATION = 75.
#FD_PERTURBATION = 50.
#FD_PERTURBATION = 25.
#FD_PERTURBATION = 20.
#FD_PERTURBATION = 15.
#FD_PERTURBATION = 10.
#FD_PERTURBATION = 5.
FD_PERTURBATION = 1.
#FD_PERTURBATION = 0.5
#FD_PERTURBATION = 0.1
#FD_PERTURBATION = 0.05
#FD_PERTURBATION = 0.01
#FD_PERTURBATION = 0.005
#FD_PERTURBATION = 0.001
#FD_PERTURBATION=0.0000001

# ==================
# OPTIMIZATION
# ==================
# which optimization framework to use
# pyOpt, ipopt
#OPT_BACKEND = 'pyOpt'
OPT_BACKEND = 'ipopt'
# specify which optimizer pyOpt should use
OPT_PYOPT_OPTIMIZER = ''
# flag determining whether to use FD or native Colonel gradients
# Possible choices:
# 'FD', 'Colonel'
#OPT_GRADIENT = 'FD'
OPT_GRADIENT = 'Colonel'
#GRADIENT_COORD_SYS = 'lattice'
GRADIENT_COORD_SYS = 'cartesian'

IPOPT_TOLERANCE = 1e-4

#IPOPT_PERTURBATION = 200.
#IPOPT_PERTURBATION = 100.
#IPOPT_PERTURBATION = 75.
#IPOPT_PERTURBATION = 50.
#IPOPT_PERTURBATION = 25.
#IPOPT_PERTURBATION = 20.
#IPOPT_PERTURBATION = 15.
#IPOPT_PERTURBATION = 10.
#IPOPT_PERTURBATION = 5.
IPOPT_PERTURBATION = 1.
#IPOPT_PERTURBATION = 0.5
#IPOPT_PERTURBATION = 0.1
#IPOPT_PERTURBATION = 0.05
#IPOPT_PERTURBATION = 0.01
#IPOPT_PERTURBATION = 0.005
#IPOPT_PERTURBATION = 0.001

# =======
# SCALING
# =======

# wflop1
__scaling_wflop1 = namedtuple('Scaling', ['aep', 'alpha', 'beta', 'b_x', 'b_y', 'd_x', 'd_y'])
__scaling_wflop1_con = namedtuple('Scaling_constraints', ['boundary'])
#WFLOP1_SCALING = __scaling_wflop1(-1., 1., 1., 1e-2, 1e-2, 1e-3, 1e-3)
#WFLOP1_SCALING = __scaling_wflop1(-1., 1e-7, 1e-7, 1e-5, 1e-5, 1e-4, 1e-4)
#WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e-6, 1e-6, 1e-4, 1e-4, 1e-3, 1e-3)  #  nope
#WFLOP1_SCALING = __scaling_wflop1(-1e-3, 1e-6, 1e-6, 1e-4, 1e-4, 1e-3, 1e-3)  # ok
#WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e-6, 1e-6, 1e-4, 1e-4, 1e-3, 1e-3)   # nope
#WFLOP1_SCALING = __scaling_wflop1(-1e-3, 1e-6, 1e-6, 1e-4, 1e-4, 1e-3, 1e-3)   # nope
#WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e-5, 1e-5, 1e-3, 1e-3, 1e-2, 1e-2)     # ok
#WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e-4, 1e-4, 1e-2, 1e-2, 1e-1, 1e-1)  # ok
#WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e-3, 1e-3, 1e-1, 1e-1, 1e0, 1e0)  # ok
#WFLOP1_SCALING = __scaling_wflop1(-1e-3, 1e-3, 1e-3, 1e-1, 1e-1, 1e0, 1e0)  # ok
#WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e-2, 1e-2, 1e0, 1e0, 1e1, 1e1)
#WFLOP1_SCALING = __scaling_wflop1(-1e-3, 1e-2, 1e-2, 1e1, 1e1, 1e1, 1e1)  # bx, dx scaled in the same way
#WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e-1, 1e-1, 1e1, 1e1, 1e2, 1e2)
#WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e0, 1e0, 1e2, 1e2, 1e3, 1e3)

#WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e-2, 1e-2, 1e0, 1e0, 1e1, 1e1)  # really bad scaling?
#WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e-1, 1e-1, 1e1, 1e1, 1e2, 1e2)

#WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e-4, 1e-4, 1e-2, 1e-2, 1e-1, 1e-1)
#WFLOP1_SCALING = __scaling_wflop1(-1e2, 1e0, 1e0, 1e2, 1e2, 1e3, 1e3)    # this is the correct way to do this? -> wrong way of scaling
#WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e0, 1e0, 1e3, 1e3, 1e3, 1e3)
WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e0, 1e0, 1e3, 1e3, 1e3, 1e3)

#WFLOP1_SCALING = __scaling_wflop1(-1e0, 1e1, 1e1, 1e4, 1e4, 1e4, 1e4)
#WFLOP1_SCALING = __scaling_wflop1(-1., 1., 1., 1., 1., 1., 1.)

WFLOP1_SCALING_CON = __scaling_wflop1_con(1.)
#OPT_SCALING = __scaling(-1., 1., 1., 0.1, 0.1, 0.01, 0.01)

# wflop2
__scaling_wflop2 = namedtuple('Scaling', ['aep', 'x', 'y'])
WFLOP2_SCALING = __scaling_wflop2(-1., 1., 1.)
#WFLOP2_SCALING = __scaling_wflop2(1., 1., 1.)

# wflop 3
__scaling_wflop3 = namedtuple('Scaling', ['aep', 'tau', 'alpha', 'beta', 'b_x', 'b_y', 'd_x', 'd_y'])
WFLOP3_SCALING = __scaling_wflop3(1e0, -1e0, 1e0, 1e0, 1e3, 1e3, 1e3, 1e3)


# ========
# PLOTTING
# ========
PLOTTING_ENABLED = True
#PLOTTING_ENABLED = False


# =======
# WFLOP 1
# =======
WFLOP1_NUMBER_ELEMENTS = 16
WFLOP1_MAX_ELEMENTS_PER_ROW = 4

# constraints
# -----------
WFLOP1_CONS_BOUNDARY = True
WFLOP1_CONS_AREA = False
#WFLOP1_CONS_AREA = True
WFLOP1_CONS_PROXIMITY = False
#WFLOP1_CONS_PROXIMITY = True

#WFLOP_REGION = 'hornsrev'
#WFLOP_REGION = 'simple'
#WFLOP_REGION = 'vesterhavsyd'
WFLOP_REGION = 'bathymetry'


# ==================
## INITIAL INPUT FILE
# ==================
initialCommandList = [
    {'Name': 'initialize', 'Arguments': None},
    {'Name': 'load farm', 'Arguments': ['HornsRev1']},
    {'Name': 'z0 =', 'Arguments': [0.0001]},
    {'Name': 'zi =', 'Arguments': [400.0]},
    {'Name': 'zeta0 =', 'Arguments': [0.]},
    {'Name': 'load wakes', 'Arguments': None},
    {'Name': 'insert met mast', 'Arguments': [435253.0, 6149502.0, 68.0]},
    #{'Name': 'Wind climates interpolation', 'Arguments': ['on']},
    #{'Name': 'Wind Climate filter width =', 'Arguments': [20]},
    {'Name': 'Gaussian fit', 'Arguments': ['on']},

    {'Name': 'Gradients on', 'Arguments': None},
    #{'Name': 'Gradients off', 'Arguments': None},

    #{'Name': 'load wind climates', 'Arguments': ['HornsRev1/WindClimates.txt']},
    #{'Name': 'load wind atlas', 'Arguments': ['HornsRev1/WindAtlasM7.lib']},
    #{'Name': 'load wind atlas', 'Arguments': ['HornsRev1/wrf_HRI_55.489N_7.832E_70m.lib']},
    # colonel cant use this one
    {'Name': 'load wind atlas', 'Arguments': ['HornsRev1/WindFromNorth20sec20151219.lib']}, 
    #{'Name': 'load wind atlas', 'Arguments': ['HornsRev1/EastManual.lib']}, 
    #{'Name': 'load wind atlas', 'Arguments': ['HornsRev1/WindFromEast20151219.lib']},
    #{'Name': 'load wind atlas', 'Arguments': ['HornsRev1/10mpsUnidirectional.lib']},
    #{'Name': 'load wind atlas', 'Arguments': ['HornsRev1/10mpsUnidirectionalEast.lib']},
    #{'Name': 'load wind atlas', 'Arguments': ['HornsRev1/10mpsOmnidirectional.lib']},
    #{'Name': 'get turbine list', 'Arguments': None}]
    ]


def RunInitialConfig():
    """ Sets up the initial variables, directory structure, runs checks, etc. """

    from ColonelInterface import ColonelInit
    from ColonelInterface import ColonelInput

    # create the directories
    logger.info('Creating temporary workspace directory {}'.format(WORKSPACE_PATH))
    os.mkdir(WORKSPACE_PATH)

    # read the init file
    InitFile = ColonelInit(COLONEL_INIT_PATH)
    InitFile.read()

    # check whether archive dir exists
    if ARCHIVE_STATUS == True:
        if not os.path.exists(ARCHIVE_FOLDER_PATH):
            logger.info('Creating archive directory {}'
                        .format(ARCHIVE_FOLDER_PATH))
            os.mkdir(ARCHIVE_FOLDER_PATH)


    for ii in range(NUMBER_COLONEL_INSTANCES):
        logger.info('Setting instance directory for {}, [{}/{}]'
                    .format(COLONEL_INSTANCE_IDENTIFIER[ii], ii+1, 
                        NUMBER_COLONEL_INSTANCES))

        # instance directory
        INSTANCE_DIR = os.path.join(WORKSPACE_PATH, COLONEL_INSTANCE_IDENTIFIER[ii])
        os.mkdir(INSTANCE_DIR)

        # input/output directories
        #COLONEL_INPUT_PATH = os.path.join(INSTANCE_DIR, __colonelInputDirPrefix)
        #COLONEL_OUTPUT_PATH = os.path.join(INSTANCE_DIR, __colonelOutputDirPrefix)
        INSTANCE_INPUT_PATH = os.path.join(INSTANCE_DIR, COLONEL_INPUT_DIR_PREFIX)
        INSTANCE_OUTPUT_PATH = os.path.join(INSTANCE_DIR, COLONEL_OUTPUT_DIR_PREFIX )
        os.mkdir(INSTANCE_INPUT_PATH)
        os.mkdir(INSTANCE_OUTPUT_PATH)

        # write the init file
        InitFile.write(location=INSTANCE_DIR)

        # write the initial input file
        inputfile = ColonelInput(COLONEL_INSTANCE_IDENTIFIER[ii])
        for iDict in initialCommandList:
            #if iDict['Name'] == 'get turbine list' and ii != 0:
            #    logger.debug('Skipping \'get turbine list\' command for <{}>'
            #                 .format(ii))
            #    continue
            inputfile.addCommand(iDict['Name'], commandArguments=iDict['Arguments'])
        inputfile.setInputFile(os.path.join(INSTANCE_INPUT_PATH, COLONEL_INPUT_FILE))
        inputfile.save()

        # link the executable
        logger.info('Linking the Colonel executable from {}'
                    .format(COLONEL_EXECUTABLE_PATH))
        os.symlink(COLONEL_EXECUTABLE_PATH,
                os.path.join(INSTANCE_DIR, COLONEL_EXECUTABLE_NAME))
